trigger Session_Trigger on Session__c (after insert) {


    if (trigger.isAfter && trigger.isInsert) {
        session_Methods.afterInsert(trigger.newmap);
    }
}