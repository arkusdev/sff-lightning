trigger night_Trigger on Night__c (after insert) {
    
    if (trigger.isAfter && trigger.isInsert){
        night_Methods.afterInsert(trigger.new);
    }
}