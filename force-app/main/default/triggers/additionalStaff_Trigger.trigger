trigger AdditionalStaff_Trigger on Additional_Staff__c (after insert, after update, before insert, before update, after delete) {
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        AdditionalStaff_Methods.afterUpsert(Trigger.newMap);
    }
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        AdditionalStaff_Methods.preventDuplicateAdditionalStaff(Trigger.New);
    }
    if(Trigger.isAfter && Trigger.isDelete) {
        AdditionalStaff_Methods.removePrimaryStaffOnCase(Trigger.Old);
    }
}