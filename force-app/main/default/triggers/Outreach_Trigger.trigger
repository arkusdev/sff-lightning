trigger Outreach_Trigger on Outreach__c (before insert) {
	
	if(trigger.isBefore && Trigger.isInsert)
		OutreachTrainingMethods.beforeInsert(trigger.new); 
}