trigger Service_Trigger on Service__c (before delete, after update, after insert) {
    /*
    if (trigger.isAfter && trigger.isInsert){
        service_Methods.afterInsert(trigger.new);
    }*/

    if (trigger.isafter && trigger.isupdate){
        service_Methods.afterUpdate( trigger.new, trigger.oldmap);
    }

    if(trigger.isBefore && trigger.isDelete) {
        Service_Methods.beforeDelete(trigger.oldmap);
    }
}