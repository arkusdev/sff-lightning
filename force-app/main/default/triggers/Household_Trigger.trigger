trigger Household_Trigger on npo02__Household__c (after insert, before update) {
	if (trigger.isAfter && trigger.isInsert) {
		Household_Methods.afterInsert(trigger.new);
	}
	if (trigger.isBefore && trigger.isUpdate) {
		Household_Methods.beforeUpdate(trigger.old, trigger.newmap);
	}
}