trigger contact_Trigger on Contact (before insert, before update, after update, before delete) {

    if (trigger.isBefore && trigger.isInsert) {
        contact_methods.beforeInsert(trigger.new);
    }

    if (trigger.isBefore && trigger.isUpdate) {
        contact_methods.beforeUpdate(trigger.new, trigger.oldmap);
    }

    if (trigger.isAfter && trigger.isUpdate){
        contact_Methods.afterUpdate(trigger.newMap, trigger.oldMap);
        contact_Methods.updateProbonoFirmOnCases(trigger.newMap);
        contact_Methods.updateContactsWithFirmOrSchoolQuantOnCase(trigger.newMap.keySet());
    }
    if (trigger.isBefore && trigger.isDelete) {
        contact_methods.beforeDelete(trigger.oldMap);
    }

}