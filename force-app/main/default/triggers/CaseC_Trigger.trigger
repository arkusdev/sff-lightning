trigger CaseC_Trigger on Case__c (after insert, after update, before delete, before insert,before update) {
	
	if (trigger.isBefore && trigger.isinsert){
		caseC_Methods.beforeInsert(trigger.new);
		caseC_Methods.updateContactsWithFirmOrSchoolQuantOnCase(trigger.new);
	}
	
	if (trigger.isafter && trigger.isinsert) {
		caseC_methods.afterInsert(trigger.new);
	}
	
	if (trigger.isafter && trigger.isupdate) {
		caseC_methods.afterUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
	}
	
	if (trigger.isbefore && trigger.isdelete) {
		caseC_Methods.beforeDelete(trigger.oldmap);
	}
	if (trigger.isbefore && trigger.isupdate) {
		system.debug('in ###############################################');
		caseC_Methods.beforeUpdate(trigger.old,trigger.new);
		caseC_Methods.updateContactsWithFirmOrSchoolQuantOnCase(trigger.new);
	}
}