({
	redirectToIntake : function(component, event, helper) {
		var id = component.get("v.recordId");
        setTimeout(function(){
            window.open('/apex/IntakeRevamp?cid='+id, '_blank');
            $A.get("e.force:closeQuickAction").fire(); 
		}, 1000);
	}
})