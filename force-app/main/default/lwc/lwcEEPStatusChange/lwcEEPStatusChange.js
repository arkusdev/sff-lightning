import { LightningElement, api, wire, track } from 'lwc';
import { getRecordCreateDefaults, generateRecordInputForCreate, getRecord } from 'lightning/uiRecordApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import updateCaseEEPStatus from '@salesforce/apex/EEPStatusController.updateCaseEEPStatus';

const RECORD_TYPE_NAME = "Economic Empowerment";
const CASE_OBJECT = 'Case__c';
const EEP_OBJECT = 'EEP_Status__c';

const CASE_FIELD_ECS = 'EEP_Current_Status__c';
const CASE_FIELD_NS = 'EEP_Next_Step__c';

const EEP_FIELD_DATE = 'Date__c';
const EEP_FIELD_STATUS = 'Status__c';
const EEP_FIELD_CASE = 'Case__c';

export default class LwcEEPStatusChange extends LightningElement {
    EEP_FIELD_DATE = EEP_FIELD_DATE;
    EEP_FIELD_STATUS = EEP_FIELD_STATUS;
    EEP_FIELD_CASE = EEP_FIELD_CASE;

    CASE_OBJECT = CASE_OBJECT;
    EEP_OBJECT = EEP_OBJECT;

    @api recordId
    @track record;
    @track error;

    @track ecsValue;
    @track nsValue;
    @track dateValue;

    @track recordTypeId;

    @track ecsPl;
    @track nsPl;

    @track openModal = false;
    @track showSpinner = false;



    @wire(getRecordCreateDefaults, { objectApiName: EEP_OBJECT })
    eepStatusObjectTemplate;

    get statusRecord() {
        if (!this.eepStatusObjectTemplate.data) {
            return undefined;
        }

        const contactObjectInfo = this.eepStatusObjectTemplate.data.objectInfos[
            EEP_OBJECT.objectApiName
        ];
        const recordDefaults = this.eepStatusObjectTemplate.data.record;
        const recordInput = generateRecordInputForCreate(
            recordDefaults,
            contactObjectInfo
        );
        return recordInput;
    }

    @wire(getRecord, {
        recordId: '$recordId',
        fields: [
            `${CASE_OBJECT}.${CASE_FIELD_ECS}`,
            `${CASE_OBJECT}.${CASE_FIELD_NS}`
        ]
    })
    wiredRecord({ error, data }) {
        if (data) {
            this.record = data;
            if (this.record && this.record.recordTypeInfo.name === RECORD_TYPE_NAME) {
                this.recordTypeId = this.record.recordTypeInfo.recordTypeId;
            }
            if (this.recordTypeId) {
                if (this.record.fields[CASE_FIELD_ECS].value) {
                    this.ecsValue = this.record.fields[CASE_FIELD_ECS].value;
                }
                if (this.record.fields[CASE_FIELD_NS].value) {
                    this.nsValue = this.record.fields[CASE_FIELD_NS].value;
                }
            }

        } else if (error) {
            this.error = "Failed to load record data."
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: `${CASE_OBJECT}.${CASE_FIELD_ECS}` })
    getEPCPicklistValues({ error, data }) {
        if (data) {
            this.ecsPl = data;
        } else if (error) {
            this.error = "Failed to load record data."
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: `${CASE_OBJECT}.${CASE_FIELD_NS}` })
    getNextStepPicklistValues({ error, data }) {
        if (data) {
            this.nsPl = data;
        } else if (error) {
            this.error = "Failed to load record data."
        }
    }

    handleOpenModal() {
        this.openModal = true;
    }

    closeModal() {
        this.openModal = false;
    }

    saveData() {
        this.showSpinner = true;
        if (this.ecsValue) {
            updateCaseEEPStatus({
                caseId: this.recordId,
                currentStatus: this.ecsValue,
                nextStep: this.nsValue,
                dateModified: this.dateValue
            }).then(() => {
                this.error = undefined;
                this.openModal = false;
                this.showSpinner = false;
            }).catch((error) => {
                this.openModal = false;
                this.showSpinner = false;
                this.error = error.body.message;
            });
        } else {
            this.error = 'EEP Status cannot be empty';
        }
    }

    handleESCChange(evt) {
        this.ecsValue = evt.detail.value;
    }

    handleNSChange(evt) {
        this.nsValue = evt.detail.value;
    }

    handleDateChange(evt) {
        this.dateValue = evt.detail.value;
    }

    handleFormError() {
        this.error = 'Unable to create EEP Status Record'
    }
}