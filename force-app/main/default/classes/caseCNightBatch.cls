global class  caseCNightBatch implements Database.Batchable<sObject> {
	
	static Date startDate;	
	
   global caseCNightBatch(){
		startDate = system.today();
   }
   
   global caseCNightBatch(Date d){
		startDate = d;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
   		
      return Database.getQueryLocator([select id,Case_Open_Date__c,Shelter__c,Case_Close_Date__c from case__c where createdDate>=:startDate and recordType.name='Shelter']);
   }

   global void execute(Database.BatchableContext BC, List<Case__c> scope){
	   	list<Night__c> newNights = new list<Night__c>();
	     for(Case__c c : scope){    
	
				if (c.Case_Open_Date__c != null && c.Shelter__c != null){ 
					if (c.Case_Close_Date__c != null) {
						createNights(c, c.Case_Open_Date__c,newNights);
					}
					else {
						Night__c n = new Night__c();
						n.date_service_was_provided__c = c.Case_Open_Date__c;
						n.case__c = c.id;
						newNights.add(n);	
					}
				}
			}						
			if (newNights.size() > 0) {
				insert newNights;	
			}
    }
    
    public static void createNights(Case__c cas, Date start,list<Night__c> newNights) {
		for (integer i=0; true; i++) {
			if (start.adddays(i) > cas.Case_Close_Date__c) {
				break;
			}
			else {
				Night__c n = new Night__c();
				n.date_service_was_provided__c = start.adddays(i);
				n.case__c = cas.id;
				newNights.add(n);
			}
		}
	}

   global void finish(Database.BatchableContext BC){
   }

}