@isTest 
private class SFF_wrapperServiceTest {
    static testmethod void test() {
        
        contact testContact = new contact();
        testContact.FirstName = 'firstname';
        testContact.LastName = 'lastname';
        testContact.Birthdate = date.newInstance(1987, 3, 3);
        insert testContact;
        //
        Case__c testCase = new Case__c();
		testCase.Contact__c = testContact.id;
        testCase.Project__c = 'Children & Youth Program';
        testCase.Location__c ='BxFJC';
        testCase.Case_Open_Date__c = date.today();
        testCase.Matter_Type_Detail__c = 'Child Support';
        insert testCase;
        //
        Event__c testEvent = new Event__c();
        testEvent.Case__c = testCase.id;
        testEvent.Mode__c = 'In Office';
        testEvent.Date__c =date.newInstance(2015, 4, 4);
        testEvent.Event_Note__c = 'text sample';
        insert testEvent;
        //
        Night__c testNight = new Night__c();
        testNight.Case__c = testCase.id; 
        date aux =  date.newInstance(2015, 5, 5 );
        testNight.Date_Service_was_Provided__c = aux;
        insert testNight;
        //
        Session__c testSession = new Session__c();
        testSession.Contact__c = testContact.id;
        testSession.Case__c = testCase.id;
        testSession.Session_Date__c = date.newInstance(2015, 6, 6);
        insert testSession;
        //
        Service__c testService = new Service__c();
        testService.Event__c = testEvent.id;
        testService.Service__c = 'Advice (Legal)';
        testService.Subtype__c='Abuse & Neglect';
        testService.Area_of_Focus__c ='Child/Parent Services - Children\'s/Family Activities';
        testService.Mode__c='In Office';
        testService.Service_Date__c= date.newInstance(2015, 7, 7);
        insert testService;
        
        Map<id,SFF_wrapperService> listWrapp = new Map<id,SFF_wrapperService>();
        list<SFF_wrapperService>  listWrapperToSort = new list<SFF_wrapperService>();
         Service__c listServices = [select id, name, Service__c,
                                                    Service_Date__c, 
                                                    Subtype__c, 
                                                    Event__r.name,
                                                    Event__r.case__r.name,
                                                    Area_of_Focus__c, 
                                                    Hours_of_Service_Provided__c, 
                                                    Unit__c, 
                                                    Event__r.Mode__c, 
                                                    Event__r.case__c,
                                                    Staff_Member__c,
                                                    Ownerid
                                            from  Service__c 
                                            where Event__r.case__c=:testCase.id limit 1];
         Session__c listSessions = [SELECT Id,Service_Provided__c,Attendance_Status__c,Area_of_Focus__c,Session_Date__c,Subtype__c,Case__r.name,Case__c,Ownerid,name  FROM Session__c WHERE Case__c = :testCase.id limit 1];
         Night__c   listNight    = [SELECT Id,Attendance_status__c,Date_Service_was_Provided__c,Case__c,Case__r.name,Case__r.Primary_Staff_on_Case__c,name FROM Night__c WHERE Case__c = :testCase.id limit 1];
         listWrapp.put(listServices.id, new SFF_wrapperService(false,
                                                  '',
                                                 listServices.Event__r.name,
                                                 listServices.Staff_Member__c,
                                                 listServices.Event__r.case__c,
                                                 listServices.Id,
                                                 listServices.Service__c,
                                                 'Service',
                                                 listServices.Event__r.case__r.name,
                                                 listServices.Service_Date__c,
                                                 listServices.Subtype__c,
                                                 listServices.Area_of_Focus__c,
                                                 listServices.Hours_of_Service_Provided__c,
                                                 listServices.Unit__c,
                                                 listServices.Event__r.Mode__c,
                                                 listServices.Staff_Member__c ));
        listWrapp.put(listSessions.id, new SFF_wrapperService(false,
                                                 listSessions.Attendance_Status__c,
                                                 listSessions.name,
                                                 listSessions.Ownerid,
                                                 listSessions.Case__c,
                                                 listSessions.id,
                                                 listSessions.Service_Provided__c,
                                                 'Session',
                                                 listSessions.Case__r.name,
                                                 listSessions.Session_Date__c,
                                                 listSessions.Subtype__c,
                                                 listSessions.Area_of_Focus__c,
                                                 0,0, '',''));
        listWrapp.put(listNight.id,new SFF_wrapperService(false,
                                                listNight.Attendance_status__c,
                                                listNight.name,
                                                listNight.Case__r.Primary_Staff_on_Case__c,
                                                listNight.Case__c,
                                                listNight.id,
                                                'Bed Night',
                                                'Night',
                                                listNight.Case__r.name,
                                                listNight.Date_Service_was_Provided__c,
                                               '','',0,0,'','' ));
        
        listWrapperToSort = listWrapp.values();
        listWrapperToSort.sort();
    }
}