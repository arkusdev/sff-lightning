public with sharing class OutreachTrainingMethods {
	
	public static void beforeInsert(list<Outreach__c> newlist){
		
		System.debug(newlist);
		set<id> ownerIds =new set<id>();
		for(OutReach__c outReach:newlist){
			ownerIds.add(outReach.ownerId);
		}
		list<Contact> staffContacts=[select id,name,salesforce_user__c from contact where Salesforce_User__c in: ownerIds];
		map<id,id> staffOwnerMap=new map<id,id>();
		for(contact c:staffContacts)
			staffOwnerMap.put(c.SalesForce_user__c,c.id);
			
		
	    for(outReach__c outReach:newlist){
	    	if(staffOwnerMap.containskey(outReach.ownerID))
	    		outReach.Primary_Staffer__c	=staffOwnerMap.get(outReach.ownerID);
	    	
	    }
		
	}
	@isTest
	public static void testOutreach(){
		Contact con=new Contact(FirstName='Test',LastName='Test');
		insert con;
		Outreach__c Out=new Outreach__c(Event_date__c=Date.today(), Staff_Name__c=con.id,Event_Length_hrs__c=22);
		insert Out; 
		
	}
}