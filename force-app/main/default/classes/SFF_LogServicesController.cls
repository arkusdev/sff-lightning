global with sharing class SFF_LogServicesController {
	
	public Service_Subtype_Mapping__c sms {get;set;}
	
	public Id caseId {get;set;}
	
	public Id eventId {get;set;}
	
	public Case__c caseRecord {get;set;}

	public Contact currentStaffMember {get;set;}
	
	public Boolean isStaffMember {get;set;}
	
	public Event__c newEvent {get; set;}
	
	public Event__c eventBackup {get;set;}
	
	public Service__c newService {get;set;}
	
	public List<ServiceWrapper> serviceList {get;set;}
	
	public List<Contact> staffMembers {get;set;}
	
	public Set<Id> staffMemberSet {get;set;}
	
	public Datetime lastSaved {get;set;}
	
	public String lastSavedStr {get;set;}
	
	public String preSetValue {get;set;}
	
	public String eventDate {get;set;}
	
	public String subtypeValue {get;set;}
	
	public String areaOfFocusValue {get;set;}
	
	public SFF_LogServicesController(){
		sms = new Service_Subtype_Mapping__c();
		
		caseId = ApexPages.currentPage().getParameters().get('cId');
		
		eventId = ApexPages.currentPage().getParameters().get('eId');
		
		newEvent = new Event__c();
		
		serviceList = new List<ServiceWrapper>();
		
		if (eventId != null){
			newEvent = [select id, 
								Mode__c, 
								Date__c, 
								Event_Note__c, 
								(select id, 
									Service__c, 
									Subtype__c, 
									Area_of_Focus__c,
									Dollar_Amount_of_Service_Provided__c, 
									Hours_of_Service_Provided__c,
									Staff_Member__r.Name,
									Staff_Member__r.Email,
									Staff_Member2__r.Name,
									Staff_Member2__r.Email,
									Staff_Member3__r.Name,
									Staff_Member3__r.Email,
									Staff_Member4__r.Name,
									Staff_Member4__r.Email,
									Staff_Member5__r.Name,
									Staff_Member5__r.Email,
									Staff_Member6__r.Name,
									Staff_Member6__r.Email,
									Staff_Member7__r.Name,
									Staff_Member7__r.Email,
									Staff_Member8__r.Name,
									Staff_Member8__r.Email,
									Staff_Member9__r.Name,
									Staff_Member9__r.Email,
									Staff_Member10__r.Name,
									Staff_Member10__r.Email
								from Services__r) 
						from Event__c 
						where id=:eventId];
		
			System.debug('#New Event '+newEvent);
			
			for (Service__c iter: newEvent.Services__r){
				
				System.debug('#Service '+iter);
				
				ServiceWrapper sw = new ServiceWrapper();
				sw.service = iter;
				sw.memberList = new List<Contact>();
				
				if (iter.Staff_Member__r != null){
					sw.memberList.add(iter.Staff_Member__r);
				}
				if (iter.Staff_Member2__r != null){
					sw.memberList.add(iter.Staff_Member2__r);
				}
				if (iter.Staff_Member3__r != null){
					sw.memberList.add(iter.Staff_Member3__r);
				}
				if (iter.Staff_Member4__r != null){
					sw.memberList.add(iter.Staff_Member4__r);
				}
				if (iter.Staff_Member5__r != null){
					sw.memberList.add(iter.Staff_Member5__r);
				}
				if (iter.Staff_Member6__r != null){
					sw.memberList.add(iter.Staff_Member6__r);
				}
				if (iter.Staff_Member7__r != null){
					sw.memberList.add(iter.Staff_Member7__r);
				}
				if (iter.Staff_Member8__r != null){
					sw.memberList.add(iter.Staff_Member8__r);
				}
				if (iter.Staff_Member9__r != null){
					sw.memberList.add(iter.Staff_Member9__r);
				}
				if (iter.Staff_Member10__r != null){
					sw.memberList.add(iter.Staff_Member10__r);
				}
				
				
				sw.memberList = staffMembers;
				sw.quantity = '1';
				
				serviceList.add(sw);	
				
			}			
			eventDate = newEvent.Date__c.month() + '/' + newEvent.Date__c.day() + '/' + newEvent.Date__c.year();			
		} else {
			eventDate = Date.Today().month() + '/' + Date.Today().day() + '/' + Date.Today().year();
		}
		
		caseRecord = [select id, Matter_Type_Detail__c, Contact_Name_Formula__c, Type_of_Assistance__c, Type_of_Matter__c, Family_Law_Detail_Option__c, Follow_up_Case_Type__c, Immigration_Law_Matter_Detail__c from Case__c where id=:caseId];
		
		List<Contact> contactList = [select id, Name, email from Contact where Salesforce_User__c =: UserInfo.getUserId()];
		
		staffMembers = new List<Contact>();
		
		staffMemberSet = new Set<Id>();
		
		isStaffMember = false;
		
		if (contactList.size() > 0){
			currentStaffMember = contactList.get(0);
			isStaffMember = true;
			staffMembers.add(currentStaffMember);
			staffMemberSet.add(currentStaffMember.Id);
		}
		
		
		System.debug('#Case Record '+caseRecord);
		
		newService = new Service__c();
		
		createBackup(eventId);
	}
	
	
	@RemoteAction
	global static List<String> getServicesValues(){
		
		List<String> values = new List<String>();
		
		for (Schema.Picklistentry ple : Service__c.service__c.getDescribe().getPickListValues()) {
			
			values.add(ple.getlabel());
		}
		
		return values;
	}
	
	@RemoteAction
	global static List<String> getAreaOfFocusValues(){
		
		List<String> values = new List<String>();
		
		for (Schema.Picklistentry ple : Service__c.Area_Of_Focus__c.getDescribe().getPickListValues()) {
			
			values.add(ple.getlabel());
		}
		
		return values;
	}
	
	@RemoteAction
	global static List<String> getServiceSubtypeMappingValues(){		
		Map<String, Service_Subtype_Mapping__c> ssmMap = new Map<String, Service_Subtype_Mapping__c>(); 
		List<String> values = new List<String>();		
		for (Service_Subtype_Mapping__c ssm : [select Name, Service__c, Subtypes__c, Area_of_Focus__c from Service_Subtype_Mapping__c]) {			
			if (!ssmMap.containsKey(ssm.Name))
				ssmMap.put(ssm.Name, ssm);
		}		
		values.addAll(ssmMap.keySet());
		return values;
	}
	
	public void addService(){
		System.debug('#New Service '+newService);
		newService.Subtype__c = subtypeValue;
		newService.Area_of_Focus__c = areaOfFocusValue;
		if (newService.Subtype__c != 'Area of Focus'){
			newService.Area_of_Focus__c = '--None--';
		}
		
		ServiceWrapper sw = new ServiceWrapper();
		sw.service = newService;
		sw.memberList = staffMembers;
		sw.quantity = '1';
		
		serviceList.add(sw);
		
		newService = new Service__c();
		staffMembers = staffMembers.clone();
	}
	
	public PageReference addPreSet() {
		List<Service_Subtype_Mapping__c> preSetList = [select Name, Service__c, Subtypes__c, Area_of_Focus__c from Service_Subtype_Mapping__c where Name =: preSetValue];		
		for (Service_Subtype_Mapping__c preSet : preSetList) { 
			if (!String.isEmpty(preSet.Subtypes__c)) {
				for (String iter: preSet.Subtypes__c.split(',')) {
					newService = new Service__c();
					staffMembers = staffMembers.clone();
					newService.Subtype__c = iter;
					newService.Service__c = preSet.Service__c;
					if (iter == 'Area of Focus') {
						newService.Area_of_Focus__c = preSet.Area_of_Focus__c;
					} else {
						newService.Area_of_Focus__c = '--None--';	
					}
					ServiceWrapper sw = new ServiceWrapper();
					sw.service = newService;
					sw.memberList = staffMembers;
					sw.quantity = '1';
					
					serviceList.add(sw);
				}
			} else {
				newService = new Service__c();
				staffMembers = staffMembers.clone();
				newService.Service__c = preSet.Service__c;
				newService.Subtype__c = '';
				newService.Area_of_Focus__c = '--None--';	
				ServiceWrapper sw = new ServiceWrapper();
				sw.service = newService;
				sw.memberList = staffMembers;
				sw.quantity = '1';
				
				serviceList.add(sw);
			}
		}
		/*
		if (!String.isEmpty(preSet.Subservices__c)) {
			List<String> subservicesValuesList = new List<String>();
			for (String iter: preSet.Subservices__c.split(',')) {
				subservicesValuesList.add(iter);
			}
			List<Service_Subtype_Mapping__c> ssmList = [select Service__c, Subtypes__c, Area_of_Focus__c from Service_Subtype_Mapping__c where Service__c IN : subservicesValuesList];
			for (Service_Subtype_Mapping__c ssm : ssmList) {
				for (String subtype : ssm.Subtypes__c.trim().split(',')) {
					if (!String.isEmpty(subtype)) {
						newService = new Service__c();
						staffMembers = staffMembers.clone();
						newService.Subtype__c = subtype;
						if (subtype != 'Area of Focus') {
							newService.Area_of_Focus__c = '--None--';
						} else {
							newService.Area_of_Focus__c = ssm.Area_of_Focus__c;
						}
						newService.Service__c = ssm.Service__c;
						
						ServiceWrapper sw = new ServiceWrapper();
						sw.service = newService;
						sw.memberList = staffMembers;
						sw.quantity = 1;
						
						serviceList.add(sw);
					}
				}
			}
		}
		*/
		newService = new Service__c();
		staffMembers = staffMembers.clone();
		
		return null;
	}
	
	public void addMember(){
			
		String staffId = ApexPages.currentPage().getParameters().get('staffId');
		
		List<Contact> contactList = [select id, name, email from Contact where id=:staffId];
		
		if (contactList.size() != 0 && !staffMemberSet.contains(contactList[0].Id) && staffMemberSet.size() < 10){
			
			staffMembers.add(contactList[0]);
			
			staffMemberSet.add(contactList[0].Id);
		}
		
	}
	
	public void removeMember(){
		
		String staffId = ApexPages.currentPage().getParameters().get('staffId');
		
		staffMemberSet.remove(staffId);
		
		Integer j = 0;
		while (j < staffMembers.size()){
		  if(staffMembers.get(j).Id == staffId){
		    staffMembers.remove(j);
		    break;
		  }else{
		    j++;
		  }
		}
		
	}
	
	public void removeService(){
		
		Integer serviceId = Integer.valueOf(ApexPages.currentPage().getParameters().get('serviceId'));
		
		serviceList.remove(serviceId);
		    
	}
	
	public PageReference save(){
		
		List<Service__c> serviceSFList = new List<Service__c>();
		
		newEvent.Case__c = caseId;
		upsert newEvent;
		
		for (ServiceWrapper iter: serviceList){
			
			//not already created
			if (iter.service.id == null){
				if (iter.memberList.size() > 0){
					iter.service.Staff_Member__c = iter.memberList[0].Id;
				}
				if (iter.memberList.size() > 1){
					iter.service.Staff_Member2__c = iter.memberList[1].Id;
				} 
				if (iter.memberList.size() > 2){
					iter.service.Staff_Member3__c = iter.memberList[2].Id;
				} 
				if (iter.memberList.size() > 3){
					iter.service.Staff_Member4__c = iter.memberList[3].Id;
				} 
				if (iter.memberList.size() > 4){
					iter.service.Staff_Member5__c = iter.memberList[4].Id;
				} 
				if (iter.memberList.size() > 5){
					iter.service.Staff_Member6__c = iter.memberList[5].Id;
				} 
				if (iter.memberList.size() > 6){
					iter.service.Staff_Member7__c = iter.memberList[6].Id;
				} 
				if (iter.memberList.size() > 7){
					iter.service.Staff_Member8__c = iter.memberList[7].Id;
				} 
				if (iter.memberList.size() > 8){
					iter.service.Staff_Member9__c = iter.memberList[8].Id;
				} 
				if (iter.memberList.size() > 9){
					iter.service.Staff_Member10__c = iter.memberList[9].Id;
				} 
				
				iter.service.Event__c = newEvent.Id;
				iter.service.Service_Date__c = newEvent.Date__c;
				serviceSFList.add(iter.service);
				Integer quantity = Integer.valueOf(iter.quantity);
				for (Integer i=1; i<quantity; i++){
					
					Service__c clonedService = iter.service.clone(false, true, false, false);
					serviceSFList.add(clonedService);
				}
			}
		}
		
		insert serviceSFList;
		
		return new PageReference('/'+caseId);
	}
	
	public PageReference saveAndNew(){
		
		save();
		
		newEvent = new Event__c();
		newService = new Service__c();
		staffMembers = new List<Contact>();
		serviceList = new List<ServiceWrapper>();
		
		PageReference pr = new PageReference('/apex/sff_logservices');
		pr.getParameters().put('cid', ApexPages.currentPage().getParameters().get('cid')); 
		pr.setRedirect(true);
		
		return pr;
		
	}
	
	public void saveAndStay(){
		
		System.debug('notes '+newEvent.Event_Note__c);
		
		if ((newEvent.Event_Note__c != '' && newEvent.Event_Note__c != null) || serviceList.size() > 0){
			
			save();
			lastSaved = Datetime.now();
		
			lastSavedStr = lastSaved.format('M/d/yyy h:mm a');
			
		}
		
		
		System.debug(lastSavedStr);
	}
	
	public void createBackup(String eventId){
		
		//edit
		if (eventId != null){
			
			eventBackup = newEvent.clone();
			
		}
		else{
			
			eventBackup = null;
			
		}	
		
	}
	
	
	public void rollbackToBackup(){
		
		//edit
		if (eventid != null){
			
			eventBackup.id = newEvent.id;
			newEvent = eventBackup;
			update newEvent;
			
		}
		else{
			
			if (newEvent.id != null){
				
				delete [select id from Service__c where Event__c =: newEvent.Id];
				delete newEvent;
			}	
			
		}
		
		
	}
	
	public PageReference cancel(){
		
		rollbackToBackup();
		
		return new PageReference('/'+caseId);
		
	}
	
	public class ServiceWrapper{
		
		public Service__c service {get;set;}
		public List<Contact> memberList {get;set;}
		public String quantity {get;set;}
	}
}