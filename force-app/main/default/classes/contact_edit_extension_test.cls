@istest
private class contact_edit_extension_test {
	
	public static testmethod void contact_edit_extension_test () {
		string recordtypeid = [select id from Recordtype where name = 'Direct Service Client' limit 1].id;
		Contact con = new Contact(lastname ='iamtest', firstname='testname', recordtypeid=recordtypeid);
		insert con;
			
		apexpages.standardcontroller sc = new apexpages.standardcontroller(con);
		contact_edit_Extension ext = new contact_edit_Extension(sc);
		Pagereference p = Page.contact_edit;
		
		
		Contact con1 = new Contact(firstname='juan',Lastname='Smith', recordtypeid = recordtypeid);
		Contact con2 = new Contact(firstname='john',Lastname='test', recordtypeid = recordtypeid);
		Contact con3 = new Contact(firstname='john',Lastname='test',birthdate=system.today(), recordtypeid = recordtypeid);
		list<contact> coninserts = new list<contact>{con1,con2,con3};
		insert coninserts;
				
		ext.con.FirstName = '';
		ext.con.LastName = '';
		ext.con.Birthdate = null;
		ext.checkBlankFields();
		
		ext.con.FirstName = 'john';
		ext.con.lastname = 'test';
		ext.con.Birthdate = system.today();
		ext.checkForDupes();
		
		ext.savecontact();
	}	
}