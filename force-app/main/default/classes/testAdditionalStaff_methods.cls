@isTest
private class testAdditionalStaff_methods {

	static testMethod void updateProbonoFirmOnRelatedCaseWithPrimaryStaffAndLawFirmOnInsert(){
		
		RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		
		List<Account> accounts = new List<Account>();
		Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
		primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(primaryLawFirm);
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account anotherLawFirm = new Account(Name = 'anotherLawFirm');
		anotherLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirm);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase', LastName = 'pimaryOnCase', Law_Firm__c = primaryLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(primaryOnCase);
		Contact additionalStaffContact = new Contact(FirstName = 'additionalStaff', LastName = 'additionalStaff', Law_Firm__c = aLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContact);
		Contact additionalStaffContactPrimary = new Contact(FirstName = 'additionalStaffPrimary',LastName = 'additionalStaffPrimary', Law_Firm__c = anotherLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactPrimary);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
		insert myCase;
		
		List<Additional_Staff__c> additionalStaffList = new List<Additional_Staff__c>();
		Additional_Staff__c staffPrimary = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContactPrimary.Id, Primary_Staff_on_Case__c = true);
		additionalStaffList.add(staffPrimary);
		Additional_Staff__c staff = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContact.Id, Primary_Staff_on_Case__c = false);
		additionalStaffList.add(staff);
		
		Test.startTest();
			insert additionalStaffList;
		Test.stopTest();
		
		myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
		system.assertEquals(primaryLawFirm.Id, myCase.Probono_Firm__c);
	}
	
	static testMethod void updateProbonoFirmOnRelatedCaseWithoutPrimaryStaffNorLawFirmOnInsert(){
		
		RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		List<Account> accounts = new List<Account>();
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account anotherLawFirm = new Account(Name = 'anotherLawFirm');
		anotherLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirm);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase', LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact additionalStaffContact = new Contact(FirstName = 'additionalStaff', LastName = 'additionalStaff', Law_Firm__c = aLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContact);
		Contact additionalStaffContactPrimary = new Contact(FirstName = 'additionalStaffPrimary',LastName = 'additionalStaffPrimary', Law_Firm__c = anotherLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactPrimary);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		insert myCase;

		List<Additional_Staff__c> additionalStaffList = new List<Additional_Staff__c>();
		Additional_Staff__c staffPrimary = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContactPrimary.Id, Primary_Staff_on_Case__c = true);
		additionalStaffList.add(staffPrimary);
		Additional_Staff__c staff = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContact.Id, Primary_Staff_on_Case__c = false);
		additionalStaffList.add(staff);
		
		
		Test.startTest();
			insert additionalStaffList;
		Test.stopTest();
		
		myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
		system.assertEquals(anotherLawFirm.Id, myCase.Probono_Firm__c);
		
	}
	
	static testMethod void updateProbonoFirmOnRelatedCaseWithPrimaryStaffAndLawFirmOnUpdate(){
		
		RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		
		List<Account> accounts = new List<Account>();
		Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
		primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(primaryLawFirm);
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account anotherLawFirm = new Account(Name = 'anotherLawFirm');
		anotherLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirm);
		Account aLawFirmForUpdate = new Account(Name = 'aLawFirmForUpdate');
		aLawFirmForUpdate.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirmForUpdate);
		Account anotherLawFirmForUpdate = new Account(Name = 'anotherLawFirmForUpdate');
		anotherLawFirmForUpdate.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirmForUpdate);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase', LastName = 'pimaryOnCase', Law_Firm__c = primaryLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(primaryOnCase);
		Contact additionalStaffContact = new Contact(FirstName = 'additionalStaff',LastName = 'additionalStaff', Law_Firm__c = aLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContact);
		Contact additionalStaffContactPrimary = new Contact(FirstName = 'additionalStaffPrimary',LastName = 'additionalStaffPrimary', Law_Firm__c = anotherLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactPrimary);
		Contact additionalStaffContactForUpdate = new Contact(FirstName = 'additionalStaffContactForUpdate', LastName = 'additionalStaffContactForUpdate', Law_Firm__c = aLawFirmForUpdate.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactForUpdate);
		Contact additionalStaffContactPrimaryForUpdate = new Contact(FirstName = 'additionalStaffContactPrimaryForUpdate',LastName = 'additionalStaffContactPrimaryForUpdate', Law_Firm__c = anotherLawFirmForUpdate.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactPrimaryForUpdate);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
		insert myCase;
		
		List<Additional_Staff__c> additionalStaffList = new List<Additional_Staff__c>();
		Additional_Staff__c staffPrimary = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContactPrimary.Id, Primary_Staff_on_Case__c = true);
		additionalStaffList.add(staffPrimary);
		Additional_Staff__c staff = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContact.Id, Primary_Staff_on_Case__c = false);
		additionalStaffList.add(staff);
		insert additionalStaffList;
		
		staffPrimary.Contact__c = additionalStaffContactPrimaryForUpdate.Id;
		staff.Contact__c = additionalStaffContactForUpdate.Id;
		
		Test.startTest();
			update additionalStaffList;
		Test.stopTest();
		
		myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
		system.assertEquals(primaryLawFirm.Id, myCase.Probono_Firm__c);
	}
	
	static testMethod void updateProbonoFirmOnRelatedCaseWithoutPrimaryStaffNorLawFirmOnUpdate(){
		
		RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		List<Account> accounts = new List<Account>();
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account anotherLawFirm = new Account(Name = 'anotherLawFirm');
		anotherLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirm);
		Account aLawFirmForUpdate = new Account(Name = 'aLawFirmForUpdate');
		aLawFirmForUpdate.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirmForUpdate);
		Account anotherLawFirmForUpdate = new Account(Name = 'anotherLawFirmForUpdate');
		anotherLawFirmForUpdate.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirmForUpdate);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact additionalStaffContact = new Contact(FirstName = 'additionalStaff',LastName = 'additionalStaff', Law_Firm__c = aLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContact);
		Contact anotherAdditionalStaffContact = new Contact(FirstName = 'anotherAdditionalStaffContact', LastName = 'anotherAdditionalStaffContact', Law_Firm__c = anotherLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(anotherAdditionalStaffContact);
		Contact additionalStaffContactForUpdate = new Contact(FirstName = 'additionalStaffContactForUpdate',LastName = 'additionalStaffContactForUpdate', Law_Firm__c = aLawFirmForUpdate.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(additionalStaffContactForUpdate);
		Contact anotherAdditionalStaffContactForUpdate = new Contact(FirstName = 'anotherAdditionalStaffContactForUpdate',LastName = 'anotherAdditionalStaffContactForUpdate', Law_Firm__c = anotherLawFirmForUpdate.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(anotherAdditionalStaffContactForUpdate);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		insert myCase;

		List<Additional_Staff__c> additionalStaffList = new List<Additional_Staff__c>();
		Additional_Staff__c aStaff = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = additionalStaffContact.Id, Primary_Staff_on_Case__c = false);
		additionalStaffList.add(aStaff);
		Additional_Staff__c anotherStaff = new Additional_Staff__c(Case__c = myCase.Id, Contact__c = anotherAdditionalStaffContact.Id, Primary_Staff_on_Case__c = false);
		additionalStaffList.add(anotherStaff);
		insert additionalStaffList;
		
		aStaff.Contact__c = additionalStaffContactForUpdate.Id;
		anotherStaff.Contact__c = anotherAdditionalStaffContactForUpdate.Id;
		
		Test.startTest();
			update additionalStaffList;
		Test.stopTest();
		
		myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
		system.assert(additionalStaffContactForUpdate.Law_Firm__c == myCase.Probono_Firm__c || anotherAdditionalStaffContactForUpdate.Law_Firm__c == myCase.Probono_Firm__c);
		
	}

}