@isTest
private class CaseC_MethodsTest {
    
    static testMethod void updateContactsWithFirmOrSchoolQuantOnCaseOnInsert() {
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];	
        Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
        primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
        insert primaryLawFirm;
        
        RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
        List<Contact> contactList = new List<Contact>();
        Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
        contactList.add(contactForCase);
        Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase', LastName = 'pimaryOnCase', Law_Firm__c = primaryLawFirm.Id, RecordTypeId = contactRecordType.Id);
        contactList.add(primaryOnCase);
        insert contactList;
        
        RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
        Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
        myCase.Contact__c = contactForCase.Id;
        myCase.RecordTypeId = caseRecordType.Id;
        myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
        
        Test.startTest();
        insert myCase;
        Test.stopTest();
        
        myCase = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
        system.assertEquals(1, myCase.Quant_Contacts_Law_Firm_School__c);
    }
    
    static testMethod void updateContactsWithFirmOrSchoolQuantOnCaseOnAdditionalStaff() {
        Test.startTest();
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];	
        Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
        primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
        insert primaryLawFirm;
        
        RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
        List<Contact> contactList = new List<Contact>();
        Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
        contactList.add(contactForCase);
        Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase', LastName = 'pimaryOnCase',  RecordTypeId = contactRecordType.Id);
        
        Contact additionalOnCase = new Contact(FirstName = 'addOnCase', LastName = 'addOnCase', RecordTypeId = contactRecordType.Id, Law_School__c = primaryLawFirm.Id);
        
        contactList.add(primaryOnCase);
        contactList.add(additionalOnCase);
        
        insert contactList;
        
        RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
        Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
        myCase.Contact__c = contactForCase.Id;
        myCase.RecordTypeId = caseRecordType.Id;
        
        insert myCase;
        
        Case__c result1 = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
        
        Additional_Staff__c addStaff = new Additional_Staff__c();
        addStaff.Case__c = myCase.Id;
        addStaff.Contact__c = additionalOnCase.Id;
        addStaff.Primary_Staff_on_Case__c = true;
        insert addStaff;
        
        Test.stopTest(); 

        Case__c result2 = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
        System.assertEquals(0, result1.Quant_Contacts_Law_Firm_School__c);
        System.assertEquals(1, result2.Quant_Contacts_Law_Firm_School__c);
    }
    
    static testMethod void updateContactsWithFirmOrSchoolQuantOnCaseOnUpdate() {
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];	
        Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
        primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
        insert primaryLawFirm;
        
        RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
        List<Contact> contactList = new List<Contact>();
        Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
        contactList.add(contactForCase);
        Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase',LastName = 'pimaryOnCase', Law_Firm__c = primaryLawFirm.Id, RecordTypeId = contactRecordType.Id);
        contactList.add(primaryOnCase);
        insert contactList;
        
        RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
        Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
        myCase.Contact__c = contactForCase.Id;
        myCase.RecordTypeId = caseRecordType.Id;
        insert myCase;
        
        myCase = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
        system.assertEquals(0, myCase.Quant_Contacts_Law_Firm_School__c);
        
        myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
        
        Test.startTest();
        update myCase;	
        Test.stopTest();
        
        myCase = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
        system.assertEquals(1, myCase.Quant_Contacts_Law_Firm_School__c);
    }
    @isTest
    public static void testCreateNights(){
        Case__c case1 = new Case__c();
        case1.Matter_Type_Detail__c = 'Child Support';
        case1.Type_of_Matter__c = 'Follow-up Case Type';
        
        CaseC_Methods.newNights = new List<Night__c>();
        Date todayDate = Date.today();
        Date tomorrowDate = Date.today().addDays(1);
        insert case1;
        CaseC_Methods.createNights(case1, todayDate, tomorrowDate);
        System.assertEquals(1, CaseC_Methods.newNights.size());
    }	
    
    @isTest
    public static void testCreateNightsNew(){
        CaseC_Methods.newNights = new List<Night__c>();
        Date start =  Date.today();
        
        Case__c case1 = new Case__c();
        case1.Matter_Type_Detail__c = 'Child Support';
        case1.Type_of_Matter__c = 'Follow-up Case Type';
        case1.Early_Close_Date__c = Date.today().addDays(1);
        case1.Case_Open_Date__c = Date.today().addDays(3);
        
        Case__c case2 = new Case__c();
        case2.Matter_Type_Detail__c = 'Child Support';
        case2.Type_of_Matter__c = 'Follow-up Case Type';
        case2.Early_Close_Date__c = Date.today().addDays(1);
        case2.Case_Open_Date__c = Date.today().addDays(3);
        
        CaseC_Methods.createNightsNew(case1, start, case2);
        System.assertEquals(1, CaseC_Methods.newNights.size());
    }
    
}