public with sharing class AdditionalStaff_Methods {
    
    public static Boolean readyToUpdate = true;
    
    public static void afterUpsert(Map<Id, Additional_Staff__c> newMap) {
        
        List<Case__c> updateCases = new List<Case__c>();
        List<Additional_Staff__c> updateStaff = new List<Additional_Staff__c>();
        
        Map<Id, Id> casePContact = new Map<Id, Id>();
        
        Set<Id> caseSet = new Set<Id>();
        
        for (Additional_Staff__c aStaff : newMap.values()){	
            caseSet.add(aStaff.Case__c);
            if(aStaff.Primary_Staff_on_Case__c && !casePContact.containsKey(aStaff.Case__c)){
                casePContact.put(aStaff.Case__c, aStaff.Contact__c);
            }
        }
        Map<Id, Case__c> cases = new Map<Id, Case__c>([SELECT Id, Primary_Staff_on_Case__c FROM Case__c WHERE Id in :caseSet]);
        Map<Id, Additional_Staff__c> currentStaff = new Map<Id, Additional_Staff__c>([SELECT Id, Case__c, Contact__c, Contact__r.Law_Firm__c, Contact__r.Law_School__c,  Primary_Staff_on_Case__c  FROM Additional_Staff__c WHERE Case__c in :caseSet]);
        for(Case__c aCase: cases.values()){
            if(casePContact.containsKey(aCase.Id)){
                Id contactId = casePContact.get(aCase.Id);
                if(aCase.Primary_Staff_on_Case__c != contactId){
                    aCase.Primary_Staff_on_Case__c = contactId;
                    updateCases.add(aCase);
                }                
            }
        }
        for(Additional_Staff__c cStaff: currentStaff.values()){
            Case__c cas = cases.get(cStaff.Case__c);
            if(cStaff.Primary_Staff_on_Case__c){
                if(cStaff.Contact__c != cas.Primary_Staff_on_Case__c){
                    cStaff.Primary_Staff_on_Case__c = false;
                    updateStaff.add(cStaff);
                }
            }else{
                if(cStaff.Contact__c == cas.Primary_Staff_on_Case__c){
                    cStaff.Primary_Staff_on_Case__c = true;
                    updateStaff.add(cStaff);
                }
            }
        }
        if(updateCases.size() > 0){
            System.debug('CASES: ' + updateCases);
            update updateCases;
        }
        if(updateStaff.size() > 0){
            System.debug('STAFF: ' + updateStaff);
            update updateStaff;
        }
    }
    
    public static void removePrimaryStaffOnCase(List<Additional_Staff__c> staffs){
        Set<Id> staffContactIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();
        for(Additional_Staff__c staff: staffs){
            if(staff.Primary_Staff_on_Case__c){
				caseIds.add(staff.Case__c);
            	staffContactIds.add(staff.Contact__c);
            }
        }
        List<Case__c> cases = [SELECT Id, Primary_Staff_on_Case__c FROM Case__c WHERE Primary_Staff_on_Case__c IN :staffContactIds AND Id IN :caseIds];
        
        for(Case__c cas: cases){
            cas.Primary_Staff_on_Case__c = null;
        }
        update(cases);
    }
    
    public static void preventDuplicateAdditionalStaff(List<Additional_Staff__c> staff){
        Set<Id> cases = new Set<Id>();
        for (Additional_Staff__c aStaff : staff){	
            cases.add(aStaff.Case__c);
        }
        List<Additional_Staff__c> currentStaff = [SELECT Id, Case__c , Contact__c FROM Additional_Staff__c WHERE Case__c in :cases];
        
        Map<Id,Map<Id, Additional_Staff__c>> caseContactsStaff = new Map<Id,Map<Id, Additional_Staff__c>>();
        for(Additional_Staff__c cStaff: currentStaff){
            if(!caseContactsStaff.containsKey(cStaff.Case__c)){
                caseContactsStaff.put(cStaff.Case__c, new Map<Id, Additional_Staff__c>());
            }
            Map<Id, Additional_Staff__c> contactStaff = caseContactsStaff.get(cStaff.Case__c);
            contactStaff.put(cStaff.Contact__c, cStaff);
        }
        for (Additional_Staff__c aStaff : staff){
            Map<Id, Additional_Staff__c> contactStaff = caseContactsStaff.get(aStaff.Case__c);
            if(contactStaff != NULL && contactStaff.containsKey(aStaff.Contact__c)){
                Additional_Staff__c old = contactStaff.get(aStaff.Contact__c);
                if(old.Id != aStaff.Id){
                    aStaff.addError('Staff member already working on this case');
                }
            }
        }        
    }	
}