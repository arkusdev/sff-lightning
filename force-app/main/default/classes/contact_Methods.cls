public with sharing class contact_Methods {

	public static map<string,string> zipToCountyCityMap = getZipToCountyCityMap();

	public static void afterUpdate (map<id,Contact> newMap, map<id,Contact> oldMap){
		list<Contact> contacts = [select id, project__c,(select id, contact__c, project__c, recordtypeid from Cases__r) from Contact where id IN :newMap.keySet()];
		
		list<Case__c> caseList = new list<Case__c>();
		set<id> applicableCaseRecordTypes = new set<id>();
		
		for (Contact c : contacts) {
			if (newMap.get(c.id).project__c != oldMap.get(c.id).project__c) {
				for (Case__c cas : c.cases__r) {
					cas.project__c = c.project__c;
					caseList.add(cas);
				}
			}
		}
		update caseList;
	}
	
	public static void updateProbonoFirmOnCases(Map<Id, Contact> newMap){
		
		List<Case__c> cases = [SELECT Probono_Firm__c, Probono_Firm_Declarer__c FROM Case__c WHERE Probono_Firm_Declarer__c IN: newMap.keySet()];
		for (Case__c oneCase : cases) {
			if (newMap.get(oneCase.Probono_Firm_Declarer__c).Law_Firm__c != null) {
				oneCase.Probono_Firm__c = newMap.get(oneCase.Probono_Firm_Declarer__c).Law_Firm__c;
			} else if (newMap.get(oneCase.Probono_Firm_Declarer__c).Law_School__c != null){
				oneCase.Probono_Firm__c = newMap.get(oneCase.Probono_Firm_Declarer__c).Law_School__c;		
			}
		}
		update cases;
	}
	
	@future
	public static void updateContactsWithFirmOrSchoolQuantOnCase(Set<Id> newList){
    	
    	List<Additional_Staff__c> additionalStaffContact = [SELECT Case__c FROM Additional_Staff__c WHERE Contact__c IN: newList];
		List<Id> casesIds = new List<Id>();
		for (Additional_Staff__c staff : additionalStaffContact) {
			casesIds.add(staff.Case__c);
		}
		
		Map<Id, Case__c> caseMap = new Map<Id, Case__c>([SELECT Quant_Contacts_Law_Firm_School__c, Primary_Staff_on_Case__c, Primary_Staff_on_Case__r.Law_Firm__c, Primary_Staff_on_Case__r.Law_School__c FROM Case__c WHERE Id IN: casesIds OR Primary_Staff_on_Case__c IN: newList]);
    	
    	List<Additional_Staff__c> addStaff = [SELECT Contact__c, Contact__r.Law_Firm__c, Contact__r.Law_School__c, Case__c FROM Additional_Staff__c WHERE Case__c IN :caseMap.keySet()];
            
        Map<Id, List<Additional_Staff__c>> mapCaseAddStaff = new Map<Id, List<Additional_Staff__c>>();
        
        for (Id caseId : caseMap.keySet()){
            List<Additional_Staff__c> auxStaffList = new List<Additional_Staff__c>();
            for (Additional_Staff__c staff : addStaff) {
                if(staff.Case__c == caseId){
                    auxStaffList.add(staff);
                }
            }
            mapCaseAddStaff.put(caseId, auxStaffList);
        }
        
        List<Case__c> casesToUpdate = new List<Case__c>();	
    	for (Case__c myCase : caseMap.values()){
    		
    		Integer qty = 0;
    		
    		List<Additional_Staff__c> additionalStaff = mapCaseAddStaff.get(myCase.Id);
    		Set<Id> contactIds = new Set<Id>();
    		for (Additional_Staff__c staff : additionalStaff) {
    			contactIds.add(staff.Contact__c);
    		}
    		
    		if(myCase.Primary_Staff_on_Case__c != null && (!contactIds.contains(myCase.Primary_Staff_on_Case__c))) {
    			if (myCase.Primary_Staff_on_Case__r.Law_Firm__c != null || myCase.Primary_Staff_on_Case__r.Law_School__c != null){
    				qty++;
    			}
    		}
    		for (Additional_Staff__c staff : additionalStaff) {
    			if (staff.Contact__r.Law_Firm__c != null || staff.Contact__r.Law_School__c != null){
    				qty++;
    			}
    		}
    		myCase.Quant_Contacts_Law_Firm_School__c = qty;  
    		casesToUpdate.add(myCase);	
    	}
    	update casesToUpdate;
    }
	
	public static void beforeInsert (list<Contact> newlist) {
		for (integer i =0; i<newlist.size();i++) {
			if (newlist[i].mailingpostalcode != null) {
				if (zipToCountyCityMap.containskey(newlist[i].mailingpostalcode)){
					newlist[i].Borough_of_Residence__c = zipToCountyCityMap.get(newlist[i].mailingpostalcode).substringbefore(':');
					newlist[i].mailingcity = zipToCountyCityMap.get(newlist[i].mailingpostalcode).substringafter(':');
				}
			}
		}
	}
	
	public static void beforeUpdate (list<Contact> newlist, map<id,Contact> oldmap) {
		for (integer i =0; i<newlist.size();i++) {
			if (newlist[i].mailingpostalcode != oldmap.get(newlist[i].id).mailingpostalcode) {
				if (zipToCountyCityMap.containskey(newlist[i].mailingpostalcode)){
					newlist[i].Borough_of_Residence__c = zipToCountyCityMap.get(newlist[i].mailingpostalcode).substringbefore(':');
					newlist[i].mailingcity = zipToCountyCityMap.get(newlist[i].mailingpostalcode).substringafter(':');
				}
			}
		}
	}
	
	public static map<string,string> getZipToCountyCityMap(){
		map<string,string> tempmap = new map<string,string>();
		tempmap.put('10001','Manhattan:New York');
		tempmap.put('10002','Manhattan:New York');
		tempmap.put('10003','Manhattan:New York');
		tempmap.put('10004','Manhattan:New York');
		tempmap.put('10005','Manhattan:New York');
		tempmap.put('10006','Manhattan:New York');
		tempmap.put('10007','Manhattan:New York');
		tempmap.put('10009','Manhattan:New York');
		tempmap.put('10010','Manhattan:New York');
		tempmap.put('10011','Manhattan:New York');
		tempmap.put('10012','Manhattan:New York');
		tempmap.put('10013','Manhattan:New York');
		tempmap.put('10014','Manhattan:New York');
		tempmap.put('10016','Manhattan:New York');
		tempmap.put('10017','Manhattan:New York');
		tempmap.put('10018','Manhattan:New York');
		tempmap.put('10019','Manhattan:New York');
		tempmap.put('10020','Manhattan:New York');
		tempmap.put('10021','Manhattan:New York');
		tempmap.put('10022','Manhattan:New York');
		tempmap.put('10023','Manhattan:New York');
		tempmap.put('10024','Manhattan:New York');
		tempmap.put('10025','Manhattan:New York');
		tempmap.put('10026','Manhattan:New York');
		tempmap.put('10027','Manhattan:New York');
		tempmap.put('10028','Manhattan:New York');
		tempmap.put('10029','Manhattan:New York');
		tempmap.put('10030','Manhattan:New York');
		tempmap.put('10031','Manhattan:New York');
		tempmap.put('10032','Manhattan:New York');
		tempmap.put('10033','Manhattan:New York');
		tempmap.put('10034','Manhattan:New York');
		tempmap.put('10035','Manhattan:New York');
		tempmap.put('10036','Manhattan:New York');
		tempmap.put('10037','Manhattan:New York');
		tempmap.put('10038','Manhattan:New York');
		tempmap.put('10039','Manhattan:New York');
		tempmap.put('10040','Manhattan:New York');
		tempmap.put('10044','Manhattan:New York');
		tempmap.put('10128','Manhattan:New York');
		tempmap.put('10280','Manhattan:New York');
		tempmap.put('10301','Richmond:Staten Island');
		tempmap.put('10302','Richmond:Staten Island');
		tempmap.put('10303','Richmond:Staten Island');
		tempmap.put('10304','Richmond:Staten Island');
		tempmap.put('10305','Richmond:Staten Island');
		tempmap.put('10306','Richmond:Staten Island');
		tempmap.put('10307','Richmond:Staten Island');
		tempmap.put('10308','Richmond:Staten Island');
		tempmap.put('10309','Richmond:Staten Island');
		tempmap.put('10310','Richmond:Staten Island');
		tempmap.put('10312','Richmond:Staten Island');
		tempmap.put('10314','Richmond:Staten Island');
		tempmap.put('10451','Bronx:Bronx');
		tempmap.put('10452','Bronx:Bronx');
		tempmap.put('10453','Bronx:Bronx');
		tempmap.put('10454','Bronx:Bronx');
		tempmap.put('10455','Bronx:Bronx');
		tempmap.put('10456','Bronx:Bronx');
		tempmap.put('10457','Bronx:Bronx');
		tempmap.put('10458','Bronx:Bronx');
		tempmap.put('10459','Bronx:Bronx');
		tempmap.put('10460','Bronx:Bronx');
		tempmap.put('10461','Bronx:Bronx');
		tempmap.put('10462','Bronx:Bronx');
		tempmap.put('10463','Bronx:Bronx');
		tempmap.put('10464','Bronx:Bronx');
		tempmap.put('10465','Bronx:Bronx');
		tempmap.put('10466','Bronx:Bronx');
		tempmap.put('10467','Bronx:Bronx');
		tempmap.put('10468','Bronx:Bronx');
		tempmap.put('10469','Bronx:Bronx');
		tempmap.put('10470','Bronx:Bronx');
		tempmap.put('10471','Bronx:Bronx');
		tempmap.put('10472','Bronx:Bronx');
		tempmap.put('10473','Bronx:Bronx');
		tempmap.put('10474','Bronx:Bronx');
		tempmap.put('10475','Bronx:Bronx');
		tempmap.put('11001','Queens:Floral Park');
		tempmap.put('11004','Queens:Glen Oaks');
		tempmap.put('11005','Queens:Floral Park');
		tempmap.put('11040','Queens:New Hyde Park');
		tempmap.put('11101','Queens:Long Island City');
		tempmap.put('11102','Queens:Astoria');
		tempmap.put('11103','Queens:Astoria');
		tempmap.put('11104','Queens:Sunnyside');
		tempmap.put('11105','Queens:Astoria');
		tempmap.put('11106','Queens:Astoria');
		tempmap.put('11201','Kings:Brooklyn');
		tempmap.put('11203','Kings:Brooklyn');
		tempmap.put('11204','Kings:Brooklyn');
		tempmap.put('11205','Kings:Brooklyn');
		tempmap.put('11206','Kings:Brooklyn');
		tempmap.put('11207','Kings:Brooklyn');
		tempmap.put('11208','Kings:Brooklyn');
		tempmap.put('11209','Kings:Brooklyn');
		tempmap.put('11210','Kings:Brooklyn');
		tempmap.put('11211','Kings:Brooklyn');
		tempmap.put('11212','Kings:Brooklyn');
		tempmap.put('11213','Kings:Brooklyn');
		tempmap.put('11214','Kings:Brooklyn');
		tempmap.put('11215','Kings:Brooklyn');
		tempmap.put('11216','Kings:Brooklyn');
		tempmap.put('11217','Kings:Brooklyn');
		tempmap.put('11218','Kings:Brooklyn');
		tempmap.put('11219','Kings:Brooklyn');
		tempmap.put('11220','Kings:Brooklyn');
		tempmap.put('11221','Kings:Brooklyn');
		tempmap.put('11222','Kings:Brooklyn');
		tempmap.put('11223','Kings:Brooklyn');
		tempmap.put('11224','Kings:Brooklyn');
		tempmap.put('11225','Kings:Brooklyn');
		tempmap.put('11226','Kings:Brooklyn');
		tempmap.put('11228','Kings:Brooklyn');
		tempmap.put('11229','Kings:Brooklyn');
		tempmap.put('11230','Kings:Brooklyn');
		tempmap.put('11231','Kings:Brooklyn');
		tempmap.put('11232','Kings:Brooklyn');
		tempmap.put('11233','Kings:Brooklyn');
		tempmap.put('11234','Kings:Brooklyn');
		tempmap.put('11235','Kings:Brooklyn');
		tempmap.put('11236','Kings:Brooklyn');
		tempmap.put('11237','Kings:Brooklyn');
		tempmap.put('11238','Kings:Brooklyn');
		tempmap.put('11239','Kings:Brooklyn');
		tempmap.put('11354','Queens:Flushing');
		tempmap.put('11355','Queens:Flushing');
		tempmap.put('11356','Queens:College Point');
		tempmap.put('11357','Queens:Whitestone');
		tempmap.put('11358','Queens:Flushing');
		tempmap.put('11360','Queens:Bayside');
		tempmap.put('11361','Queens:Bayside');
		tempmap.put('11362','Queens:Little Neck');
		tempmap.put('11363','Queens:Little Neck');
		tempmap.put('11364','Queens:Oakland Gardens');
		tempmap.put('11365','Queens:Fresh Meadows');
		tempmap.put('11366','Queens:Fresh Meadows');
		tempmap.put('11367','Queens:Flushing');
		tempmap.put('11368','Queens:Corona');
		tempmap.put('11369','Queens:East Elmhurst');
		tempmap.put('11370','Queens:East Elmhurst');
		tempmap.put('11372','Queens:Jackson Heights');
		tempmap.put('11373','Queens:Elmhurst');
		tempmap.put('11374','Queens:Rego Park');
		tempmap.put('11375','Queens:Forest Hills');
		tempmap.put('11377','Queens:Woodside');
		tempmap.put('11378','Queens:Maspeth');
		tempmap.put('11379','Queens:Middle Village');
		tempmap.put('11385','Queens:Ridgewood');
		tempmap.put('11411','Queens:Cambria Heights');
		tempmap.put('11412','Queens:St. Albans');
		tempmap.put('11413','Queens:Springfield Gardens');
		tempmap.put('11414','Queens:Howard Beach');
		tempmap.put('11415','Queens:Kew Gardens');
		tempmap.put('11416','Queens:Ozone Park');
		tempmap.put('11417','Queens:Ozone Park');
		tempmap.put('11418','Queens:Richmond Hill');
		tempmap.put('11419','Queens:South Richmond Hill');
		tempmap.put('11420','Queens:South Ozone Park');
		tempmap.put('11421','Queens:Woodhaven');
		tempmap.put('11422','Queens:Rosedale');
		tempmap.put('11423','Queens:Hollis');
		tempmap.put('11426','Queens:Bellerose');
		tempmap.put('11427','Queens:Queens Village');
		tempmap.put('11428','Queens:Queens Village');
		tempmap.put('11429','Queens:Queens Village');
		tempmap.put('11430','Queens:Jamaica');
		tempmap.put('11432','Queens:Jamaica');
		tempmap.put('11433','Queens:Jamaica');
		tempmap.put('11434','Queens:Jamaica');
		tempmap.put('11435','Queens:Jamaica');
		tempmap.put('11436','Queens:South Ozone Park');
		tempmap.put('11691','Queens:Far Rockaway');
		tempmap.put('11692','Queens:Arverne');
		tempmap.put('11693','Queens:Far Rockaway');
		tempmap.put('11694','Queens:Rockaway Park');
		tempmap.put('11695','Queens:Far Rockaway');
		tempmap.put('11697','Queens:Breezy Point');
		return tempmap;
	}

    public static void beforeDelete(Map<Id,Contact> contacts){
        Set<Id> contactIds = contacts.keySet();
        List<Additional_Staff__c> staff = [SELECT Id FROM Additional_Staff__c WHERE Contact__c in :contactIds];
        delete staff;
    }
}