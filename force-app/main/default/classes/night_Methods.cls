public with sharing class night_Methods {

	public static void afterInsert(list<Night__c> newlist){
		cleanupInsert(newlist);
	}
	
	//for those rare cases where today's night record for a case already has an attendance set and a duplicate, with no attendance would have been created.
	public static void cleanupInsert(list<Night__c> scope){
		map<id,Case__c> caseMap = new map<id,Case__c>();
		set<id> deleteSet = new set<id>();
		for (Night__c n : scope){
			caseMap.put(n.case__c,null);
		}
		
		caseMap = new map<id, Case__c>([select id, (select id from Nights__r where Date_Service_was_Provided__c = :system.today()) from Case__c where id IN :caseMap.keyset()]);
		
		for (Night__c n : scope){
			if (n.Date_Service_was_Provided__c == system.today() && caseMap.get(n.case__c).nights__r.size()>1) deleteSet.add(n.id);
		}
		
		list<id> deleteIds = new list<id>();
		deleteIds.addall(deleteSet);
		database.delete(deleteIds);
	}
}