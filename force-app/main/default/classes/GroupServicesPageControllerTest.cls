@isTest
private class GroupServicesPageControllerTest {
	
	@isTest static void testConstructor() {
		
		Id serviceRecordtypeId = Schema.SObjectType.Service__c.getRecordTypeInfosByName().get('Support Group').getRecordTypeId();
		Service__c testService = new Service__c();
		testService.RecordTypeId = serviceRecordtypeId;

		insert testService;

		String sessions = '[{"attendanceStatus":"Present","metroCard":false,"service":"' + testService.Id + '"}]';
		
		List<Session__c> sess = GroupServicesPageController.createSessions(testService.Id, 'testing sessions - note', String.valueOf(System.today()), sessions);

		GroupServicesPageController controller = new GroupServicesPageController(new ApexPages.StandardController(testService));
		
	}
	
	@isTest(SeeAllData = true) static void testGetAllActiveCases(){
		List<GroupServicesPageController.CaseContact> caseContacts = GroupServicesPageController.getAllActiveCases();

		System.assert(caseContacts.size() > 0);
	}

	@isTest(SeeAllData = true) static void testGetAllStaff(){
		RecordType rt = [Select Id, Name From RecordType Where Name =: 'Staff'];

		System.assert(rt != null);

		List<Contact> contacts = GroupServicesPageController.getAllStaff(rt.Id);

		System.assert(contacts != null);
		System.assert(contacts.size() > 0);
	}

	@isTest(SeeAllData = true) static void testCreateEvents(){
		Service__c service = createService();

		Test.startTest();
		String events = createJSONEvents(service);
		
		List<Event__c> ev = GroupServicesPageController.createEvents(service.Id, String.valueOf(System.today()), events);

		System.assert(ev != null);
		System.assertEquals(ev.size(), 10);

		Test.stopTest();
	}

	@isTest(SeeAllData = true) static void testCreateSessions(){
		Service__c service = createService();
		
		String events = createJSONEvents(service);
		
		List<Event__c> ev = GroupServicesPageController.createEvents(service.Id, String.valueOf(System.today()), events);

		Case__c c = createCase();

		String sessions = '[{"attendanceStatus":"Present","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"}]';
		
		Test.startTest();
        
		List<Session__c> sess = GroupServicesPageController.createSessions(service.Id, 'testing sessions - note', String.valueOf(System.today()), sessions);
		System.assert(sess != null);
		System.assertEquals(sess.size(), 1);

		List<Session__c> sessionsCreated = [SELECT Id, Case__c, Contact__c, Event__c, Metro_Card__c, Notes__c, Service__c, Session_Date__c, Session_Status__c 
											FROM Session__c
											Where Service__c =: service.Id 
											ORDER BY Session_Date__c ASC];

		System.assertEquals(sessionsCreated.size(), 10);
		System.assertEquals(sessionsCreated[0].Case__c, c.Id);
		System.assertEquals(sessionsCreated[1].Contact__c, c.Contact__c);
		System.assertEquals(sessionsCreated[0].Session_Status__c, 'Closed');
		System.assertEquals(sessionsCreated[2].Session_Status__c, 'Open');

		Test.stopTest();
	}
	
	@isTest(SeeAllData = true) static void testCreateMetrocardService(){
		Service__c service = createService();
		
		String events = createJSONEvents(service);
		
		List<Event__c> ev = GroupServicesPageController.createEvents(service.Id, String.valueOf(System.today()), events);

		Case__c c = createCase();

		String sessions = '[{"attendanceStatus":"Present","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"}]';
		
		List<Session__c> sess = GroupServicesPageController.createSessions(service.Id, 'testing sessions note', String.valueOf(System.today()), sessions);
		
		String metrocards = '[{"Service__c":"MetroCards (non-monetary)","Event__c":"'+ ev[0].Id +'","Case__c":"' + c.Id + '","Staff_Member__c":"' + c.Primary_Staff_on_Case__c + '"}]';
		
		System.debug(metrocards);
		GroupServicesPageController.createMetrocardService(metrocards);

		Test.startTest();
        
		List<Service__c> metroCardsCreated = [ SELECT Id, Service_Date__c, Service__c, Staff_Member__c 
												FROM Service__c 
												WHERE Service__c =: 'MetroCards (non-monetary)' 
												AND Event__c =: ev[0].Id
												AND Case__c =: c.Id
												ORDER BY Service_Date__c ASC];

		System.assertEquals(metroCardsCreated.size(), 1);

		Test.stopTest();
	}
	

	@isTest(SeeAllData=false) static void testCreateTwentyThousandSessions(){
		Service__c service = createService();
		
		String events = '[{"eventDate":"'+ String.valueOf(System.today()) +'","serviceId":"' + service.Id + '","eventNote":"test note"},';

		for(Integer i = 0; i < 600; i ++){
			events = events + '{"eventDate":"'+ String.valueOf(System.today().addDays(i)) +'","serviceId":"' + service.Id + '","eventNote":""},';
		}
		events = events + '{"eventDate":"'+ String.valueOf(System.today().addDays(601)) +'","serviceId":"' + service.Id + '","eventNote":""}]';

		List<Event__c> ev = GroupServicesPageController.createEvents(service.Id, String.valueOf(System.today()), events);

		Case__c c = createCase();

		String sessions = '[{"attendanceStatus":"Present","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"},';

		for(Integer i = 0; i < 40; i ++){
			sessions = sessions + '{"attendanceStatus":"","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"},';
		}
		sessions = sessions + '{"attendanceStatus":"","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"}]';
		
		Test.startTest();
        
		List<Session__c> sess = GroupServicesPageController.createSessions(service.Id, 'testing sessions - note', String.valueOf(System.today()), sessions);
		
		System.assert(sess != null);

		List<Session__c> sessionsCreated = [SELECT Id, Case__c, Contact__c, Event__c, Metro_Card__c, Notes__c, Service__c, Session_Date__c, Session_Status__c 
											FROM Session__c
											Where Service__c =: service.Id 
											ORDER BY Session_Date__c ASC];

		System.assert(sessionsCreated.size() > 40);

		Test.stopTest();
	}

	@isTest(SeeAllData = true) static void testGroupServiceBatch(){
		Service__c service = createService();
		
		String events = '[{"eventDate":"'+ String.valueOf(System.today()) +'","serviceId":"' + service.Id + '","eventNote":"test note"},';

		for(Integer i = 0; i < 99; i ++){
			events = events + '{"eventDate":"'+ String.valueOf(System.today().addDays(i)) +'","serviceId":"' + service.Id + '","eventNote":""},';
		}
		events = events + '{"eventDate":"'+ String.valueOf(System.today().addDays(100)) +'","serviceId":"' + service.Id + '","eventNote":""}]';

		List<Event__c> ev = GroupServicesPageController.createEvents(service.Id, String.valueOf(System.today()), events);

		Case__c c = createCase();

		String sessions = '[{"attendanceStatus":"Present","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"},' + 
						  '{"attendanceStatus":"","metroCard":false,"service":"' + service.Id + '","sessionCase":"' + c.Id + '","sessionContact":"' + c.Contact__c + '"}]';
		
		Test.startTest();

		Id testBatchId = Database.executeBatch(new GroupServicesBatch(service.Id, 'test batch', String.valueOf(System.today()), sessions));

		Test.stopTest();

	}

	public static String createJSONEvents(Service__c service){
		String events = '[{"eventDate":"'+ String.valueOf(System.today()) +'","serviceId":"' + service.Id + '","eventNote":"test note"},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(1)) +'","serviceId":"' + service.Id + '","eventNote":""},' + 
						'{"eventDate":"'+ String.valueOf(System.today().addDays(2)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(3)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(4)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(5)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(6)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(7)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(8)) +'","serviceId":"' + service.Id + '","eventNote":""},' +
						'{"eventDate":"'+ String.valueOf(System.today().addDays(9)) +'","serviceId":"' + service.Id + '","eventNote":""}]';
		return events;
	}

	public static Service__c createService(){
		Id serviceRecordtypeId = Schema.SObjectType.Service__c.getRecordTypeInfosByName().get('Support Group').getRecordTypeId();
		Service__c service = new Service__c();
		service.RecordTypeId = serviceRecordtypeId;
		service.Service__c = 'Advice (Legal)';
		service.SubType__c = 'Abuse & Neglect';
		service.Start_Date__c = System.today();
		service.End_Date__c = System.today().addDays(30);
		service.Frequency__c = 'Daily';
		service.Frequency_daily__c = 'Every day';

		insert service;

		return service;
	}

	public static Case__c createCase(){

		Contact con = createContact();
		Id caseRecordtypeId = Schema.SObjectType.Case__c.getRecordTypeInfosByName().get('Economic Empowerment').getRecordTypeId();

		Case__c c = new Case__c();
		c.RecordTypeId = caseRecordtypeId;
		c.Contact__c = createContact().Id;
		c.Project__c = 'Economic Empowerment Program';
		c.Type_of_Assistance__c = 'Economic Empowerment';
		c.Type_of_Matter__c = 'Economic';
		c.Matter_Type_Detail__c = 'Employment';
		c.Case_Open_Date__c = System.today();
		c.Primary_Staff_on_Case__c = createStaff().Id;

		insert c;

		return c;
	}

	public static Contact createContact(){
		Account acc = new Account(Name = 'Test account');
		insert acc;

		Id contactRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Direct Service Client').getRecordTypeId();
		Contact con = new Contact(AccountId = acc.Id, LastName = 'Tester', FirstName = 'Oktana', RecordTypeId = contactRecordtypeId);
		insert con;

		return con;
	}

	public static Contact createStaff(){
		Account acc = new Account(Name = 'Staff account');
		insert acc;

		Id staffRecordtypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
		Contact con = new Contact(AccountId = acc.Id, LastName = 'Tester', FirstName = 'Staff', RecordTypeId = staffRecordtypeId);

		insert con;

		return con;
	}
}