global with sharing class SFF_UnitData implements Comparable{
    
    public Event__c EventData{get;set;}
    public SFF_detailCase serviceItem{get;set;}
    public Date dateCompare{get;set;}
    public boolean isEvent{get;set;}
    public boolean wrap{get;set;}
    public boolean HaveImmigrationEconomic{get;set;} 
    public string  ImmigrationEconomic{get;set;} 
    public string  ValueImmigrationEconomic{get;set;}
    public string  TypeOfAssistance{get;set;}
    public string  TypeOfMatter{get;set;}
    
    public SFF_UnitData(){
    }
    public void initEvent(boolean isEventC,date serviceDateC, Event__c e){
        isEvent = isEventC;
        dateCompare = serviceDateC;
        EventData = new Event__c();
        EventData = e;
    }
    public void initService(boolean isEventC,String AttendanceStatusC,String nameSC,String OwnerServiceC, id idCaseC, id idServiceC,String serviceC,String typeServiceC,String nameC,date serviceDateC,String subtypeC,String areaFocusC,decimal horsServiceC,decimal unitC,String modeC,String staffMemberC){
        dateCompare = serviceDateC;
        isEvent = isEventC;
        serviceItem = new SFF_detailCase(idCaseC,AttendanceStatusC,nameSC,OwnerServiceC,  idServiceC, serviceC, typeServiceC, nameC, serviceDateC, subtypeC, areaFocusC, horsServiceC, unitC, modeC, staffMemberC);
   }
    global Integer compareTo(Object compareTo) {
        SFF_UnitData compareToUnit = (SFF_UnitData)compareTo;
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (this.dateCompare > compareToUnit.dateCompare) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (this.dateCompare < compareToUnit.dateCompare) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        if((returnValue == 0)&&((this.isEvent == true )||(compareToUnit.isEvent == true))){
        	if(this.isEvent == true){
        		returnValue = 1;
        	}else{
        		returnValue = -1;
        	}
        }
        return returnValue; 
    }
}