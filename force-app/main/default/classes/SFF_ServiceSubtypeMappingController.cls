global  with sharing class SFF_ServiceSubtypeMappingController {
    
    public String serviceSelected					{get;set;} // service selected
    public String EditOrSave						{get;set;}
    public String idRecord;
    public String subtypeSelected					{get;set;} // subtype selected in vf 
    public integer subtypeDele						{get;set;} // index of subtype in slectedSubtypes index   from vf
    public String subtypeAreaOfFocus				{get;set;}
    public Service_Subtype_Mapping__c SSMapping 	{get;set;} // get record if is edit 
    public list<String> slectedSubtypes				{get;set;} //
    public Service__c service						{get;set;} //
    public Service_Subtype_Mapping__c ssm;
    
    public SFF_ServiceSubtypeMappingController(ApexPages.StandardController stdController){
	    this.SSMapping = (Service_Subtype_Mapping__c)stdController.getRecord();
	    //String idRecord = ApexPages.currentPage().getParameters().get('id');
	    system.debug(idRecord);
	    if(SSMapping.id != null){
	    	initID(SSMapping.id);
	    }else{
	    	SSMapping = new Service_Subtype_Mapping__c();
	    	init();
	    }
            
    }
    
    public void init(){
		serviceSelected 		= '';
		subtypeSelected 		= '';
        service 				= new Service__c();
        slectedSubtypes 		= new list<String>();
		subtypeAreaOfFocus 		= '';
        //SSMapping = new Service_Subtype_Mapping__c();
    }
    public void initID(id recordToEdit){
	    //SSMapping = new Service_Subtype_Mapping__c();
	    SSMapping 			= [SELECT Name, Service__c, Subtypes__c, Area_of_Focus__c FROM Service_Subtype_Mapping__c  WHERE id = :recordToEdit];
		serviceSelected 	= SSMapping.Service__c; // input 1
		subtypeSelected 	= ''; // input 2
        service 			= new Service__c();	// init service sobject
        service.Service__c 	= serviceSelected; // input field
        subtypeAreaOfFocus 	= SSMapping.Area_of_Focus__c;
        slectedSubtypes 	= new list<String>(SSMapping.Subtypes__c.split(','));
    }
    public void deleteSubtype(){
    	if(subtypeDele<0){
    		slectedSubtypes.clear();
    	}else{
    	 	slectedSubtypes.remove(subtypeDele);
    	}
    }
    
    public void addSubtype(){
    		slectedSubtypes.add(service.Subtype__c);
    		SSMapping.Area_of_Focus__c = !String.isEmpty(subtypeAreaOfFocus) ? subtypeAreaOfFocus.trim() : subtypeAreaOfFocus;	
    		service.Subtype__c = '';
    }
    
    public string save(){
    	SSMapping.Area_of_Focus__c = subtypeAreaOfFocus;
        SSMapping.Service__c = service.Service__c;
        String liststring = '';
        for(string var : slectedSubtypes){
            liststring += var+', ';
        }
        SSMapping.Subtypes__c = liststring;
        String response = insertUpdate();
        return response;
    }
    public Pagereference newService(){
    	String response = save();
	    if(!response.equals('0')){
			Pagereference page = new Pagereference('/apex/SFF_ServiceSubtypeMapping');
			page.setRedirect(true);
			return page;
    	}
    	return null;
    }
    public PageReference refPage(){
    	String save = save();
    	if(!save.equals('0')){
    		return new PageReference('/'+save);
    	}
    	return null;
    }
    //  upsert if doesn't exist other record with the same service 
    public String insertUpdate(){
    	list<Service_Subtype_Mapping__c> SSMapp= new list<Service_Subtype_Mapping__c>([SELECT Name, Service__c, Area_of_Focus__c FROM Service_Subtype_Mapping__c where id <> :SSMapping.id]);
    	
		try{
			upsert SSMapping;
			return SSMapping.Id;
		}
		catch(DmlException e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info,e.getMessage()));
		}
	
    	return '0';
    }
    
    @RemoteAction
	global static List<String> getServicesValues(){
		
		List<String> values = new List<String>();
		
		for (Schema.Picklistentry ple : Service__c.service__c.getDescribe().getPickListValues()) {
			
			values.add(ple.getlabel());
		}
		
		return values;
	}
	
	@RemoteAction
	global static List<String> getAreaOfFocusValues(){
		
		List<String> values = new List<String>();
		
		for (Schema.Picklistentry ple : Service__c.Area_Of_Focus__c.getDescribe().getPickListValues()) {
			
			values.add(ple.getlabel());
		}
		
		return values;
	}
}