global class GroupServicesBatch implements Database.Batchable<sObject> {
    
    String query;
    Id serviceId;
    String sessionNote;
    Date sessionDate;
    String sessionsToProcess;
    
    global GroupServicesBatch(Id serviceId, String note, String sessionDate, String sessionsToCreate) {
        this.sessionDate = Date.valueOf(sessionDate);
        this.serviceId = serviceId;
        this.sessionNote = note;
        this.sessionsToProcess = sessionsToCreate;

        // Get the list of event for the serviceId
        query = 'Select Id, Group_Service__c, Date__c From Event__c Where Group_Service__c =: serviceId AND Date__c >: sessionDate';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Event__c> scope) {
        if(scope.size() > 0){
            List<Session> sessions = (List<Session>)JSON.deserialize(sessionsToProcess, List<Session>.class);
            sessions.remove(0);
            List<Session__c> sessionsToInsert = new List<Session__c>();

            for(Event__c ev : scope){
                
                if(ev.Date__c != sessionDate){

                    for(Session s : sessions){
                        Session__c sess = new Session__c();
                        sess.Case__c = s.sessionCase;
                        sess.Contact__c = s.sessionContact;
                        sess.Service__c = s.service;
                        sess.Event__c = ev.Id;
                        sess.Session_Date__c = ev.Date__c;
                        sess.Metro_Card__c = false;
                        sess.Attendance_Status__c = '';
                        sess.Session_Status__c = 'Open';
                        sess.Notes__c = '';

                        sessionsToInsert.add(sess);
                    }
                }
            }

            if(sessionsToInsert.size() > 0){
                try{
                    insert sessionsToInsert;
                } catch(DmlException e) {
                    System.debug('##################### ERROR ##################### \n');
                    System.debug('Error message: ' + e.getMessage() + '\n');
                    System.debug('################################################# \n');
                }
            }
        }
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }

    public class Session {
        public String sessionCase;
        public String sessionContact;
        public String sessionDate;
        public String service;
        public String event;
        public String attendanceStatus;
        public Boolean metroCard;
        public String sessionStatus;
        public String sessionNotes;
    }
}