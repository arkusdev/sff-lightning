public with sharing class contact_Dedupe_Controller {

	public static final Integer RESULTSPERPAGE = 200;
	public ApexPages.standardSetController setController{get;set;}
	public list<ContactRow> contacts {get;set;}
	
	 set<string> uniquePerfectTracker;
	 set<string> uniquePartialTracker;
	 static map<string,integer> matchCount;
	 map<string,string> matchToPerfect;
	public list<ContactRow> mergeList {get;set;}
	public boolean showMergeScreen {get;set;}
	public boolean contact1Master {get;set;}
	public boolean contact2Master {get;set;}
	public string contact1id {get;set;}
	public string contact2id {get;set;}
	public boolean mergesuccessful {get;set;}
	public class contactRow {
	public contact c {get;set;}
	public transient string matchtype {get;set;}
	public boolean selected {get;set;}
	public string contactid {get;set;}
	public contactRow (Contact con) {
		c = con;
		contactid = con.id; 
		}
	}
	public contact_Dedupe_Controller () {
		setController = new Apexpages.StandardSetController([select id from contact order by lastname,firstname,createddate asc,birthdate limit 9900]);
		setController.setPageSize(RESULTSPERPAGE);
		contacts = new list<ContactRow> ();
		uniquePerfectTracker = new set<string>();
		uniquePartialTracker = new set<string>();
		matchcount = new map<string,integer>();
		matchtoperfect = new map<string,string>();
		//pagecount = [select count() from Contact limit 49000];
		//pagecount = getTotalPages();   //pagecount/200;
		getContacts();
	}
	
	public void gotoNext(){
		SetController.next();
		getContacts();
	}
	
	public void gotoPrevious(){
		SetController.previous();
		getContacts();	
	}
	
	public pagereference getContacts () {
		system.debug('get contacts');
		set<id> idSet = new set<id>();
		for (contact c : (list<contact>)setController.getRecords())
		{
			idSet.add(c.id);
		}
		for (contact c : [select id, firstname, lastname, birthdate, createddate from contact where id in :idSet]) {
			ContactRow cr = new ContactRow(c);
			system.debug(c);
			string perfect = fixnull(c.firstname).tolowercase()+fixnull(c.lastname).tolowercase()+string.valueof(c.birthdate); // perfect match
			string partial1 = fixnull(c.firstname).tolowercase()+fixnull(c.lastname).tolowercase(); // first + last match
			string partial2 = fixnull(c.firstname).tolowercase()+string.valueof(c.birthdate); // first + birth match
			string partial3 = fixnull(c.lastname).tolowercase()+string.valueof(c.birthdate); // last + birth match
			if (uniquePerfectTracker!= null && uniquePerfectTracker.contains(perfect)) { // perfect match
			cr.matchtype = 'Perfect';	
			incrementMatchCount(perfect);
			}
			else if (uniquePartialTracker != null && uniquePartialTracker.contains(partial1)) { //partial
				cr.matchtype = 'Partial';
				incrementMatchCount(matchtoperfect.get(partial1));
			}
			else if (uniquePartialTracker != null && uniquePartialTracker.contains(partial2)) { //partial
				cr.matchtype = 'Partial';
				incrementMatchCount(matchtoperfect.get(partial2));
			}
			else if (uniquePartialTracker != null && uniquePartialTracker.contains(partial3)) { //partial
				cr.matchtype = 'Partial';
				incrementMatchCount(matchtoperfect.get(partial3));
			}
			else {
				cr.matchtype = '';
				uniquePerfectTracker.add(perfect);
				uniquePartialTracker.add(partial1); 
				uniquePartialTracker.add(partial2); 
				uniquePartialTracker.add(partial3);
				matchtoperfect.put(perfect,perfect);
				matchtoperfect.put(partial1,perfect);
				matchtoperfect.put(partial2,perfect);
				matchtoperfect.put(partial3,perfect);
				 
				incrementMatchCount(perfect);
				}
				contacts.add(cr);
		}
		uniqueCheck();
		return null;
	}
	public pagereference incrementMatchCount(string key){
		system.debug('matchcount'+matchcount);
		if (matchcount==null) matchcount = new map<string,integer>();
		if (matchcount.containskey(key)) {
		integer i = matchcount.get(key);
		i++;
		matchcount.put(key,i);
		}
		else {
			matchcount.put(key,0); //first record
		}
			return null;
	}
		
	public pagereference nextPage () {
		/*pagenumber++;
		refresh();*/
		gotoNext();
		return null;
	}
	
	public integer getCurrentPage(){
		return setController.getPageNumber();
	}
	
	public Integer getTotalPages(){
		return (Integer)(Decimal.valueOf(setController.getResultSize())/Decimal.valueof(RESULTSPERPAGE)).round(SYSTEM.roundingmode.CEILING);
	
	}
	
	
	 
	public pagereference prevPage () {
		/*pagenumber--;
		refresh();*/
		gotoPrevious();
		return null;
	}
	public pagereference uniqueCheck () {
		// get rid of those with no partial or perfect matches
		list<contactrow> templist = new list<contactrow>();
		for (integer i=0; i<contacts.size();i++){
			string key = fixnull(contacts[i].c.firstname).tolowercase()+fixnull(contacts[i].c.lastname).tolowercase()+string.valueof(contacts[i].c.birthdate);
			string partial1 = fixnull(contacts[i].c.firstname).tolowercase()+fixnull(contacts[i].c.lastname).tolowercase(); // first + last match
			string partial2 = fixnull(contacts[i].c.firstname).tolowercase()+string.valueof(contacts[i].c.birthdate); // first + birth match
			string partial3 = fixnull(contacts[i].c.lastname).tolowercase()+string.valueof(contacts[i].c.birthdate); // last + birth match
			if (contacts[i].matchtype == 'Partial' && matchtoperfect.containsKey(partial1)) {
				key = matchtoperfect.get(partial1);
			}
			else if (contacts[i].matchtype == 'Partial' && matchtoperfect.containsKey(partial2)) {
				key = matchtoperfect.get(partial2);
			}
			else if (contacts[i].matchtype == 'Partial' && matchtoperfect.containsKey(partial3)) {
				key = matchtoperfect.get(partial3);
			}
			if (matchcount.get(key) != null && matchcount.get(key) >= 1) {
				templist.add(contacts[i]);
			}
		}
		contacts.clear();
		contacts = templist.clone();
		return null;
	}
	public pagereference beginMerge () {
		mergelist = new list<Contactrow>();
		for (contactRow cr : contacts) {
			if (cr.selected==true) {
				mergelist.add(cr);
			}
		}
		if (mergelist.size() > 2) {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You may only select 2 records for merging'));
		}
		else if (mergelist.size() < 2) {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error,'You must select 2 records for merging'));
		}
		else {
			contact1id = mergelist[0].c.id;
			contact2id = mergelist[1].c.id;
			showMergeScreen = true;
		}
		return null;
	}
	public pagereference executeMerge () {
		if (contact1master && contact2master) {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error, 'You can have only one master record'));
		return null;
		}
		else if (contact1master == false && contact2master == false) {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error, 'You must select a record to be the master'));
		return null;
		}
		try {
			if (contact1master) {
				Contact master = [select id from Contact where id = :contact1id];
				Contact slave = [select id from Contact where id = :contact2id];
				merge master slave;
			}
			else if (contact2master) {
				Contact master = [select id from Contact where id = :contact2id];
				Contact slave = [select id from Contact where id = :contact1id];
				merge master slave;
			}
			apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, 'Records merged successfully'));
			mergesuccessful = true;
		}
		catch (exception e) {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error, 'There was an unexpected error while merging records'));
			mergesuccessful = false;
		}
		return null;
	}
	public pagereference backtolist () {
		showmergescreen = false;
		refresh();
		mergesuccessful = false;
		return null;
	}
	public pagereference refresh () {
		contacts.clear();
		uniquePartialTracker.clear();
		uniquePerfectTracker.clear();
		//matchcount.clear();
		matchtoperfect.clear();
		getContacts();
		return null;
	}
	public string fixnull (string s) {
		if (s == null) return '';
		else return s;
	}
	public pagereference deleteContact () {
		list<Contact> ContactDeletions = new list<Contact>();
		for (integer i = 0; i<contacts.size(); i++) {
		if (contacts[i].selected == true) {
			contactdeletions.add(contacts[i].c);
			contacts.remove(i);	
			i--;
		}
	}
	delete contactdeletions;	
		return null;
	}
	@isTest
	static void testContactDedupeController() {
	list<contact> clist=new list<contact>();
	for(Integer i=0;i<201;i++){
		clist.add(new contact(lastname='test'+i, firstname='testn'+i));
	}
	insert clist;
	contact_Dedupe_Controller cdc=new contact_Dedupe_Controller();
	cdc.nextPage();	
	cdc.getContacts();
	cdc.prevPage();
	//cdc.incrementMatchCount();
	cdc.contact1master=true;
	cdc.contact2master=false;
	cdc.beginmerge();
	cdc.executeMerge();
	cdc.backtolist();
	cdc.deletecontact();
	}
}