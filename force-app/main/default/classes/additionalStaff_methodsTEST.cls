@isTest
public class AdditionalStaff_MethodsTest {
    @isTest
    public static void additionalStaff_Methods_Test() {
        Case__c c = new Case__c();
        c.Matter_Type_Detail__c = 'Education';
        insert c;
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.RecordTypeId = [select Id from recordtype where Name = 'Staff' and sObjectType = 'Contact'].Id;
        insert con;
        
        Additional_Staff__c addStaff = new Additional_Staff__c();
        addStaff.Case__c = c.Id;
        addStaff.Contact__c = con.Id;
        addStaff.Primary_Staff_on_Case__c = false;
        insert addStaff;
        
        addStaff.Primary_Staff_on_Case__c = true;
        update addStaff;
    }
    
    @isTest
    public static void testInserASWithSameContact() {
        Case__c c = new Case__c();
        c.Matter_Type_Detail__c = 'Education';
        insert c;
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.RecordTypeId = [select Id from recordtype where Name = 'Staff' and sObjectType = 'Contact'].Id;
        insert con;
        try{
            Additional_Staff__c addStaff1 = new Additional_Staff__c();
            addStaff1.Case__c = c.Id;
            addStaff1.Contact__c = con.Id;
            addStaff1.Primary_Staff_on_Case__c = false;
            insert addStaff1;
            
            Additional_Staff__c addStaff2 = new Additional_Staff__c();
            addStaff2.Case__c = c.Id;
            addStaff2.Contact__c = con.Id;
            addStaff2.Primary_Staff_on_Case__c = false;
            insert addStaff2;
            System.assert(false);
        }catch(Exception e){
            System.assert(true);
        }
    }
    
    @isTest
    public static void testRemovePrimaryStaffOnAdditionalStaffDelete() {
        Test.startTest();
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.RecordTypeId = [select Id from recordtype where Name = 'Staff' and sObjectType = 'Contact'].Id;
        insert con;
        
        Case__c cas = new Case__c();
        cas.Matter_Type_Detail__c = 'Education';
        cas.Primary_Staff_on_Case__c = con.Id;
        insert cas;
       
        List<Additional_Staff__c> staffBefore = [SELECT Id FROM Additional_Staff__c WHERE Case__c = : cas.Id];
	    delete staffBefore;
        Test.stopTest(); 
        
        Case__c casRes = [SELECT Id,Primary_Staff_on_Case__c FROM Case__c WHERE Id =: cas.Id][0];
        System.assertEquals(null, casRes.Primary_Staff_on_Case__c);
    }
}