global with sharing class GroupServicesPageController {

    public List<Session__c> sessions {get; set;}

    public List<Session__c> latestSessions {get;set;}

    private final Service__c service;

    public GroupServicesPageController(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) { 
            stdController.addFields(new List<String>{'Service__c', 'Subtype__c', 'Area_of_Focus__c', 'Mode__c', 'Staff_Member__c', 'Staff_Member2__c', 'Staff_Member3__c', 'Staff_Member4__c', 'Staff_Member5__c', 'Staff_Member6__c', 'Staff_Member7__c', 'Staff_Member8__c', 'Staff_Member9__c', 'Staff_Member10__c', 'Event__c', 'Service_Date__c', 'Notes__c', 'Frequency__c', 'Frequency_daily__c', 'Frequency_monthly_on_1__c', 'Frequency_monthly_on_2__c', 'Frequency_monthly_on_day__c', 'Frequency_weekly__c', 'Start_Date__c', 'End_Date__c'});
        }
        this.service = (Service__c)stdController.getRecord();

        if(service.Id != null){
            
            Date lastSessionDate = System.today();

            // Get the date of the last session close ordered by Session_Date__c DESC
            // Filter for that specific date to get all the users sessions related to the service
            List<Session__c> lastSessionClosed = [  Select Id, Service__c, Session_Status__c, Session_Date__c 
                                                    From Session__c 
                                                    Where Service__c =: service.Id 
                                                    AND Session_Status__c =: 'Closed'
                                                    ORDER BY Session_Date__c DESC Limit 1];


            if(lastSessionClosed.size() > 0){
                lastSessionDate = lastSessionClosed[0].Session_Date__c;
            }
            
            sessions = [ SELECT Case__c, Case__r.Name, Case__r.Type_of_Assistance__c, Case__r.Type_of_Matter__c,
                                Contact__c, Contact__r.Name, Contact__r.Birthdate, 
                                Session_Date__c, Attendance_Status__c, Notes__c, Metro_Card__c, Session_Status__c 
                        FROM Session__c 
                        WHERE Service__c = :service.Id 
                        AND Session_Date__c >: lastSessionDate ORDER BY Session_Date__c ASC Limit 1000];
            System.debug('Sessions: ' + sessions);

            if(lastSessionClosed.size() > 0){
                latestSessions = [SELECT Case__c, Case__r.Name, Case__r.Type_of_Assistance__c, Case__r.Type_of_Matter__c,
                                    Contact__c, Contact__r.Name, Contact__r.Birthdate, 
                                    Session_Date__c, Attendance_Status__c, Notes__c, Metro_Card__c, Session_Status__c 
                                FROM Session__c 
                                WHERE Service__c = :service.Id 
                                AND Session_Date__c =: lastSessionClosed[0].Session_Date__c
                                AND Session_Status__c =: 'Closed'];

            }
        }
    }

    @RemoteAction
    global static List<Session__c> getSessions(Id serviceId, String sessionDate){
        Date currentSessionDate = Date.valueOf(sessionDate);
        List<Session__c> ss = [SELECT Case__c, Case__r.Name, Case__r.Type_of_Assistance__c, Case__r.Type_of_Matter__c,
                                            Contact__c, Contact__r.Name, Contact__r.Birthdate, 
                                            Session_Date__c, Attendance_Status__c, Notes__c, Metro_Card__c, Session_Status__c 
                                    FROM Session__c 
                                    WHERE Service__c = : serviceId
                                    AND Session_Date__c =: currentSessionDate];

        return ss;
    }

    @RemoteAction
    global static List<CaseContact> getAllActiveCases(){

        //Gets the custom metadata with recordtypes for filtering
        Attendance_RecordTypes_Filter__mdt[] attendanceFilters = [Select DeveloperName, MasterLabel From  Attendance_RecordTypes_Filter__mdt];

        List<String> recordtypesToFilter = new List<String>();

        for(Attendance_RecordTypes_Filter__mdt filter : attendanceFilters){
            recordtypesToFilter.add(filter.DeveloperName);
        }

        List<RecordType> recordtypes = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType Where DeveloperName IN : recordtypesToFilter];
        Set<Id> rtIds = new Set<Id>();

        for(RecordType rt : recordtypes) {
            rtIds.add(rt.Id);
        }

        Type_of_Assistance_filter__mdt[] types = [Select DeveloperName, MasterLabel, Record_Type_Id__c From Type_of_Assistance_filter__mdt];
        
        Map<String, String> mapRtAssistance = new Map<String, String>();

        for(Type_of_Assistance_filter__mdt ta : types){
            mapRtAssistance.put(ta.MasterLabel, ta.Record_Type_Id__c);
        }

        List<Case__c> cases = [ Select Id, Name, Type_of_Assistance__c, Type_of_Matter__c, StatusFormula__c, Matter_Type_Detail__c, Case_Open_Date__c,
                                        Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.RecordTypeId, Contact__r.Birthdate, Contact__r.Salesforce_User__c,
                                        RecordTypeId
                                From Case__c
                                Where StatusFormula__c =: 'Active' AND Contact__c != '' 
                                AND RecordTypeId NOT IN : rtIds];

        List<Case__c> casesAux = new List<Case__c>();

        for(Case__c c : cases){
            if(mapRtAssistance.containsKey(c.Type_of_Assistance__c)){
                if(mapRtAssistance.get(c.Type_of_Assistance__c) != c.RecordTypeId){
                    casesAux.add(c);
                }
            } else {
                casesAux.add(c);
            }
        }

        List<CaseContact> caseContacts = new List<CaseContact>();

        if(casesAux.size() > 0){
            for(Case__c c : casesAux){
                Contact con = new Contact(Id = c.Contact__c, FirstName = c.Contact__r.FirstName, LastName = c.Contact__r.LastName, RecordTypeId = c.Contact__r.RecordTypeId, Birthdate = c.Contact__r.Birthdate, Salesforce_User__c = c.Contact__r.Salesforce_User__c);
                CaseContact d = new CaseContact(c, con);
                caseContacts.add(d);
            }
        }

        return caseContacts;
    }

    @RemoteAction
    global static List<Contact> getAllStaff(String rtId){
        return [Select Id, FirstName, LastName, RecordTypeId, Birthdate, Name, Salesforce_User__c 
                From Contact Where RecordTypeId =: rtId 
                AND Salesforce_User__c != '' 
                ORDER BY LastName ASC, FirstName ASC];
    }


    @RemoteAction
    global static List<Event__c> getServiceEventsFromDate(String serviceId, String sessionDateFrom){
        Date dateFrom = Date.valueOf(sessionDateFrom);
        return [Select Id, Group_Service__c, Date__c  
                From Event__c Where Group_Service__c =: serviceId 
                AND Date__c >=: dateFrom];
    }

    @RemoteAction
    global static List<Event__c> createEvents(Id serviceId, String currentSessionDate, String eventsToCreate){

        List<GroupServiceEvent> events = (List<GroupServiceEvent>)JSON.deserialize(eventsToCreate, List<GroupServiceEvent>.class);

        if(events.size() > 0){
            List<Event__c> eventsToInsert = new List<Event__c>();

            for(GroupServiceEvent gse : events){
                Event__c ev = new Event__c();
                ev.Group_Service__c = gse.serviceId;
                ev.Date__c = gse.eventDate.date();
                ev.Event_Note__c = gse.eventNote;
                eventsToInsert.add(ev);
            }

            if(eventsToInsert.size() > 0){
                try{
                    insert eventsToInsert;

                    return eventsToInsert;
                } catch(Exception e){
                    System.debug('##### ERROR ##### \n');
                    System.debug('Error message: ' + e.getMessage());
                }
            }
        }

        return null;
    }

    global class GroupServiceEvent {
        public String eventNote {get;set;}
        public Datetime eventDate {get;set;}
        public Id serviceId {get;set;}
    }

    @RemoteAction
    global static List<Session__c> createSessions(Id serviceId, String sessionNote, String sessionDate, String sessionsToCreate){
        
        Date currentSessionDate = Date.valueOf(sessionDate);

        // Get the list of event for the serviceId
        List<Event__c> events = [Select Id, Group_Service__c, Date__c From Event__c Where Group_Service__c =: serviceId AND Date__c >=: currentSessionDate];

        if(events.size() > 0){
            List<Session> sessions = (List<Session>)JSON.deserialize(sessionsToCreate, List<Session>.class);

            if(sessions.size() > 0) {

                Integer numOfRecordsToInsert = events.size() * sessions.size();

                if(numOfRecordsToInsert < 10000){
                    List<Session__c> sessionsToInsert = new List<Session__c>();

                    for(Event__c ev : events){

                        for(Session s : sessions){
                            Session__c sess = new Session__c();
                            sess.Case__c = s.sessionCase;
                            sess.Contact__c = s.sessionContact;
                            sess.Service__c = s.service;
                            sess.Event__c = ev.Id;

                            if(ev.Date__c == currentSessionDate){
                                sess.Session_Date__c = currentSessionDate;
                                sess.Attendance_Status__c = s.attendanceStatus;
                                sess.Metro_Card__c = s.metroCard;

                                // Add session status to the attendance depending on the option selected     
                                if (s.attendanceStatus != 'Absent' && s.attendanceStatus != 'Excused Absence') {
                                    sess.Notes__c = sessionNote;
                                } else {
                                    sess.Notes__c = 'Absent';
                                }

                                sess.Session_Status__c = 'Closed';


                            } else {
                                sess.Session_Date__c = ev.Date__c;
                                sess.Metro_Card__c = false;
                                sess.Attendance_Status__c = '';
                                sess.Session_Status__c = 'Open';
                                sess.Notes__c = '';
                            }

                            sessionsToInsert.add(sess);
                        }
                    }

                    if(sessionsToInsert.size() > 0){
                        try{
                            insert sessionsToInsert;

                            // Returns just the sessions for the current date
                            List<Session__c> currentDaySessions = new List<Session__c>();
                            for(Session__c s : sessionsToInsert){
                                if(s.Session_Date__c == currentSessionDate){
                                    currentDaySessions.add(s);
                                }
                            }

                            return currentDaySessions;

                        } catch(DmlException e) {
                            System.debug('##################### ERROR ##################### \n');
                            System.debug('Error message: ' + e.getMessage() + '\n');
                            System.debug('################################################# \n');
                        }
                    }

                    return null;

                } else {
                    // Insert just the sessions for the current date and then
                    // call the batch apex class to insert the rest of sessions for each event.
                    List<Session__c> currentSessions = new List<Session__c>();

                    for(Event__c ev : events){
                        
                        if(ev.Date__c == currentSessionDate){
                            
                            for(Session s : sessions){
                                
                                Session__c sess = new Session__c();
                                sess.Case__c = s.sessionCase;
                                sess.Contact__c = s.sessionContact;
                                sess.Service__c = s.service;
                                sess.Event__c = ev.Id;

                                sess.Session_Date__c = currentSessionDate;
                                sess.Attendance_Status__c = s.attendanceStatus;
                                sess.Metro_Card__c = s.metroCard;

                                // Add session status to the attendance depending on the option selected     
                                if (s.attendanceStatus != 'Absent' && s.attendanceStatus != 'Excused Absence') {
                                    sess.Notes__c = sessionNote;
                                } else {
                                    sess.Notes__c = 'Absent';
                                }

                                sess.Session_Status__c = 'Closed';


                                currentSessions.add(sess);
                            }
                        }
                    }

                    if(currentSessions.size() > 0){
                        try{
                            insert currentSessions;
                        } catch(DmlException e) {
                            System.debug('##################### ERROR ##################### \n');
                            System.debug('Error message: ' + e.getMessage() + '\n');
                            System.debug('################################################# \n');
                        }

                        // Prevent running the batch if its call from test class
                        if(!Test.isRunningTest()){
                            // Call the batch to processs all the other events/sessions
                            Id groupServiceBatchId = Database.executeBatch(new GroupServicesBatch(serviceId, sessionNote, sessionDate, sessionsToCreate), 200);
                        }

                        return currentSessions;

                    }

                }
            }
        }
        return null;
    }

    @RemoteAction
    global static void deleteSessions(Id serviceId, Id contactId){

        List<Session__c> sessions = [Select Id, Service__c, Contact__c From Session__c Where Service__c =: serviceId AND Contact__c =: contactId AND Session_Status__c =: 'Open'];

        if(sessions.size() > 0){
            try{
                delete sessions;
            } catch(DmlException e){
                System.debug('##### ERROR ##### \n');
                System.debug('Error message: ' + e.getMessage());
            }
        }
    }

    global class Session {
        public String sessionCase;
        public String sessionContact;
        public String sessionDate;
        public String service;
        public String event;
        public String attendanceStatus;
        public Boolean metroCard;
        public String sessionStatus;
        public String sessionNotes;

        public Session(String sCase, String sContact, String sDate, String sService, String sEvent, String sAttendance, Boolean metroC, String sStatus, String sNote){
            this.sessionCase = sCase;
            this.sessionContact = sContact;
            this.sessionDate = sDate;
            this.service = sService;
            this.event = sEvent;
            this.attendanceStatus = sAttendance;
            this.metroCard = metroC;
            this.sessionStatus = sStatus;
            this.sessionNotes = sNote;
        }
    }

    @RemoteAction
    global static void createMetrocardService(String metrocardsToCreate){
        List<Service__c> metrocards = (List<Service__c>)JSON.deserialize(metrocardsToCreate, List<Service__c>.class);

        if(metrocards.size() > 0){
            try{
                insert metrocards;
            } catch(DmlException e){
                System.debug('##### ERROR ##### \n');
                System.debug('Error message: ' + e.getMessage());
            }
        }
    }

    global class CaseContact{
        Case__c cas{get;set;}
        Contact con{get;set;}

        public CaseContact(Case__c pCase, Contact pContact){
            this.cas = pCase;
            this.con = pContact;
        } 
    }

}