public  class ClientCaseReportController {
	public case__c caseRecord{get;set;}
	public ClientCaseReportController(ApexPages.StandardController con){
		caseRecord=(case__c)con.getRecord();
		
	}
	
	public list<service__c> getEventServices(){
		set<id> eventIds=new set<id>();
		for(Event__c evt:caseRecord.events__r)
			eventIds.add(evt.id);
			
		list<service__c> services=[select id,recordtype.name,name,Service__c,Subtype__c,Area_of_Focus__c,Start_date__c,End_Date__c,Service_Date__c, Ownerid,owner.name,event__r.name,event__c,event__r.Mode__c,event__r.date__c,event__r.Event_note__c from service__c where event__c in :eventIds order by event__r.name ,Service_Date__c,recordType.name];
		
		return services;
		
	}
	public list<session__c> getSessions(){
		set<id> eventIds=new set<id>();
		for(Event__c evt:caseRecord.events__r)
			eventIds.add(evt.id);
		list<Session__c> sessions=[select id,Area_of_Focus__c,Attendance_status__c,Service__c,service__r.name,Session_date__c,Subtype__c,name from session__c where Attendance_Status__c='Present' and Service__r.event__c in :eventIds order by Service__r.name, Session_date__c ];

		return sessions;
	}

}