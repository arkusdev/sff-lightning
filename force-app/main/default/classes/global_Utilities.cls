global without sharing class global_Utilities {

	webservice static void changeToDirectServiceClient(string cid) {
		update new Contact(id=cid, recordtypeid=[select id from recordtype where name = 'Direct Service Client' AND sobjecttype = 'Contact'].id);
	}
	
	public static sObject queryAllFields (id objectid) {
		string queryString = 'Select ';
		string objectname = string.valueof(objectid.getsobjecttype());
		map<String, Schema.SObjectField> M = Schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap();
		for (string s : m.keySet()) if (m.get(s).getDescribe().getType() != Schema.Displaytype.LOCATION) queryString += s+', ';
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		queryString += ' FROM '+objectname;
		queryString += ' where id=\''+objectid+'\''; 
		
		return database.query(queryString)[0];
	}
	
	@isTest
	static void globalUtilitiesTest(){
		Contact c=new Contact(FirstName='test', LastName='test');
		insert c;
		changeToDirectServiceClient(c.id);
	}
	
}