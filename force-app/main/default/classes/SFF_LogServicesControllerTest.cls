@isTest
private class SFF_LogServicesControllerTest {
	
	private static void config() {
		Case__c testCase = new Case__c(Matter_Type_Detail__c='ACS record request');
        insert testCase;
        Event__c testEvent = new Event__c(Date__c = system.Today());
        insert testEvent;
        Id recordTypeId = [select Id from RecordType where SObjectType = 'Contact' AND DeveloperName = 'Staff'].Id;
        List<Contact> contactList = new List<Contact>();
        Contact contact1 = new Contact(FirstName = 'Con1', Lastname = 'Contact1', Email = 'contact1@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact1);
        Contact contact2 = new Contact(FirstName = 'Con2', Lastname = 'Contact2', Email = 'contact2@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact2);
        Contact contact3 = new Contact(FirstName = 'Con3', Lastname = 'Contact3', Email = 'contact3@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact3);
        Contact contact4 = new Contact(FirstName = 'Con4', Lastname = 'Contact4', Email = 'contact4@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact4);
        Contact contact5 = new Contact(FirstName = 'Con5', Lastname = 'Contact5', Email = 'contact5@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact5);
        Contact contact6 = new Contact(FirstName = 'Con6', Lastname = 'Contact6', Email = 'contact6@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact6);
        Contact contact7 = new Contact(FirstName = 'Con7', Lastname = 'Contact7', Email = 'contact7@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact7);
        Contact contact8 = new Contact(FirstName = 'Con8', Lastname = 'Contact8', Email = 'contact8@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact8);
        Contact contact9 = new Contact(FirstName = 'Con9', Lastname = 'Contact9', Email = 'contact9@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact9);
        Contact contact10 = new Contact(FirstName = 'Con10', Lastname = 'Contact10', Email = 'contact10@email.com',	RecordTypeId = recordTypeId);
        contactList.add(contact10);
        insert contactList;        
        
        Service__c testService = new Service__c(
        	Service__c = 'TestService',
        	Subtype__c = 'Subtype1',
        	Event__c = testEvent.Id,
        	Staff_Member__c = contactList[0].Id,
        	Staff_Member2__c = contactList[1].Id,
        	Staff_Member3__c = contactList[2].Id,
        	Staff_Member4__c = contactList[3].Id,
        	Staff_Member5__c = contactList[4].Id,
        	Staff_Member6__c = contactList[5].Id,
        	Staff_Member7__c = contactList[6].Id,
        	Staff_Member8__c = contactList[7].Id,
        	Staff_Member9__c = contactList[8].Id,
        	Staff_Member10__c = contactList[9].Id
        );
        insert testService;
        Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c(
        	Service__c = 'TestService',
        	Subtypes__c = 'Subtype1,Subtype2',
        	Area_of_Focus__c = '--None--'
        );
        insert ssm;
        PageReference pageRef = Page.SFF_LogServices;
		pageRef.getParameters().put('cId', testCase.Id);
		pageRef.getParameters().put('eId', testEvent.Id);
		Test.setCurrentPage(pageRef);		
	}
	
    static testMethod void SFF_LogServicesControllerTest() {    	
		config();
		Test.startTest();
			SFF_LogServicesController sff = new SFF_LogServicesController();
		Test.stopTest();
    }
    
    static testMethod void SFF_LogServicesControllerGetServicesValuesTest() {
		List<String> response;
		Test.startTest();
			response = SFF_LogServicesController.getServicesValues();
		Test.stopTest();
		system.assert(response.size() > 0);
	}
	
	static testMethod void SFF_LogServicesControllerGetAreaOfFocusValuesTest() {
		List<String> response;
		Test.startTest();
			response = SFF_LogServicesController.getAreaOfFocusValues();
		Test.stopTest();
		system.assert(response.size() > 0);
	}
	
	static testMethod void SFF_LogServicesControllerAddServiceTest() {    		
		config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		Test.startTest();
			sff.addService();
		Test.stopTest();
		system.assert(sff.serviceList.size() > 0);
    }
    
    static testMethod void SFF_LogServicesControllerAddMemberTest() {    	
		Case__c testCase = new Case__c(Matter_Type_Detail__c='ACS record request');
        insert testCase;
        Contact contact1 = new Contact(FirstName='Con1' ,Lastname = 'Contact1');
        insert contact1;
		PageReference pageRef = Page.SFF_LogServices;
		pageRef.getParameters().put('cId', testCase.Id);
		pageRef.getParameters().put('staffId', contact1.Id);
		Test.setCurrentPage(pageRef);
		SFF_LogServicesController sff = new SFF_LogServicesController();
		Test.startTest();
			sff.addMember();
		Test.stopTest();
		system.assert(sff.staffMembers.size() == 1);
		system.assert(sff.staffMemberSet.size() == 1);
    }
    
    static testMethod void SFF_LogServicesControllerRemoveMemberTest() {    	
		Case__c testCase = new Case__c(Matter_Type_Detail__c='ACS record request');
        insert testCase;
        Contact contact1 = new Contact(FirstName='Con2', Lastname = 'Contact1');
        insert contact1;
		PageReference pageRef = Page.SFF_LogServices;
		pageRef.getParameters().put('cId', testCase.Id);
		pageRef.getParameters().put('staffId', contact1.Id);
		Test.setCurrentPage(pageRef);
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.addMember();
		Test.startTest();
			sff.removeMember();
		Test.stopTest();
		system.assert(sff.staffMembers.size() == 0);
		system.assert(sff.staffMemberSet.size() == 0);
    }
    
    static testMethod void SFF_LogServicesControllerRemoveServiceTest() {    		
		config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.addService();
		Integer serviceListSize = sff.serviceList.size();
		
		PageReference pageRef = Page.SFF_LogServices;
		pageRef.getParameters().put('serviceId', '0');
		Test.setCurrentPage(pageRef);
		Test.startTest();
			sff.removeService();
		Test.stopTest();
		system.assertEquals(serviceListSize - 1, sff.serviceList.size());
    }
    
    static testMethod void SFF_LogServicesControllerSaveTest() {    		
		config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.addService();
		Test.startTest();
			sff.save();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllerSaveAndNewTest(){
    	config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.addService();
		Test.startTest();
			sff.saveAndNew();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllerSaveAndStayTest(){
    	config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.addService();
		Test.startTest();
			sff.saveAndStay();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllerRollBackToBackupTest(){
    	config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.addService();
		Test.startTest();
			sff.rollbackToBackup();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllerCancelTest(){
    	config();
		SFF_LogServicesController sff = new SFF_LogServicesController();
		Test.startTest();
			sff.cancel();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllerAddPreSetTest(){
    	config();
    	Service__c testService = new Service__c(Service__c = 'TestService2',Subtype__c = 'Subtype3');
        insert testService;
    	Service_Subtype_Mapping__c serSubMap = new Service_Subtype_Mapping__c(Name='test', Service__c=testService.id);
    	insert serSubMap;		
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.preSetValue = 'test';
		Test.startTest();
			sff.addPreSet();
		Test.stopTest();
    }

    static testMethod void SFF_LogServicesControllergetServiceSubtypeMappingValuesTest(){
    	config();
    	Service__c testService = new Service__c(Service__c = 'TestService2',Subtype__c = 'Subtype3');
        insert testService;
    	Service_Subtype_Mapping__c serSubMap = new Service_Subtype_Mapping__c(Name='test', Service__c=testService.id);
    	insert serSubMap;		
		SFF_LogServicesController sff = new SFF_LogServicesController();
		sff.newService.Service__c = 'TestService';
		sff.newService.Subtype__c = 'Subtype4';
		sff.preSetValue = 'test';
		Test.startTest();
			SFF_LogServicesController.getServiceSubtypeMappingValues();
		Test.stopTest();
    }		

}