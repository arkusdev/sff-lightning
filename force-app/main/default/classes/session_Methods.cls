public with sharing class session_Methods { 

	public static boolean hasRun = false;

	public static void afterInsert (map<id,Session__c> newmap) {
		
		list<Session__c> triggerSession = [Select s.Case__c, s.SystemModstamp, s.Session_Date__c, s.Service__c, s.service__r.start_date__c, s.service__r.end_date__c, s.service__r.Frequency__c, s.OwnerId, s.Name, s.LastModifiedDate, s.LastModifiedById, s.LastActivityDate, s.IsDeleted, s.Id, s.CreatedDate, s.CreatedById, s.Contact__c, s.Attendance_Status__c From Session__c s where id IN :newmap.keyset()];
		list<Session__c> newSessions = new list<Session__c>();
		
		if (hasRun == false ) {//|| test.isRunningTest()){
			for (Session__c s : triggerSession){
				if (s.service__r.frequency__c == 'Weekly') {
					//every 7 days
					Date futureDate = s.Session_Date__c.addDays(7);
					while (futureDate <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futuredate;
						newSessions.add(newS);
						futureDate = futureDate.addDays(7);
					}
					
				}
				else if (s.service__r.frequency__c == 'Bi Weekly') {
					//every 14 days
					Date futureDate = s.Session_Date__c.addDays(14);
					while (futureDate <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futuredate;
						newSessions.add(newS);
						futureDate = futureDate.addDays(14);
					}
				}
				else if (s.service__r.frequency__c == 'Monthly') {
					Date futureDate = s.Session_Date__c.addMonths(1);
					while (futureDate <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futuredate;
						newSessions.add(newS);
						futureDate = futureDate.addMonths(1);
					}
				}
				else if (s.service__r.frequency__c == 'M, W') {
					// Get today's day of the week
					DateTime dt = DateTime.newInstance(s.Session_Date__c, Time.newInstance(0,0,0,0));
					String dayOfWeek = dt.format('EEE');
					System.debug(dayOfWeek);
					
					Date futureMon = null;
					Date futureWed = null;
					if(dayOfWeek == 'Mon') {
						futureMon = s.Session_Date__c.addDays(7);
						futureWed = s.Session_Date__c.addDays(2);
					}
					if(dayOfWeek == 'Tue') {
						futureMon = s.Session_Date__c.addDays(6);
						futureWed = s.Session_Date__c.addDays(1);
					}
					if(dayOfWeek == 'Wed') {
						futureMon = s.Session_Date__c.addDays(5);
						futureWed = s.Session_Date__c.addDays(7);
					}
					if(dayOfWeek == 'Thu') {
						futureMon = s.Session_Date__c.addDays(4);
						futureWed = s.Session_Date__c.addDays(6);
					}
					if(dayOfWeek == 'Fri') {
						futureMon = s.Session_Date__c.addDays(3);
						futureWed = s.Session_Date__c.addDays(5);
					}
					if(dayOfWeek == 'Sat') {
						futureMon = s.Session_Date__c.addDays(2);
						futureWed = s.Session_Date__c.addDays(4);
					}
					if(dayOfWeek == 'Sun') {
						futureMon = s.Session_Date__c.addDays(1);
						futureWed = s.Session_Date__c.addDays(3);
					}
					
					while (futureMon <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureMon;
						newSessions.add(newS);
						futureMon = futureMon.addDays(7);
					}
					
					while (futureWed <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureWed;
						newSessions.add(newS);
						futureWed = futureWed.addDays(7);
					}
				}
				else if (s.service__r.frequency__c == 'T, W') {
					// Get today's day of the week
					DateTime dt = DateTime.newInstance(s.Session_Date__c, Time.newInstance(0,0,0,0));
					String dayOfWeek = dt.format('E');
					System.debug(dayOfWeek);
					
					Date futureTue = null;
					Date futureWed = null;
					if(dayOfWeek == 'Mon') {
						futureTue = s.Session_Date__c.addDays(1);
						futureWed = s.Session_Date__c.addDays(2);
					}
					if(dayOfWeek == 'Tue') {
						futureTue = s.Session_Date__c.addDays(7);
						futureWed = s.Session_Date__c.addDays(1);
					}
					if(dayOfWeek == 'Wed') {
						futureTue = s.Session_Date__c.addDays(6);
						futureWed = s.Session_Date__c.addDays(7);
					}
					if(dayOfWeek == 'Thu') {
						futureTue = s.Session_Date__c.addDays(5);
						futureWed = s.Session_Date__c.addDays(6);
					}
					if(dayOfWeek == 'Fri') {
						futureTue = s.Session_Date__c.addDays(4);
						futureWed = s.Session_Date__c.addDays(5);
					}
					if(dayOfWeek == 'Sat') {
						futureTue = s.Session_Date__c.addDays(3);
						futureWed = s.Session_Date__c.addDays(4);
					}
					if(dayOfWeek == 'Sun') {
						futureTue = s.Session_Date__c.addDays(2);
						futureWed = s.Session_Date__c.addDays(3);
					}
					
					while (futureTue <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureTue;
						newSessions.add(newS);
						futureTue = futureTue.addDays(7);
					}
					
					while (futureWed <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureWed;
						newSessions.add(newS);
						futureWed = futureWed.addDays(7);
					}
				}
				else if (s.service__r.frequency__c == 'T, W, TH') {
					// Get today's day of the week
					DateTime dt = DateTime.newInstance(s.Session_Date__c, Time.newInstance(0,0,0,0));
					String dayOfWeek = dt.format('E');
					System.debug(dayOfWeek);
					
					Date futureTue = null;
					Date futureWed = null;
					Date futureThu = null;
					if(dayOfWeek == 'Mon') {
						futureTue = s.Session_Date__c.addDays(1);
						futureWed = s.Session_Date__c.addDays(2);
						futureThu = s.Session_Date__c.addDays(3);
					}
					if(dayOfWeek == 'Tue') {
						futureTue = s.Session_Date__c.addDays(7);
						futureWed = s.Session_Date__c.addDays(1);
						futureThu = s.Session_Date__c.addDays(2);
					}
					if(dayOfWeek == 'Wed') {
						futureTue = s.Session_Date__c.addDays(6);
						futureWed = s.Session_Date__c.addDays(7);
						futureThu = s.Session_Date__c.addDays(1);
					}
					if(dayOfWeek == 'Thu') {
						futureTue = s.Session_Date__c.addDays(5);
						futureWed = s.Session_Date__c.addDays(6);
						futureThu = s.Session_Date__c.addDays(7);
					}
					if(dayOfWeek == 'Fri') {
						futureTue = s.Session_Date__c.addDays(4);
						futureWed = s.Session_Date__c.addDays(5);
						futureThu = s.Session_Date__c.addDays(6);
					}
					if(dayOfWeek == 'Sat') {
						futureTue = s.Session_Date__c.addDays(3);
						futureWed = s.Session_Date__c.addDays(4);
						futureThu = s.Session_Date__c.addDays(5);
					}
					if(dayOfWeek == 'Sun') {
						futureTue = s.Session_Date__c.addDays(2);
						futureWed = s.Session_Date__c.addDays(3);
						futureThu = s.Session_Date__c.addDays(4);
					}
					
					while (futureTue <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureTue;
						newSessions.add(newS);
						futureTue = futureTue.addDays(7);
					}
					
					while (futureWed <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureWed;
						newSessions.add(newS);
						futureWed = futureWed.addDays(7);
					}
					
					while (futureThu <= s.service__r.end_date__c){
						Session__c newS = new Session__c();
						newS.service__c = s.service__c;
						newS.Case__c = s.Case__c;
						newS.contact__c = s.contact__c;
						newS.Session_Date__c = futureThu;
						newSessions.add(newS);
						futureThu = futureThu.addDays(7);
					}
				}
				else if (s.service__r.frequency__c == 'One Time') {
					//do nothing
				}
			}
			hasRun = true;
		}	
		
		if (newSessions.size() > 0) {
			insert newSessions;
		}
	}
	
	@isTest
	static void sessionSchedulerTest () {
		Contact c = new Contact(firstname='testclient',lastname='testclient');
		insert c;
		Service__c serv = new Service__c(frequency__c = 'Weekly', start_date__c = system.today(), end_date__c = system.today().addDays(21));
		Service__c serv2 = new Service__c(frequency__c = 'Bi Weekly', start_date__c = system.today(), end_date__c = system.today().addDays(21));
		Service__c serv3 = new Service__c(frequency__c = 'Monthly', start_date__c = system.today(), end_date__c = system.today().addMonths(3));
		Service__c serv4 = new Service__c(frequency__c = 'M, W', start_date__c = system.today(), end_date__c = system.today().addDays(21));
		Service__c serv5 = new Service__c(frequency__c = 'T, W', start_date__c = system.today(), end_date__c = system.today().addDays(21));		
		Service__c serv6 = new Service__c(frequency__c = 'T, W, TH', start_date__c = system.today(), end_date__c = system.today().addDays(21));
		Service__c serv7 = new Service__c(frequency__c = 'One Time', start_date__c = system.today(), end_date__c = system.today().addDays(21));
		insert new list<Service__c>{serv,serv2,serv3,serv4,serv5,serv6};
		list<Session__c> allsess=new list<Session__c>();
		for(integer i=0;i<7;i++){
			Session__c ses = new Session__c(contact__c = c.id, service__c = serv.id, session_date__c = system.today().addDays(i));
			Session__c ses2 = new Session__c(contact__c = c.id, service__c = serv2.id, session_date__c = system.today().addDays(i));
			Session__c ses3 = new Session__c(contact__c = c.id, service__c = serv3.id, session_date__c = system.today().addDays(i));
			Session__c ses4 = new Session__c(contact__c = c.id, service__c = serv4.id, session_date__c = system.today().addDays(i));
			Session__c ses5 = new Session__c(contact__c = c.id, service__c = serv5.id, session_date__c = system.today().addDays(i));
			Session__c ses6 = new Session__c(contact__c = c.id, service__c = serv6.id, session_date__c = system.today().addDays(i));
			Session__c ses7 = new Session__c(contact__c = c.id, service__c = serv7.id, session_date__c = system.today().addDays(i));
			allsess.add(ses);
			allsess.add(ses2);
			allsess.add(ses3);
			allsess.add(ses4);
			allsess.add(ses5);
			allsess.add(ses6);
			allsess.add(ses7);
		}
		insert allsess;
		//insert new list<Session__c>{ses,ses2,ses3,ses4,ses5,ses6,ses7};
		
		
		
		
		
	}
}