public class EEPStatusController {
    
    @AuraEnabled
    public static EEP_Status__c getLastStatusUpdate(Id caseId){
        List<EEP_Status__c> status =  [SELECT Id, Case__c, Days__c, Date__c, Status__c, Previous_Status__c FROM EEP_Status__c WHERE Case__c =: caseId ORDER BY Date__c Desc LIMIT 1];
        if(status.isEmpty()){return NULL;}
        return status[0];
    }
    
    @AuraEnabled
    public static void updateCaseEEPStatus(Id caseId, String currentStatus, String nextStep, Date dateModified){

        System.SavePoint sp = Database.setSavepoint();
        try{
            List<Case__c> cases = [SELECT Id, EEP_Current_Status__c, EEP_Next_Step__c FROM Case__c WHERE Id =: caseId];
            if(cases.isEmpty()){
                throw new AuraHandledException('Case not found');
            }
			EEP_Status__c status =  getLastStatusUpdate(caseId);
            Case__c cas = cases[0];
            cas.EEP_Current_Status__c = currentStatus;
            cas.EEP_Next_Step__c = nextStep;
            update(cas);
			EEP_Status__c sta = new EEP_Status__c();
			sta.Date__c = dateModified;
            sta.Status__c = cas.EEP_Current_Status__c;
			sta.Case__c = cas.Id;
            if(status != null){
                sta.Previous_Status__c = status.Id;
            }
            insert(sta);
        }catch(System.DmlException dml){
            Database.rollback(sp);
            throw new AuraHandledException(dml.getMessage());
        }catch (Exception e){
            Database.rollback(sp);
            throw new AuraHandledException(e.getMessage());
        }
    }
}