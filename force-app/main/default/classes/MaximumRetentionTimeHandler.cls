public with sharing class MaximumRetentionTimeHandler {
	
	Map<Id, Case__c> idCasesMap;
	Map<Id, List<Job_Placement__c>> caseIdJobPlacementMap;
	
	public void retrieveCases(){
		
		idCasesMap = new Map<Id, Case__c>([SELECT Maximum_Retention_Time__c FROM Case__c WHERE RecordType.Name = 'Economic Empowerment']);
		
	}
	
	public void retrieveJobPlacements(){
		
		Set<Id> casesIds = idCasesMap.keySet();
		List<Job_Placement__c> jobPlacements = [SELECT Start_Date__c, End_Date__c, Case__c FROM Job_Placement__c WHERE Case__c IN :casesIds AND RecordType.Name = 'Job Placement' ORDER BY Start_Date__c ASC];
		caseIdJobPlacementMap = new Map<Id, List<Job_Placement__c>>();
		
		for (Job_Placement__c jobPlacement : jobPlacements){
			List<Job_Placement__c> jobList = new List<Job_Placement__c>();
			if (caseIdJobPlacementMap.get(jobPlacement.Case__c) != null){
				jobList = caseIdJobPlacementMap.get(jobPlacement.Case__c);
			}
			jobList.add(jobPlacement);
			caseIdJobPlacementMap.put(jobPlacement.Case__c, jobList);
		}
		
	}
	
	public Integer calculateMaximumRetentionTime(List<Job_Placement__c> jobPlacements){
		for(Job_Placement__c jobPlacement : jobPlacements){
			if(jobPlacement.End_Date__c == null){
				jobPlacement.End_Date__c = Date.today();
			}
		}
		Integer maximumRetentionTime = 0;
		Integer startIndex = 0;
		while (startIndex <= jobPlacements.size()-1){
			Integer endIndex = startIndex;
			system.debug('endIndex < (jobPlacements.size()-1) '+ (endIndex < (jobPlacements.size()-1)));
			while (endIndex < (jobPlacements.size()-1) && jobPlacements[endIndex].End_Date__c.daysBetween(jobPlacements[endIndex+1].Start_Date__c)<=60){
				endIndex++;
				system.debug('endIndex ' + endIndex);
			}
			Integer retentionTime = jobPlacements[startIndex].Start_Date__c.daysBetween(jobPlacements[endIndex].End_Date__c);
			if(retentionTime > maximumRetentionTime){
				maximumRetentionTime = retentionTime;
			}
			
			startIndex = endIndex+1;
			
			system.debug('startIndex < jobPlacements.size()-1 ' + (startIndex < jobPlacements.size()-1));
		}
		return maximumRetentionTime;
	}
	
	public void updateMaximumRetentionTimeOnCases(){
		
		retrieveCases();
		retrieveJobPlacements();
		
		Set<Id> casesIds = idCasesMap.keySet();
		
		for (Id caseId : casesIds){
			if (caseIdJobPlacementMap.get(caseId) != null){
				Integer maximumRetentionTime = calculateMaximumRetentionTime(caseIdJobPlacementMap.get(caseId));
				idCasesMap.get(caseId).Maximum_Retention_Time__c = maximumRetentionTime;
			}
		}
		
		update idCasesMap.values();
	}
	
}