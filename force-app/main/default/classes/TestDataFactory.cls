public with sharing class TestDataFactory {

	public static User createUser(String profileName){

		User u = new User(
	 	ProfileId = [SELECT Id FROM Profile WHERE Name = :profileName].Id,
	 	LastName = 'last',
	 	Email = 'test@testfalse.com',
	 	Username = 'test@sff.com' + UserInfo.getOrganizationId(),
	 	CompanyName = 'TEST',
	 	Title = 'title',
	 	Alias = 'alias',
	 	TimeZoneSidKey = 'America/Los_Angeles',
	 	EmailEncodingKey = 'UTF-8',
	 	LanguageLocaleKey = 'en_US',
		LocaleSidKey = 'en_US'
		);
		insert u;
		return u;
	}

	public static Contact createContact(Integer num){
		Contact con = new Contact(firstname='Testman'+String.valueOf(num), LastName='Testman'+String.valueOf(num));
		insert con;
		return con;
	}

	public static Npe4__Relationship__c createRelationship(Id parentId, Id relatedId){
		Npe4__Relationship__c rel = new Npe4__Relationship__c(
									Npe4__Contact__c=parentId, 
									Npe4__RelatedContact__c=relatedId,
									Npe4__Status__c='Current',
									Npe4__Type__c = 'Son');
		insert rel;
		return rel;
	}

	public static Case__c createCase(String contactId){
		Case__c cas = new Case__c(Type_of_Assistance__c='After School Program',Type_of_Matter__c='Educational Enhancement Program', Contact__c=contactId);
		insert cas;
		return cas;
	}
}