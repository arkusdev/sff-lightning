@isTest
public class SFF_ServiceSubtypeMappingControllerTest {

	static testMethod void SFF_ServiceSubtypeMappingControllerTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff;
		Test.startTest();
			sff = new SFF_ServiceSubtypeMappingController(sc);
		Test.stopTest();
		system.assert(String.isEmpty(sff.serviceSelected));
		system.assert(String.isEmpty(sff.subtypeSelected));
		system.assert(sff.slectedSubtypes.isEmpty());
		system.assert(sff.service != null);
		system.assert(String.isEmpty(sff.subtypeAreaOfFocus));
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerExistingIdTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ssm.Service__c = 'TestService';
		ssm.Subtypes__c = 'Subtype1,Subtype2';
		ssm.Area_of_Focus__c = '--None--';
		insert ssm;
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff;
		Test.startTest();
			sff = new SFF_ServiceSubtypeMappingController(sc);
		Test.stopTest();
		system.assertEquals(ssm.Id, sff.SSMapping.Id);
		system.assertEquals(ssm.Service__c, sff.serviceSelected);
		system.assert(String.isEmpty(sff.subtypeSelected));
		system.assert(sff.service != null);
		system.assertEquals(ssm.Service__c, sff.service.Service__c);
		system.assertEquals(ssm.Area_of_Focus__c, sff.subtypeAreaOfFocus);
		system.assert(sff.slectedSubtypes.size() == 2);
		
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerDeleteAllSubtypesTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ssm.Service__c = 'TestService';
		ssm.Subtypes__c = 'Subtype1,Subtype2';
		ssm.Area_of_Focus__c = '--None--';
		insert ssm;
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		Test.startTest();
			sff.subtypeDele = -1;
			sff.deleteSubtype();		
		Test.stopTest();
		system.assert(sff.slectedSubtypes.isEmpty());
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerDeleteSubtypeTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ssm.Service__c = 'TestService';
		ssm.Subtypes__c = 'Subtype1,Subtype2';
		ssm.Area_of_Focus__c = '--None--';
		insert ssm;
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		Test.startTest();
			sff.subtypeDele = 0;
			sff.deleteSubtype();		
		Test.stopTest();
		system.assert(sff.slectedSubtypes.size() == 1);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerAddSubtypeTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ssm.Service__c = 'TestService';
		ssm.Subtypes__c = 'Subtype1,Subtype2';
		ssm.Area_of_Focus__c = '--None--';
		insert ssm;
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		Test.startTest();
			sff.service.Subtype__c = 'Subtype3';
			sff.addSubtype();		
		Test.stopTest();
		system.assert(sff.slectedSubtypes.size() == 3);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerSaveTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		sff.service.Service__c = 'TestService';
		sff.service.Subtype__c = 'Subtype1';
		sff.addSubtype();
		String response;
		Test.startTest();
			response = sff.save();
		Test.stopTest();
		system.assert([select Id from Service_Subtype_Mapping__c where Id = : response].size() == 1);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerSaveExistingTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ssm.Service__c = 'TestService';
		ssm.Subtypes__c = 'Subtype1,Subtype2';
		ssm.Area_of_Focus__c = '--None--';
		insert ssm;
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		String response;
		Test.startTest();
			response = sff.save();
		Test.stopTest();
		system.assertEquals(ssm.Id, response);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerNewServiceTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		sff.service.Service__c = 'TestService';
		sff.service.Subtype__c = 'Subtype1';
		sff.addSubtype();
		Test.startTest();
			pageRef = sff.newService();
		Test.stopTest();
		system.assert([select Id from Service_Subtype_Mapping__c].size() == 1);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerRefPageTest() {
		PageReference pageRef = Page.SFF_ServiceSubtypeMapping;
		Test.setCurrentPage(pageRef);
		Service_Subtype_Mapping__c ssm = new Service_Subtype_Mapping__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(ssm);
		SFF_ServiceSubtypeMappingController sff = new SFF_ServiceSubtypeMappingController(sc);
		sff.service.Service__c = 'TestService';
		sff.service.Subtype__c = 'Subtype1';
		sff.addSubtype();
		Test.startTest();
			pageRef = sff.refPage();
		Test.stopTest();
		system.assert([select Id from Service_Subtype_Mapping__c].size() == 1);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerGetServicesValuesTest() {
		List<String> response;
		Test.startTest();
			response = SFF_ServiceSubtypeMappingController.getServicesValues();
		Test.stopTest();
		system.assert(response.size() > 0);
	}
	
	static testMethod void SFF_ServiceSubtypeMappingControllerGetAreaOfFocusValues() {
		List<String> response;
		Test.startTest();
			response = SFF_ServiceSubtypeMappingController.getAreaOfFocusValues();
		Test.stopTest();
		system.assert(response.size() > 0);
	}
	
}