public with sharing class checkIn_Extension {

	public Shelter__c thisShelter {get;set;}
	public list<Case__c> todaysCases {get;set;}
	public boolean closenight {get;set;}
	public Shelter_Status__c night {get;set;}
	public date selecteddate {get;set;}
	public Contact dateholder {get;set;}
	
	public boolean quickadd {get;set;}
	public Contact quickContact {get;set;}
	public string lastnameholder {get;set;}
	Public Case__c quickCase {get;set;}
	public Night__c quickNight {get;set;}
	
	public list<Night__c> todaysStays {get;set;}
	
	public checkIn_Extension (apexpages.standardcontroller cont) {
		thisShelter = (Shelter__c)cont.getRecord();
		thisShelter = [select id, name, capacity__c from Shelter__c where id = :thisShelter.id];
		selecteddate = system.today();
		dateholder = new Contact(); 
		dateholder.birthdate = selecteddate;
		
		todaysCases = new list<Case__c>();
//		getCases(); //DEPRECATED, USING NIGHTS
		todaysStays = new list<Night__c>();
		getStays();
	}
/*	
	public pagereference getCases(){
		selectedDate = dateholder.birthdate;
		todaysCases = [select c.walk_in__c, c.id, c.contact__r.lastname, c.contact__r.firstname, c.contact__r.birthdate, c.Date_Service_was_Provided__c, c.NYC_vs_Out_of_Country__c, c.contact__r.npo02__Household__r.name, c.Attendance_status__c, c.DPE_Capacity_Status__c from Case__c c where Shelter__c = :thisShelter.id AND Date_Service_was_Provided__c = :selectedDate order by contact__r.npo02__Household__r.name, contact__r.head_of_household__c desc, contact__R.lastname, contact__R.firstname, contact__r.birthdate];
		return null;
	}
*/
	public pagereference getStays(){
		selectedDate = dateholder.birthdate;
		todaysStays = [select id, attendance_status__c, case__c, DPE_Capacity_Status__c, date_service_Was_provided__c, case__r.NYC_vs_Out_of_Country__c, case__r.contact__R.lastname, case__r.contact__R.firstname, case__r.contact__R.birthdate, case__r.DPE_Capacity_Status__c, case__r.contact__r.npo02__Household__r.name from Night__c where case__r.shelter__c = :thisshelter.id AND date_service_Was_provided__c = :selectedDate order by case__r.contact__r.npo02__Household__r.name, case__r.contact__r.head_of_household__c desc, case__r.contact__R.lastname, case__r.contact__R.firstname, case__r.contact__r.birthdate];
		return null;
	}
/*	
	public pagereference saveCases () {
		update todaysCases;
		return null;
	} 
*/	
	public pagereference saveStays() {
		update todaysStays;
		return null;	
	}
	
	public pagereference allpresent(){
		for (Night__c n : todaysStays){
			n.Attendance_status__c = 'Present';
		}
		return null;
	}
	
	public pagereference closeNight() {
		closenight=true;
		string nightname = thisShelter.name + ' - ' + selecteddate.Month()+'/'+ selecteddate.day() +'/'+ selecteddate.year();
		
		list<Shelter_Status__c> existingNight = [Select s.Walk_Ins__c, s.Unit_Vacancy_Description__c, s.Total_Empty_Units__c, s.SystemModstamp, s.Shelter__c, s.Present__c, s.Partial_Unit_Description__c, s.Number_of_Vacancies__c, s.Name, s.Maintenance_Description__c, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Id, s.Expected__c, s.Excused_Absence__c, s.Empty_Due_to_Unit_under_Maintenance__c, s.Empty_Due_to_Unit_Vacancy__c, s.Empty_Due_to_Partial_Unit__c, s.Date__c, s.CreatedDate, s.CreatedById, s.Absent__c From Shelter_Status__c s where name = :nightname ] ;
		if (existingNight.size() == 0) {
			night = new Shelter_Status__c();
			night.shelter__c = thisShelter.id;
			night.date__c = selecteddate;
			night.name = nightname;	
		}
		else {
			night = existingNight[0];
		}
		
		night.Number_of_Vacancies__c = 0;
		if (thisShelter.capacity__c != null) {
			night.Number_of_Vacancies__c = thisShelter.capacity__c;
		}
		
		night.Empty_Due_to_Unit_Vacancy__c=0;
		night.Empty_Due_to_Partial_Unit__c=0;
		night.Empty_Due_to_Unit_under_Maintenance__c=0;
		night.absent__c = 0;
		night.excused_absence__c = 0;
		night.present__c = 0;
		night.expected__c = todaysCases.size();
		night.walk_ins__c = 0;
		/*
		for (Case__c c : todaysCases){
			if (c.Attendance_status__c == 'Present') {
				night.Number_of_Vacancies__c--; 
				night.present__c++;
			}
			if (c.Attendance_status__c == 'Excused Absence') {
				night.excused_absence__c++;
			} 
			if (c.Attendance_status__c == 'Absent') {
				night.absent__c++;
			}
			if (c.walk_in__c == true) {
				night.expected__c--;
				night.walk_ins__c++;
			}
		}*/
		
		for (Night__c n : todaysStays) {
			night.Number_of_Vacancies__c--;
			if (n.Attendance_status__c == 'Present') { 
				night.present__c++;
			}
			if (n.Attendance_status__c == 'Excused Absence') {
				night.excused_absence__c++;
			} 
			if (n.Attendance_status__c == 'Absent') {
				night.absent__c++;
			}
		}
		return null;
	}
	
	public pagereference uncloseNight () {
		closenight = false;
		return null;
	}	
	
	public pagereference confirmNight () {
		if (night.Number_of_Vacancies__c == (night.Empty_Due_to_Unit_Vacancy__c+night.Empty_Due_to_Partial_Unit__c+night.Empty_Due_to_Unit_under_Maintenance__c)) {
			upsert night;
			pagereference p = new pagereference('/'+thisshelter.id);
			return p;	
		}
		else {
			apexpages.addmessage(new apexpages.message(apexpages.severity.error, 'Vacancy reasons must add up to total vacancies.'));
			return null;
		}
		
	}
	
	public pagereference doQuickAdd() {
		quickadd = true;
		quickContact = new Contact();
		lastnameholder = '';
		quickCase = new Case__c();
//		quickcase.attendance_Status__c = 'Present';
//		quickcase.walk_in__c = true;
		quickNight = new Night__c();
		quicknight.Attendance_status__c = 'Present';
		return null;
	}
	
	public pagereference quickinsert () {
		if (quickcase.contact_placeholder__c != null) {
			quickcase.Contact__c = quickcase.contact_placeholder__c;
		}
		else {
			quickcontact.lastname = lastnameholder;
			quickcontact.RecordTypeId = [select id from RecordType where name = 'Direct Service Client'].id;
			insert quickContact;
			quickcase.Contact__c = quickcontact.id;
		}
		
		
		quickcase.Shelter__c = thisShelter.id;
//		quickcase.Date_Service_was_Provided__c = selecteddate;
		quickcase.RecordTypeId = [select id from RecordType where name = 'Shelter' and SobjectType= 'Case__c'].id;
		quickcase.contact_placeholder__c = null;
		insert quickcase;
		
		quicknight.Date_Service_was_Provided__c = selecteddate;
		insert quicknight;
		getStays();
		
//		getCases();
		closequick();
		return null;
	}
	
	public pagereference closequick() {
		quickadd = false;
		return null;
	}
}