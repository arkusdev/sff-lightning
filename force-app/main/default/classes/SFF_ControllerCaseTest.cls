@isTest
public class SFF_ControllerCaseTest {
    static testMethod void testMethodCase(){
        //
        //
       
        
        contact testContact = new contact();
        testContact.FirstName = 'firstname';
        testContact.LastName = 'lastname';
        testContact.Birthdate = date.newInstance(1987, 3, 3);
        insert testContact;
        //
        Case__c testCase = new Case__c();
		testCase.Contact__c = testContact.id;
        testCase.Project__c = 'Children & Youth Program';
        testCase.Location__c ='BxFJC';
        testCase.Case_Open_Date__c = date.today();
        testCase.Matter_Type_Detail__c = 'Education';
        insert testCase;
        //
        Event__c testEvent = new Event__c();
        testEvent.Case__c = testCase.id;
        testEvent.Mode__c = 'In Office';
        testEvent.Date__c =Date.today();
        testEvent.Event_Note__c = 'text sample';
        insert testEvent;
        //
        Night__c testNight = new Night__c();
        testNight.Case__c = testCase.id; 
        date aux =  Date.today();
        testNight.Date_Service_was_Provided__c = aux;
        insert testNight;
        //
        Session__c testSession = new Session__c();
        testSession.Contact__c = testContact.id;
        testSession.Case__c = testCase.id;
        testSession.Session_Date__c = Date.today();
        insert testSession;
        //
        Service__c testService = new Service__c();
        testService.Event__c = testEvent.id;
        testService.Service__c = 'Advice (Legal)';
        testService.Subtype__c='Abuse & Neglect';
        testService.Area_of_Focus__c ='Child/Parent Services - Children\'s/Family Activities';
        testService.Mode__c='In Office';
        testService.Service_Date__c= Date.today();
        insert testService;
        
        PageReference pageRef = Page.SFF_Case_Detail;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCase = new ApexPages.StandardController(testCase);
        SFF_CaseDetailController control = new SFF_CaseDetailController(stdCase );
        

        control.NotesRadio = '2';
        control.NoteRadioList();
        control.CaseRadio  = '1';
        control.ServicesFromCasesRadio();
        control.ServicesRadio = '2';
        control.CaseRadioList();
        control.OrderByCase();
        control.OrderBy();

        control.NotesRadio = '1';
        control.NoteRadioList();
        control.CaseRadio = '2';
        control.ServicesRadio = '1';
        control.CaseRadioList();
        control.OrderByCase();
        control.OrderBy();
        control.fillNoteLists();

        
    }
}