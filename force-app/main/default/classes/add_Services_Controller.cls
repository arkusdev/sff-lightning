public with sharing class add_Services_Controller {

	public set<string> options {get;set;}
	public set<string> suboptions {get;set;}
	public set<string> aofoptions {get;set;}
	public string typing {get;set;}
	public string subtyping {get;set;}
	public string aoftyping {get;set;}
	public set<string> dataSet {get;set;}
	public set<string> subdataSet {get;set;}
	public set<string> aofdataSet {get;set;}
	public String selectedService {get; set;}
	public String selectedSubType {get; set;}
	public String selectedAof {get; set;}
	
	public List<String> availableServices {get; set;}
	public List<String> availableSubTypes {get; set;}
	public List<String> availableAreasOfFocus {get; set;}
	
	public list<ServiceWrapper> servicelist{get;set;}
	public Service__c currentService {get;set;}
	
	public Event__c incomingEvent {get; set;}
	public Id incomingEventID {get; set;}
	public Id incomingCaseID {get; set;}
	//public Id staffHolderID {get; set;}
	public Integer removeServiceIndex {get; set;}
	
	public Integer globalIndex {get; set;}
	
	public Integer staffMemberCount {get; set;}
	public List<StaffWrapper> staffMembers {get; set;}
	public Integer removeStaffIndex {get; set;}

	public class ServiceWrapper {
		public Service__c serv {get; set;}
		public Integer index {get; set;}
		public String serv_str {get; set;}
		public String staffMemberStr {get; set;}
		public ServiceWrapper(Service__c s, Integer i) {
			staffMemberStr = '';
			serv = s;
			index = i;
			serv_str = s.Service__c;
			serv_str = serv_str + ', ' + ((s.SubType__c == 'Area of Focus') ? s.Area_of_Focus__c: s.SubType__c);
			if(s.Dollar_Amount_of_Service_Provided__c != null) {
				serv_str = serv_str + ', $' + s.Dollar_Amount_of_Service_Provided__c;
			}
			if(s.Hours_of_Service_Provided__c != null) {
				serv_str = serv_str + ', ' + s.Hours_of_Service_Provided__c + 'hr';
			}
		}
	}
	
	public class StaffWrapper {
		public Contact staffMember {get; set;}
		public Integer index {get; set;}
		
		public StaffWrapper(Contact s, Integer i) {
			staffMember = s;
			index = i;
		}
	}

	public add_Services_Controller() {
		availableServices = new List<String>();
		availableSubTypes = new List<String>();
		availableAreasOfFocus = new List<String>();
		
		for (Schema.Picklistentry ple : Service__c.service__c.getDescribe().getPickListValues()) {
			availableServices.add(ple.getlabel());
		}
		for (Schema.Picklistentry ple : Service__c.subtype__c.getDescribe().getPickListValues()) {
			availableSubTypes.add(ple.getlabel());
		}
		for (Schema.Picklistentry ple : Service__c.Area_of_Focus__c.getDescribe().getPickListValues()) {
			availableAreasOfFocus.add(ple.getlabel());
		}
		
		serviceList = new list<ServiceWrapper>();
		currentService = new Service__c();
		typing = '';
		subtyping = '';
		aoftyping = '';
		selectedService = '';
		selectedSubType = '';
		selectedAof = '';
		globalIndex = 0;
		staffMemberCount = 0;
		
		Id c = null;
		List<Contact> conList = [Select Id from Contact where Salesforce_user__c = :Userinfo.getUserId()];
		if(conList.size() == 1) {
			c = conList.get(0).Id;
		}
		currentService.Staff_Member__c = c;
		staffMembers = new List<StaffWrapper>();
		
		options = new set<string>();
		dataset= new set<string>();
		for (Schema.Picklistentry ple : Service__c.service__c.getDescribe().getPickListValues()) {
			dataset.add(ple.getlabel());
		}
		
		suboptions = new set<String>();		
		subdataset= new set<string>();
		for (Schema.Picklistentry ple : Service__c.subtype__c.getDescribe().getPickListvalues()) {
			subdataset.add(ple.getlabel());
		}
		
		aofoptions = new set<String>();		
		aofdataset= new set<string>();
		for (Schema.Picklistentry ple : Service__c.Area_of_Focus__c.getDescribe().getPickListvalues()) {
			aofdataset.add(ple.getlabel());
		}
		
		// Get the event passed from the add_event page
		if(ApexPages.currentPage().getParameters().get('eid') != null) {
			incomingEventID = ApexPages.currentPage().getParameters().get('eid');
			incomingEvent = [select id, Event_Note__c, date__c from Event__c where Id = :incomingEventID];
		}
		else {
			incomingEventID = null;
			
		}
		
		if(ApexPages.currentPage().getParameters().get('cid') != null) {
			incomingCaseID = ApexPages.currentPage().getParameters().get('cid');
		}
		else {
			incomingCaseID = '';
		}
		
		addStaffMember();
		currentService.Staff_Member__c = null;
	}
	
	public pagereference refreshOptions() {
		system.debug(selectedService);
		system.debug(selectedSubtype);
		system.debug(selectedAof);
		system.debug(subtyping);
		system.debug(aoftyping);
		
		if(selectedService != typing) {
			selectedService = '';
			selectedSubtype = '';
			selectedAof = '';
			subtyping = '';
			aoftyping = '';
			refreshSubOptions();
			refreshAofOptions();
		}
		set<string> temp = new set<string>();
		
		if(typing != '') {
			for (string s : dataset) {
				string temps = s.toLowerCase();
				if (temps.startswith(typing.tolowercase())) temp.add(s);
			}
		}
		options = temp;
		return null;
	}
	
	public pagereference refreshsubOptions() {
		set<string> temp = new set<string>();
		
		if(selectedSubType != subtyping) {
			selectedSubType = '';
			selectedAof = '';
			aoftyping = '';
			refreshAofOptions();
		}
		
		if(subtyping != '') {
			if (subtypedependencymap.containskey(typing)) subdataset = subtypeDependencyMap.get(typing);
			for (string s : subdataset) {
				string temps = s.toLowerCase();
				if (temps.startswith(subtyping.tolowercase())) temp.add(s);
			}
		}
		suboptions = temp;
		
		return null;
	}
	
	public pagereference refreshAofOptions() {
		
		set<string> temp = new set<string>();
		
		if(aoftyping != '') {
			for (string s : aofdataset) {
				string temps = s.toLowerCase();
				if (temps.startswith(aoftyping.tolowercase())) temp.add(s);
			}
		}
		aofoptions = temp;
		return null;
	}
	
	public PageReference selectService() {
		typing = selectedService;
		refreshOptions();
		if (subtypedependencymap.containskey(typing)) suboptions = subtypeDependencyMap.get(typing);
		if(suboptions.contains('Area of Focus')) {
			selectedSubType = 'Area of Focus';
			subtyping = 'Area of Focus';
		}
		return null;
	}
	
	public PageReference selectSubType() {
		subtyping = selectedSubType;
		refreshsubOptions();
		return null;
	}
	
	public PageReference selectAof() {
		aoftyping = selectedAof;
		refreshaofOptions();
		return null;
	}
	
	public PageReference addServiceToList() {
		if (subtyping != null && subtyping == 'Area of Focus' && (selectedAof == null || selectedAof.trim().length()==0)){
			apexpages.addmessage(new apexpages.message(apexpages.severity.info, 'You must specify an area of focus for this service.'));
			return null;
		}
		if (staffMembers.size() == 0){
			apexpages.addmessage(new apexpages.message(apexpages.severity.info, 'You must have at least one staff member in a service'));
			return null;
		}
		//staffHolderId = currentService.Staff_Member__c;
		
		String memStr = '';
		
		for(Integer i = 0; i < staffMembers.size(); i++) {
			if(i == 0) {
				currentService.Staff_Member__c = staffMembers.get(0).staffmember.Id;
				memStr += staffMembers.get(0).staffmember.Name;
			}
			if(i == 1) {
				currentService.Staff_Member2__c = staffMembers.get(1).staffmember.Id;
				memStr += '; ' + staffMembers.get(1).staffmember.Name;
			}
			if(i == 2) {
				currentService.Staff_Member3__c = staffMembers.get(2).staffmember.Id;
				memStr += '; ' + staffMembers.get(2).staffmember.Name;
			}
			if(i == 3) {
				currentService.Staff_Member4__c = staffMembers.get(3).staffmember.Id;
				memStr += '; ' + staffMembers.get(3).staffmember.Name;
			}
			if(i == 4) {
				currentService.Staff_Member5__c = staffMembers.get(4).staffmember.Id;
				memStr += '; ' + staffMembers.get(4).staffmember.Name;
			}
			if(i == 5) {
				currentService.Staff_Member6__c = staffMembers.get(5).staffmember.Id;
				memStr += '; ' + staffMembers.get(5).staffmember.Name;
			}
			if(i == 6) {
				currentService.Staff_Member7__c = staffMembers.get(6).staffmember.Id;
				memStr += '; ' + staffMembers.get(6).staffmember.Name;
			}
			if(i == 7) {
				currentService.Staff_Member8__c = staffMembers.get(7).staffmember.Id;
				memStr += '; ' + staffMembers.get(7).staffmember.Name;
			}
			if(i == 8) {
				currentService.Staff_Member9__c = staffMembers.get(8).staffmember.Id;
				memStr += '; ' + staffMembers.get(8).staffmember.Name;
			}
			if(i == 9) {
				currentService.Staff_Member10__c = staffMembers.get(9).staffmember.Id;
				memStr += '; ' + staffMembers.get(9).staffmember.Name;
			}
		}
		
		currentService.Service__c = selectedService;
		currentService.Subtype__c = selectedSubType;
		if(selectedSubType == 'Area of Focus') {
			currentService.Area_of_Focus__c = selectedAof;
		}
		currentService.Event__c = incomingEventId;
		ServiceWrapper tempSW = new ServiceWrapper(currentService, globalIndex);
		tempSW.staffMemberStr = memStr;
		globalIndex++;
		servicelist.add(tempSW);
		
		currentService = new Service__c();
		staffMembers = new List<StaffWrapper>();
		//currentService.Staff_Member__c = staffHolderId;
		Id c = null;
		List<Contact> conList = [Select Id from Contact where Salesforce_user__c = :Userinfo.getUserId()];
		if(conList.size() == 1) {
			c = conList.get(0).Id;
		}
		currentService.Staff_Member__c = c;
		addStaffMember();
		currentService.Staff_Member__c = null;
		
		typing = '';
		subtyping = '';
		selectedService = '';
		selectedSubType = '';
		options = new Set<String>();
		suboptions = new Set<String>();
		return null;
	}
	
	public PageReference removeService() {
		for(Integer i = 0; i < serviceList.size(); i++) {
			if(serviceList[i].index == removeServiceIndex) {
				serviceList.remove(i);
				break;
			}
		}
		return null;
	}
	
	public PageReference addStaffMember() {
		if(currentService.Staff_Member__c != null) {
			Contact temp = [select Id, Name from Contact where Id = :currentService.Staff_Member__c];
			StaffWrapper sw = new StaffWrapper(temp, globalIndex);
			staffMembers.add(sw);
			currentService.Staff_Member__c = null;
			globalIndex++;
		}
		return null;
	}
	
	public PageReference removeStaffMember() {
		for(Integer i = 0; i < staffMembers.size(); i++) {
			if(staffMembers[i].index == removeStaffIndex) {
				staffMembers.remove(i);
				break;
			}
		}
		return null;
	}
	
	public PageReference finalizeServices() {
		PageReference p = null;
		if(serviceList.size() > 0) {
			List<Service__c> insertServiceList = new List<Service__c>();
			for(ServiceWrapper sw : serviceList) {
				sw.serv.service_date__c = incomingevent.date__c;
				insertServiceList.add(sw.serv);
			}
			insert insertServiceList;
			update incomingEvent;
			p = new PageReference('/' + incomingEventId);
		}
		else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Add some services before finalizing');
			ApexPages.addMessage(myMsg);
		}
		return p;
	}
	
	public PageReference cancel() {
		Event__c removeEvent = [select Id from Event__c where Id = :incomingEventID];
		if(removeEvent != null) {
			delete removeEvent;
		}
		return new PageReference('/' + incomingCaseId);
	}
	
	public static final map<string,set<string>> subtypeDependencyMap = new map<string,set<string>>{
		'Over Capacity Bednight' => new set<string>{'NYC Bednight - Over Capacity', 'Out of County Bednight - Over Capacity'}, 
		'Educational Group' => new set<string>{'MO Craft Group', 'SBH Designing Women', 'MO Mentors&apos; Training-Spanish', 'BxFJC Creativity Workshop', 'SBH Jazz Journey', 'QFJC Literacy Project (Mom and Children)', 'SBH Poetry Group', 'BkFJC Literacy Project', 'SBH Creative Commercial Competition', 'SBH Saturday Academy', 'MO ESOL Group-Advanced', 'SBH Writing Club', 'MO Photography Workshop', 'SBH Science Club (ages 5-8)', 'SBH Children&apos;s No-Heat Cooking Class', 'SBH Family Learning Center', 'BxFJC Literacy Group', 'RP Reading Group', 'SBH Computer Instruction', 'MO Computer Skills Training Group', 'MO ESOL Group-Evening', 'MO ESOL Group-Day', 'SBH Science Club (ages 9-12)', 'MO Mentors&apos; Training-English', 'SBH Children’s Regular Cooking Class', 'MO ESOL Group-Beginner'}, 'Bednight' => new set<string>{'Out of County Bednight', 'NYC Bednight'}, 
		'Gift Provision' => new set<string>{'Gift Provision-FJC Margaret&apos;s Place Teddy Bear', 'SFF', 'FJC'}, 
		'Aftercare/Continuing Care Workshop' => new set<string>{'SBH Intro to Aftercare Workshop'}, 
		'Collateral Contact-Family Member' => new set<string>{'Area of Focus'}, 
		'Financial Grant (Other Private Funder)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Document Preparation' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', 'Petition/Pleading', '722-c Order', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Children&apos;s Items Provision (non-monetary)' => new set<string>{'SFF-Baby Buggy', 'SFF-non-Baby Buggy', 'FJC'}, 
		'Document Editing/Review (supervision)' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Transcript', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', '722-c Order', 'Petition/Pleading', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Shelter Client Monthly Contribution' => new set<string>{'Collected', 'Waived/Uncollected'}, 
		'Educational Session' => new set<string>{'Writing Skills', 'Computer Training', 'ESOL', 'Literacy', 'Employment Readiness'}, 
		'Document Service' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Transcript', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', '722-c Order', 'Petition/Pleading', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Case Management Session (non-legal)' => new set<string>{'Area of Focus'}, 'Document Pick-Up' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Transcript', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', '722-c Order', 'Petition/Pleading', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Financial Grant (Anonymous Relief Fund)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Client Accompaniment' => new set<string>{'Area of Focus'}, 
		'Document Analysis (non-supervision)' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Transcript', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', '722-c Order', 'Petition/Pleading', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Cash Assistance (General)' => new set<string>{'BKFJC-Family Law', 'QFJC-Immigration', 'SBH', 'RP', 'BKFJC Children&apos;s Program', 'MO Legal', 'EEP', 'HT', 'BxFJC Clinical', 'BxFJC-Family Law', 'BKFJC-Immigration', 'ER', 'MO Children&apos;s Program', 'FK', 'BxFJC-Immigration', 'QFJC Children&apos;s Program', 'MO Non-Res', 'MO Residential Program'}, 
		'Household Items Provision (non-monetary)' => new set<string>{'SFF', 'FJC'}, 'Financial Grant (Zucker Fund)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'PTSD Reaction Index' => new set<string>{'Guardian', 'Client'}, 
		'Financial Grant (USCCB)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Advocacy (Legal)' => new set<string>{'Area of Focus'}, 
		'School Aged Program Session' => new set<string>{'SBH After School Group (5-8)', 'SBH After School Group (K-4)', 'SBH After School Group (Until 9/5/06 & After 6/4/09)'}, 
		'Service Coordination-Brief' => new set<string>{'Area of Focus'}, 
		'Document Filing' => new set<string>{'Correspondence', 'Affirmation', 'Motion', 'Transcript', 'Response Discovery Demand/RFE Response', 'Final Papers', 'Summons', 'Order', 'Discovery Demand', 'Non-Case Documents', 'Stipulation', 'Judgment', '722-c Order', 'Petition/Pleading', 'Brief/Memorandum of Law', 'Other Case Documents', 'Subpoena', 'Affidavit', 'Notice', 'Application', 'Certification'}, 
		'Financial Grant (Anonymous Revolving Fund)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'MetroCards (non-monetary)' => new set<string>{'MetroCards: QFJC Literacy Project (Mom and Children)', 'MetroCards: MO Cont. Freedom-French', 'MetroCards: MO Craft Group', 'MetroCards: MO Age 8-11 Children&apos;s Support Group', 'MetroCards: MO Office Operations Workshop', 'MetroCards: BxFJC Literacy Group', 'MetroCards: MO ESOL Group-Evening', 'MetroCards: QFJC Childen&apos;s Support Group', 'MetroCards: MO ESOL Group-Beginner', 'MetroCards: MO DV 101-Spanish (Thursday)', 'FJC', 'MetroCards: MO Teen Girls&apos; Group', 'MetroCards: BxFJC Cont. Freedom-English', 'MetroCards: MO Career Readiness Workshop', 'MetroCards: MO Computer Skills Training Group', 'MetroCards: MO ESOL Group-Advanced', 'MetroCards: MO DV 101 - South Asian Support Group', 'MetroCards: BxFJC Parenting-English', 'MetroCards: BkFJC Teen Group', 'MetroCards: MO Pre-Teen Support Group', 'MetroCards: MO Cross-Cultural Parenting Workshop', 'MetroCards: MO School-Aged Children&apos;s Group', 'MetroCards: BkFJC Parenting Group-Spanish', 'MetroCards: MO DV 101-English NonRes', 'MetroCards: MO Creative Arts Group', 'MetroCards: MO DV 101-Spanish (Tuesday)', 'MetroCards: BxFJC DV 101-South Asian Group', 'MetroCards: BxFJC DV 101-English', 'MetroCards: MO DV 101-English Res', 'MetroCards: MO Parenting-Spanish', 'MetroCards: MO Latina Community Group', 'MetroCards: MO Cont. Freedom-English', 'MetroCards: MO Age 9-12 Support Group-English', 'MetroCards: MO African Girls&apos; Support Group', 'MetroCards: MO Mentors&apos;-Spanish', 'MetroCards: QFJC Parenting Group-English', 'MetroCards: MO Crossroads Caring Circle', 'SFF', 'MetroCards: BxFJC Cont. Freedom-Spanish', 'MetroCards: MO Family Reunification', 'MetroCards: BxFJC Pre-Teen Support Group-English', 'MetroCards: MO Cross Cultural Workshop', 'MetroCards: BxFJC Teen Support Group-English', 'MetroCards: BkFJC Childen&apos;s Support Group', 'MetroCards: MO DV 101-Spanish (Monday)', 'MetroCards: BxFJC Parenting-Spanish', 'MetroCards: BkFJC Parenting Group', 'MetroCards: BkFJC Teen Girls&apos; Group', 'MetroCards: BxFJC Creativity Workshop', 'MetroCards: BkFJC Literacy Project', 'MetroCards: MO Parenting-English', 'MetroCards: MO DV 101-Spanish', 'MetroCards: MO Latina Cross-Cultural Group', 'MetroCards: MO South Asian Community Group', 'MetroCards: MO Teen Support Group', 'MetroCards: BxFJC Financial Education Group', 'MetroCards: BkFJC DV 101-English', 'MetroCards: MO Steps to Success', 'MetroCards: QFJC Adolescent Support Group-English', 'MetroCards: MO ESOL Group-Day', 'MetroCards: BkFJC Kid&apos;s Group (Parenting-Spanish)', 'MetroCards: MO Age 13-17 Support Group-English', 'MetroCards: MO DV 101-French', 'MetroCards: BxFJC DV 101-Spanish', 'MetroCards: MO Mentors&apos;-English', 'MetroCards: QFJC Pre-Teen Support Group', 'MetroCards: MO Cont. Freedom-Spanish', 'MetroCards: MO Parenting-French', 'MetroCards: MO Photography Workshop', 'MetroCards: MO Housing Workshop/Economic Planning Workshop'}, 
		'Psychiatric Session' => new set<string>{'face-to-face long', 'telephone long', 'telephone brief', 'face-to-face brief'}, 
		'Psychological Session-Individual' => new set<string>{'face-to-face long', 'telephone long', 'telephone brief', 'face-to-face brief'}, 
		'Translation/Interpretation Provision' => new set<string>{'Professional', 'Language Line', 'SFF Staff', 'MO Translation Provision-Housing Workshop', 'Volunteer'}, 
		'HRA EAF Status' => new set<string>{'Eligible - Consent Declined', 'Ineligible - No Dependent Children (not pregnant)', 'Eligible - Consent Granted', 'Ineligible - Undocumented Family'}, 
		//'Document Received' => new set<string>{'Biometrics notice', '&quot;Work Advantage Eligibility Letter&quot;', 'Notice of Receipt', '&quot;Work Advantage Certification Letter&quot;', 'Transfer of file', 'Certificate of disposition', 'Ordered card production', 'U certification', 'Request for Evidence (RFE)', 'Issued Decision', 'T certification'}, 
		'Document Received' => new set<string>{'Affidavit of Defendant','Affidavit of Service','Appeal','Appointment Notice', 'Approval Notice','Biometrics Notice','Certificate of Citizenship','Certificate of disposition','Closing Letter','Deferred Action Granted','Denial notice','Employment Authorization Document (EAD)','Extension of Prima facie determination','Extension of status','Green Card','Inquiry','Interview notice','Invoice','Issued Decision','Oath Ceremony Notice','Ordered card production','Other case documents','Prima facie determination','Receipt Notice','Registration','Request','Request for Evidence (RFE)','T certification','Transfer of file','U certification','Work Advantage Certification Letter','Work Advantage Eligibility Letter'},
		'Food Pantry (non-monetary)' => new set<string>{'SFF', 'RP City Harvest Food Provision', 'FJC'}, 
		'Past DPE Bednight' => new set<string>{'NYC Bednight - Past DPE', 'Out of County Bednight - Past DPE'}, 
		'Financial Grant (EFSP)' => new set<string>{'Mortgage', 'Rent', 'Utilities'}, 
		'Art Therapy Group' => new set<string>{'SBH Art Therapy Group (Preschool)', 'MO Art Therapy Group (Tuesday Late Evening)', 'MO Art Therapy Group (Wednesday Afternoon)', 'SBH Family Art Therapy Group', 'MO Art Therapy Group (Monday Evening)', 'MO Art Therapy Group (Tuesday Early Evening)', 'MO Creative Arts Teen Group', 'MO Art Therapy Group (Friday Morning)', 'MO Art Therapy Group (Thursday Evening)', 'MO Art Therapy Group (Monday Afternoon)', 'MO Art Therapy Group (Wednesday Evening)', 'SBH Art Therapy Group (School Aged)'}, 
		'Cash Assistance (Transportation)' => new set<string>{'QFJC-Immigration', 'SBH', 'RP', 'MO Legal', 'EEP', 'HT', 'BxFJC Clinical', 'BxFJC-Family Law', 'ER', 'MO Children&apos;s Program', 'FK', 'BxFJC-Immigration', 'QFJC Children&apos;s Program', 'MO Non-Res', 'MO Residential Program'}, 
		'Psychological Session-Parent' => new set<string>{'face-to-face long', 'telephone long', 'telephone brief', 'face-to-face brief'}, 
		'Advice (non-Legal)' => new set<string>{'Area of Focus'}, 
		'Financial Grant (NTAC)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Missed Appointment' => new set<string>{'Staff Cancellation', 'Client Cancellation', 'No Show for Appointment'}, 
		'Financial Grant (SFF)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Supervision Advice/Explanation' => new set<string>{'Area of Focus'}, 
		'Advocacy (non-Legal)' => new set<string>{'Area of Focus'}, 
		'Parenting and Play Group' => new set<string>{'MO Parenting and Play Group'}, 
		'Counseling-Individual' => new set<string>{'face-to-face long', 'telephone long', 'telephone brief', 'written', 'face-to-face brief'}, 
		'Follow-Up' => new set<string>{'Area of Focus'}, 
		'Shelter Rules &amp; Regulations' => new set<string>{'Contract Issued', 'Written Warning', 'Verbal Warning'}, 
		'Counseling-Group' => new set<string>{'SBH Pet Therapy Group', 'MO Cross Cultural Workshop', 'SBH Phase 3 Women&apos;s Group-English', 'MO Age 8-11 Children&apos;s Support Group', 'BkFJC Children&apos;s Support Group', 'QFJC Children&apos;s Support Group', 'SBH Galli Theater Children&apos;s Group', 'MO Age 9-12 Support Group-English', 'MO Family Reunification', 'SBH PM Adults&apos; Group-English', 'MO DV 101-Spanish (Monday)', 'MO Creative Arts Support Group', 'BxFJC DV 101-Spanish', 'SBH Children&apos;s Support Group Grades 4-8', 'SBH Children&apos;s Support Group Grades K-3', 'MO DV 101-English NonRes Day', 'MO Teen Girls&apos; Group', 'QFJC Pre-Teen Support Group', 'MO DV 101-Spanish', 'MO School-Aged Children&apos;s Group', 'MO DV 101-French', 'BxFJC Pre-Teen Support Group-English', 'MO Cont. Freedom-Spanish', 'MO Teen Support Group', 'MO DV 101-English NonRes', 'BxFJC DV 101-South Asian Group', 'MO DV 101-English Res', 'QFJC Adolescent Support Group-English', 'MO Latina Community Group', 'STARS Counseling Group', 'MO DV 101-Spanish (Tuesday)', 'BxFJC Teen Support Group-English', 'MO South Asian Community Group', 'MO Pre-Teen Support Group', 'SBH Adults&apos; Group-Spanish', 'MO Age 13-17 Support Group-English', 'SBH Children&apos;s Support Group Ages 8 & Up', 'SBH Teen Night', 'BkFJC Teen Group', 'SBH Phase 2 Women&apos;s Group-English', 'MO DV 101 - South Asian Support Group', 'RP Children&apos;s Support Group', 'SBH Children&apos;s Support Group Ages 9-12', 'MO Latina Cross-Cultural Group', 'MO Cont. Freedom-English', 'SBH Children&apos;s Support Group Ages 5-7', 'BxFJC DV 101-English', 'SBH Phase 4 Women&apos;s Group-English', 'MO African Girls&apos; Support Group', 'MO Cont. Freedom-French', 'BkFJC Teen Girls&apos; Group', 'SBH Children&apos;s Support Group Ages 6-8', 'SBH Boys as Teens Group', 'SBH AM Adults&apos; Group-English', 'BxFJC School-Aged Support Group-English', 'SBH Survival Skills Group', 'MO DV 101-Spanish (Thursday)', 'BxFJC Cont. Freedom-Spanish', 'BkFJC DV 101-English', 'MO Office Operations Workshop Support Group', 'SBH Children&apos;s Support Group Ages 4-5', 'BxFJC Cont. Freedom-English', 'BkFJC Pre-Teen Girls&apos; Support Group', 'SBH Teen Group'}, 
		'Appearance (Court or Administrative Agency)' => new set<string>{'Trial', 'Conciliation', 'Mandatory Dispute Resolution', 'Immigration Master Hearing', 'Fair Hearing', 'Immigration Individual Merits Hearing', 'Conference', 'Immigration Interview', 'Non-Trial'}, 
		'Evaluation Tool Distribution' => new set<string>{'Legal services', 'Adult counseling', 'Shelter'}, 
		'Intake Session' => new set<string>{'Non-Residential Counseling', 'Aftercare/Continuing Care', '(Legal) Public Benefits', 'FJC Family Law Interview', 'Residential Services', 'FJC Civil Legal Screening', 'General Legal Center', 'STARS', '(Legal) Divorce', '(Legal) Immigration', 'FJC Immigration Interview', '(Legal) Custody/Visitation', 'Economic Empowerment'}, 
		'Contact Attempt' => new set<string>{'Client Contact', 'Service Provider Contact'}, 
		'Tutorial Assistance' => new set<string>{'RP Tutoring Group'}, 
		'Children&apos;s Development Group' => new set<string>{'RP Art Therapy Group', 'SBH Mommy & Me', 'BxFJC Literacy Development Group'}, 
		'Collateral Contact-Service Provider' => new set<string>{'Area of Focus'}, 
		'Application Filing' => new set<string>{'Housing - Low-income (non-subsidy)', 'Housing - NYCHA', 'Housing - HRA DV Advantage Program', 'Public Benefits - Food Stamps', 'Housing - Children&apos;s Advantage Program', 'Housing - HPD Section 8', 'Public Benefits - Food Stamps & Medicaid', 'Public Benefits - Cash Benefits & Food Stamps', 'Housing - Fixed Income Advantage Program', 'OVS', 'Public Benefits - Cash Benefits, Food Stamps & Medicaid', 'Housing - Section 8', 'Public Benefits - Cash Benefits & Medicaid', 'Housing - Short-Term Advantage Program', 'Public Benefits - Medicaid', 'Housing - Work Advantage Program', 'Housing - Supportive Housing', 'Public Benefits - PA - Cash Assistance'}, 'Economic Planning Group' => new set<string>{'MO Economic Planning Workshop', 'MO Residential Economic Workshop', 'BxFJC Financial Education Group', 'SBH Financial Freedom'}, 
		'Financial Grant (Fansler)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}, 
		'Educational/Therapeutic Materials Provision' => new set<string>{'SFF', 'MO Craft Group Materials Provision', 'FJC'}, 
		'Advice (Legal)' => new set<string>{'Estate Planning', 'Custody', 'Law Enforcement', 'Paternity', 'Other', 'Visitation', 'Landlord & Tenant Law', 'Public Housing', 'Order of Protection', 'Contested Divorce', 'Public Benefits', 'Child Support', 'Relocation', 'Abuse & Neglect', 'Immigration Enforcement/Court', 'Criminal Prosecution', 'Spousal Support', 'Citizenship & Immigration Services', 'Uncontested Divorce', 'Criminal Defense', 'Dissolution of Marriage'}, 
		'Orientation' => new set<string>{'Benefits Orientation Session', 'Residential Family Orientation (Crisis)', 'Neighborhood Orientation', 'Housing Orientation Session', 'Legal Services Orientation', 'Children&apos;s Services Orientation', 'Shelter Orientation', 'Legal Rights Orientation'}, 
		'Mentoring-Individual' => new set<string>{'face-to-face long', 'telephone long', 'telephone brief', 'written', 'face-to-face brief'}, 
		'Career Development Group' => new set<string>{'MO Career Readiness Workshop', 'MO ESP Resume Writing-Beginners', 'MO Office Operations Workshop', 'SBH Job Readiness Group', 'MO Steps to Success', 'SBH Career Development Forum', 'SBH Resume Writing/Interview Skills Workshop'}, 
		'Clothing Provision (non-monetary)' => new set<string>{'SFF-Baby Buggy', 'SFF-non-Baby Buggy', 'FJC'}, 
		'Office of Victims Services Application Information' => new set<string>{'OVS Info - Application declined - Crime occurred out state', 'OVS Info - Application declined - Otherwise ineligible', 'OVS Info - Application declined - No proof of financial need', 'OVS Info - Application begun', 'OVS Info - Application declined - Insufficient documentation', 'OVS Info - Application declined - Client not interested', 'OVS Info - Application declined - No need for financial comp', 'OVS Info - Application Eligibility Assessment in Progress', 'OVS Info - Application declined - No crime documentation', 'OVS Info - N/A - Not provided for Clinical Reasons'}, 
		'Referral' => new set<string>{'Area of Focus'}, 
		'Housing Packet Submission' => new set<string>{'Housing - Low-income (non-subsidy)', 'Housing - NYCHA', 'Housing - Short-Term Advantage Program', 'Housing - HRA DV Advantage Program', 'Housing - Children&apos;s Advantage Program', 'Housing - HPD Section 8', 'Housing - Work Advantage Program', 'Housing - Fixed Income Advantage Program', 'Housing - Supportive Housing', 'Housing - Section 8'}, 
		'Client Visit' => new set<string>{'Home', 'Mentor', 'Hospital', 'Shelter'}, 
		'Cell Phone Provision (non-monetary)' => new set<string>{'SFF', 'FJC'}, 
		'Parenting Group' => new set<string>{'MO Parenting-Spanish', 'BxFJC Parenting-Spanish', 'MO Office Operations Workshop Parenting Group', 'MO Parenting-English', 'MO Cross-Cultural Parenting Workshop', 'SBH Parenting Group-English', 'BkFJC Parenting Group', 'BkFJC Parenting Group-Spanish', 'SBH Parenting Group-Spanish', 'QFJC Parenting Group-English', 'BxFJC Parenting-English', 'MO Parenting-French'}, 
		'House Meeting/Residents Meeting' => new set<string>{'Eleanor Roosevelt House Meeting', 'Frida Kahlo House Meeting', 'Harriet Tubman House Meeting', 'RP Residents Meeting'}, 
		'Information' => new set<string>{'Area of Focus'}, 
		'Past DPE &amp; Over Capacity Bednight' => new set<string>{'Out of County Bednight - Past DPE & Over Capacity', 'NYC Bednight - Past DPE & Over Capacity'}, 
		'Miscellaneous Service Provision' => new set<string>{'Area of Focus'}, 
		'Service Plan Session' => new set<string>{'Housing', 'General Service Plan'}, 
		'Summer Camp Session' => new set<string>{'SBH Summer Camp'}, 
		'Children&apos;s Play Group/Activity' => new set<string>{'MO Kids&apos; Group (Friday Morning)', 'BkFJC Kid&apos;s Group (Parenting-Spanish)', 'MO Kids&apos; Group (Parenting-English)', 'MO Kids&apos; Group (Thursday Late Evening)', 'MO Kids&apos; Group (Wednesday Afternoon)', 'BxFJC Kids&apos; Group (Spanish-Cont. Freedom)', 'MO Kids&apos; Group (Monday Evening)', 'MO Kids&apos; Group (Community Meeting)', 'MO Kids&apos; Group (Drama Therapy)', 'MO Kids&apos; Group (DV 101)', 'BxFJC Kids&apos; Group (Parenting-Spanish)', 'BxFJC Kids&apos; Group (DV 101-Spanish)', 'BxFJC Kids&apos; Group (Financial Education)', 'MO Kids&apos; Group (Monday Afternoon)', 'QFJC Kids&apos; Group (Computer Class)', 'MO Kids&apos; Group (DV 101/Mentors) (Monday)', 'MO Kids&apos; Group (Spanish Reunification)', 'MO Kids&apos; Group (Tuesday Late Evening)', 'MO Kids&apos; Group (Wednesday Morning)', 'QFJC Kid&apos;s Group (SAVI-Monday Evening)', 'MO Kids&apos; Group (Housing/Economic)', 'MO Kids&apos; Group (Mentors/Mentors Training)', 'MO Kids&apos; Group (Wednesday Evening)', 'BxFJC Kids&apos; Group (Parenting-English)', 'MO Kids&apos; Group (Tuesday Early Evening)', 'MO Kids&apos; Group (Thursday Early Evening)', 'QFJC Kid&apos;s Group (Parenting-English)', 'MO Kids&apos; Group (English Reunification)', 'MO Kids&apos; Group (Career Readiness Workshop)', 'BxFJC Kids&apos; Group (DV 101-English)', 'BxFJC Kids&apos; Group (English-Cont. Freedom)', 'MO Kids&apos; Group (Tuesday Early Afternoon)', 'BkFJC Kid&apos;s Group (Parenting-English)'}, 
		'Case/Client Meeting' => new set<string>{'Client Not Participating', 'Client Participating'}, 
		'Coordination Meeting' => new set<string>{'MO Mentors&apos; Meeting-English', 'MO Mentors&apos; Meeting-Spanish'}, 
		'Legal Fee Assistance' => new set<string>{'QDRO', 'Business Appraisal', 'Medical Record or Exam', 'Witness', 'Birth Certificate', 'Translating Services', 'Process Server', 'Duplication', 'Certification of Court Records', 'Interpreter Services', 'Assessment of Marital Business', 'Notary Vertification', 'FOIA', 'Biometrics', 'Fingerprints', 'Filing', 'Appraisal', 'Certificate of Dispostion', 'Passport', 'Purchase of Index Number', 'Passport Photos', 'Application', 'ID', 'Background Check'}, 
		'Health and Recreation Group' => new set<string>{'RP Gina Gibney Dance Group', 'SBH Little Flower Yoga', 'SBH Children’s Game Night', 'SBH Children&apos;s Sports & Fitness Group', 'SBH Nutrition Group', 'RP Tea Time Children&apos;s Group', 'RP Pre-Teen and Teen Night', 'RP Tea Time Women&apos;s Group', 'SBH Children&apos;s Music Workshop', 'SBH Gina Gibney Group', 'RP Children&apos;s Night', 'SBH Fitness Group', 'SBH Gina Gibney Children&apos;s Group'}, 
		'Deposition' => new set<string>{'Prepare', 'Conduct/Defend'}, 'Peer Support Group' => new set<string>{'SBH Sister to Sister Workshop'}, 
		'Mentor Accompaniment' => new set<string>{'Area of Focus'}, 
		'Appearance Preparation' => new set<string>{'Trial', 'Conciliation', 'Mandatory Dispute Resolution', 'Immigration Master Hearing', 'Fair Hearing', 'Immigration Individual Merits Hearing', 'Conference', 'Immigration Interview', 'Non-Trial'}, 
		'Financial Grant (Sunshine Lady)' => new set<string>{'Phone', 'Moving/Storage', 'Security Deposit/Brokerage Fee', 'Food/Personal Care Items', 'Child Care', 'Utilities', 'Clothing', 'Mortgage', 'Education', 'Job Training/Preparation', 'Transportation', 'Children’s Items', 'Medical', 'Rent', 'Furniture/Household Items'}
	};
	
	
	
	
	
	@isTest
	static void testAddServicesController() {
		Event__c ev=new Event__c();
		Case__c cas=new Case__c();
		cas.Matter_Type_Detail__c = 'Child Support';
		Id conRt=[Select id from RecordType where name='Staff' and sObjectType='Contact'].id;
		contact con=new contact(lastname='test', Salesforce_User__c = Userinfo.getUserId());
		con.RecordTypeId=conRt;
        con.FirstName = 'test';
        con.LastName = 'test';
		insert con;
			
		insert ev;
		insert cas;
		ApexPages.currentPage().getParameters().put('eid', ev.id);
		ApexPages.currentPage().getParameters().put('cid', cas.id);
		add_Services_Controller aec = new add_Services_Controller();
		aec.refreshOptions();
		aec.refreshAofOptions();
		aec.refreshsubOptions();
		aec.selectService();
		aec.selectAof();
		aec.selectSubType();
		aec.removeStaffMember();
		aec.addStaffMember();
		aec.addServiceToList(); 
		aec.removeService();
		aec.addStaffMember();
		aec.removeStaffMember();
		aec.finalizeServices();
		aec.cancel();
		aec.selectedService=aec.subtyping;
		aec.refreshOptions();
		aec.selectedService='test';
		aec.refreshOptions();
		
	}
	
}