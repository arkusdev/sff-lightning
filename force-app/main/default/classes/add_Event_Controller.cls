public with sharing class add_Event_Controller {
	public Case__c thisCase {get; set;}
	public Event__c newEvent {get; set;}
	
	public add_Event_Controller(ApexPages.StandardController con) {
		thisCase = (Case__c)con.getRecord();
		newEvent = new Event__c();
		if(thisCase.Id != null) {
			thisCase = [select Id, Name from Case__c where Id = :thisCase.Id];
			newEvent.Case__c = thisCase.Id;
		}
	}
	
	public PageReference cancel() {
		PageReference p = new PageReference('/' + thisCase.Id);
		return p;
	}
	
	public PageReference addServices() {
		PageReference p = null;
		if(newEvent.Case__c != null) {
			insert newEvent;
			p = page.add_Services;
			p.getParameters().put('eid', newEvent.Id);
			p.getParameters().put('cid', thisCase.Id);
			p.setredirect(true);
		}
		else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select Case for new event');
			ApexPages.addMessage(myMsg);
		}
		
		return p;
	}
	
	@isTest
	static void testAddEventController() {
		Case__c test_case = new Case__c();
        test_case.Matter_Type_Detail__c = 'Child Support';
		insert test_case;
		ApexPages.StandardController sc = new ApexPages.StandardController(test_case);
		add_Event_Controller aec = new add_Event_Controller(sc);
		aec.addServices();
		aec.cancel();
		aec.newEvent.Case__c=null;
		aec.addServices();
	}
	
}