public with sharing class CaseC_Methods {
    
    @testVisible
    private static List<Night__c> newNights {get;set;}
    private static String shelterRecordType = [SELECT Id from RecordType where sobjecttype = 'Case__c' AND name = 'Shelter'].id;
    private static Map<id, Shelter__c> shelterMap = new Map<id,Shelter__c>([SELECT Id, max_number_of_nights__c from Shelter__c]);
    
    public static void beforeUpdate(list<Case__c> oldList,list<Case__c> newlist){
        
        Set<id> PrimaryStaffIds = new Set<id>();
        
        List<Case__c> init = new  list<Case__c>();
        for(Integer i=0;i<newlist.size();i++){
            if(newlist[i].primary_staff_on_case__c!=null && oldlist[i].Primary_staff_on_case__c!=newlist[i].primary_staff_on_case__c){
                PrimaryStaffIds.add(newlist[i].Primary_Staff_on_case__c);
            }
            // set Max Date when for the first time is set Open Date / that happen when Open Date and Close Date were null 
            if (newlist[i].recordtypeid == shelterRecordType && newlist[i].Case_open_date__c != null  && oldList[i].Case_open_date__c == null && oldList[i].Early_Close_Date__c == null){
                date maxdate;
                if (newlist[i].shelter__c != null){
                    maxdate = newlist[i].case_open_date__c.adddays(integer.valueof(shelterMap.get(newlist[i].shelter__c).max_number_of_nights__c));
                    newlist[i].Case_Close_Date__c = maxdate;
                }
                
                init.add(newlist[i]);
            }       
        }
        //this function creates new Night__c records
        afterInsert (init);
        
        List<Id> caseIds = new List<Id>();
        List<Id> casePrimaryStaffIds = new List<Id>();
        for (Case__c c : newlist) {
            caseIds.add(c.Id);
            casePrimaryStaffIds.add(c.Primary_Staff_on_Case__c);
        }
        List<Contact> casesPrimaryStaff = [SELECT Law_Firm__c, Law_School__c FROM Contact WHERE Id IN :casePrimaryStaffIds];
        
        for (Case__c c : newlist){
            if (c.Primary_Staff_on_Case__c != null) {
                Contact primaryStaffOnCase = new Contact();
                for (Contact primary : casesPrimaryStaff){
                    if (c.Primary_Staff_on_Case__c == primary.Id){
                        primaryStaffOnCase = primary;
                        break;
                    }
                }
                if(primaryStaffOnCase.Law_Firm__c!=null){
                    c.Probono_Firm__c = primaryStaffOnCase.Law_Firm__c;
                    c.Probono_Firm_Declarer__c = c.Primary_Staff_on_Case__c;
                }else if(primaryStaffOnCase.Law_School__c!=null){
                    c.Probono_Firm__c = primaryStaffOnCase.Law_School__c;
                    c.Probono_Firm_Declarer__c = c.Primary_Staff_on_Case__c;
                }
            } 
        }
    }
    
    public static void beforeInsert (list<Case__c> newlist){
        List<Contact> contactStaffList = [Select Id From Contact Where Salesforce_User__c = :UserInfo.getUserId()];
        list<case__c> legalFamilyLawCases=new list<case__c>();
        list<RecordType> rts=[select id, name from recordtype where name='Legal - Family Law' and sobjectType='Case__c'];
        String legalRecordType;
        if(rts.size()>0){
            legalRecordType=rts.get(0).id;
        }
        Set<id> shelterids = new Set<id>();
        Set<id> PrimaryStaffIds = new Set<id>();
        
        for (Case__c c: newlist){
            if(c.recordtypeid==legalRecordType){
                legalFamilyLawCases.add(c); 
            }
            shelterids.add(c.shelter__c);
            if(c.Primary_Staff_on_Case__c!=null){
                PrimaryStaffIds.add(c.Primary_staff_on_case__c);
            }
            else {
                if(contactStaffList.size() == 1) {
                    c.Primary_Staff_on_Case__c = contactStaffList[0].Id;
                    PrimaryStaffIds.add(c.Primary_staff_on_case__c);
                }
            }
        }
        updateMatrimonialCaseInformation(legalFamilyLawCases); 
        
        Map<id,Contact> primaryStaffInfo= new Map<id,Contact>([select id,Project__c,Project_Location__c, Law_Firm__c, Law_School__c from contact where id IN :PrimaryStaffIds]);
        
        for (Case__c c : newlist){
            if(c.Primary_Staff_on_Case__c!=null && primaryStaffInfo.containsKey(c.Primary_Staff_on_case__c)){
                Contact staffOnCase = primaryStaffInfo.get(c.Primary_Staff_on_case__c);
                c.location__c=staffOnCase.Project_Location__c;
                c.Project__c=staffOnCase.Project__c;
                if(staffOnCase.Law_Firm__c!=null){
                    c.Probono_Firm__c = staffOnCase.Law_Firm__c;
                    c.Probono_Firm_Declarer__c = staffOnCase.Id;
                }else if(staffOnCase.Law_School__c!=null){
                    c.Probono_Firm__c = staffOnCase.Law_School__c;
                    c.Probono_Firm_Declarer__c = staffOnCase.Id;
                }
            }
            
            if ((c.recordtypeid == shelterRecordType )&& (c.case_open_date__c != null) && (c.case_close_date__c == null)){
                if(c.shelter__c != null){
                    date maxdate = c.case_open_date__c.adddays(integer.valueof(shelterMap.get(c.shelter__c).max_number_of_nights__c));
                    c.Case_Close_Date__c = maxdate;
                    //c.Early_Close_Date__c = maxdate;
                }
            }
        }
    }
    
    public static void updateMatrimonialCaseInformation(list<case__c> legalCases){
        for(case__c legalcase:legalCases){
            if(legalCase.Case_Open_Date__c!=null)
                legalCase.Date_of_Assignment__c=legalCase.Case_Open_Date__c;            
            
            if(legalCase.Date_index_number_Purchased__c!=null)
                legalCase.Deadline_to_Serve_Defendant__c=legalCase.Date_index_number_Purchased__c.addDays(120);
            
            if(legalCase.Date_Defendant_Served__c!=null)
                legalCase.Date_of_Default__c=legalCase.Date_Defendant_Served__c.addDays(20);
            
            if(legalCase.Date_of_Default__c!=null)
                legalCase.Marked_Off_Calendar__c=legalCase.Date_of_Default__c.addYears(1);
        }       
    }
    
    public static void afterInsert (list<Case__c>  newlist) {
        List<Additional_Staff__c> addStaff = new List<Additional_Staff__c>();
        newNights = new list<Night__c>();
        for (Case__c c : newlist) {
            if (c.Case_Open_Date__c != null && c.Shelter__c != null){ 
                if (c.Early_Close_Date__c != null){
                    createNights(c, c.Case_Open_Date__c, c.Early_Close_Date__c);
                }
                else if (c.Case_Close_Date__c != null){
                    createNights(c, c.Case_Open_Date__c, c.Case_Close_Date__c);
                }
            }
            //if there is no additional staff for this contact, create it
            if(c.Primary_Staff_on_Case__c != null){
                Additional_Staff__c newStaff = new Additional_Staff__c(Contact__c = c.Primary_Staff_on_Case__c, Case__c = c.Id);
                addStaff.add(newStaff);
            }
        }
        if (newNights.size() > 0) {
            insert newNights;   
        }
        upsert addStaff;
    }
    
    public static void afterUpdate (list<Case__c> newlist, list<Case__c> oldlist, Map<Id, Case__c> newMap, Map<Id, Case__c> oldMap) {
        Map<Id, List<Date>> idToNightDates = new Map<Id, List<Date>>();
        set<Id> caseSet = new set<Id>();
        Map<id,list<Date>> newDatesList = new Map<id,list<Date>>();
        list<Date>  newDates ;
        newNights = new list<Night__c>();
        for (integer i=0; i<newlist.size(); i++) {
            if ( ((newlist[i].recordtypeid == shelterRecordType)&& (newlist[i].Case_Open_Date__c != null) && (newlist[i].Early_Close_Date__c != null)) && ((newlist[i].Early_Close_Date__c != oldlist[i].Early_Close_Date__c)||(newlist[i].Case_Open_Date__c != oldlist[i].Case_Open_Date__c))) {
                //if (newlist[i].recordtypeid == shelterRecordType && newlist[i].Case_Open_Date__c != null && newlist[i].Early_Close_Date__c != null ||(newlist[i].Early_Close_Date__c != null && newlist[i].Early_Close_Date__c != oldlist[i].Early_Close_Date__c))    
                if(oldlist[i].Case_Open_Date__c == null){
                    createNights(newlist[i], newlist[i].Case_Open_Date__c, newlist[i].Early_Close_Date__c);
                }else{
                    createNightsNew(newlist[i], newlist[i].Case_Open_Date__c,oldlist[i]);
                }
                
                newDates = new list<Date>{newlist[i].Case_Open_Date__c , newlist[i].Early_Close_Date__c};
                    newDatesList.put(newlist[i].Id,newDates);
            }
        }
        if(newDatesList.size()>0){
            for( Night__c var :[select id,Date_Service_Was_Provided__c,case__c from Night__c where case__c IN :newDatesList.keySet() ]){
                if( (var.Date_Service_Was_Provided__c < newDatesList.get(var.case__c).get(0)) || ( var.Date_Service_Was_Provided__c >= newDatesList.get(var.case__c).get(1) ) ){
                    caseSet.add(var.id);
                }
            }
            delete [select id from Night__c where id IN :caseSet ];
        }
        if (newNights.size() > 0) {
            insert newNights;
        }
        
        
        List<Additional_Staff__c> staffToUpdate = new List<Additional_Staff__c>();
        Map<Id, Additional_Staff__c> releatedStaff = new Map<Id, Additional_Staff__c>([SELECT Id, Contact__c, Case__c,Primary_Staff_on_Case__c FROM Additional_Staff__c WHERE Case__c IN : newlist]);
        Map<Id, List<Additional_Staff__c>> caseStaffMap = new Map<Id, List<Additional_Staff__c>>();
        for(Additional_Staff__c aStaff: releatedStaff.values()){
            if(!caseStaffMap.containsKey(aStaff.Case__c)){
                caseStaffMap.put(aStaff.Case__c, new List<Additional_Staff__c>());
            }
            List<Additional_Staff__c> staffs = caseStaffMap.get(astaff.Case__c);
            staffs.add(aStaff);
        }
        
        for(Case__c aCase : newlist){
            boolean isNewStaff = true;            
            if(oldMap.containsKey(aCase.Id) && oldMap.get(aCase.Id).Primary_Staff_on_Case__c != aCase.Primary_Staff_on_Case__c){
                List<Additional_Staff__c> aCaseStaffs = caseStaffMap.get(aCase.Id);
                if(aCaseStaffs != null){
                    for(Additional_Staff__c caseStaff: aCaseStaffs){
                        if(caseStaff.Contact__c == aCase.Primary_Staff_on_Case__c){
                            isNewStaff = false;
                        }
                        if(caseStaff.Contact__c != aCase.Primary_Staff_on_Case__c){
                            if(caseStaff.Primary_Staff_on_Case__c == true){
                                caseStaff.Primary_Staff_on_Case__c = false;
                                staffToUpdate.add(caseStaff);
                            }                  
                        }else{
                            caseStaff.Primary_Staff_on_Case__c = true;
                            staffToUpdate.add(caseStaff);
                        }  
                    }
                }
                if(isNewStaff && aCase.Primary_Staff_on_Case__c != null){
                    Additional_Staff__c newStaff = new Additional_Staff__c(Contact__c = aCase.Primary_Staff_on_Case__c, Case__c = aCase.Id);
                    staffToUpdate.add(newStaff);
                }
            }
        }
        upsert staffToUpdate;
    }
    
    public static void beforeDelete (map<id,Case__c> oldmap) {
        list<Night__c> nightDeletes = [select id from Night__c where case__c IN :oldmap.keyset()];
        if (nightDeletes.size() > 0) delete nightDeletes;
    }
    
    public static void createNights(Case__c cas, Date start, Date end_date) {
        if(start <> null){
            for (integer i=0; true; i++) {
                if (start.adddays(i) >= end_date) {
                    break;
                }
                else {
                    Night__c n = new Night__c();
                    n.date_service_was_provided__c = start.adddays(i);
                    n.case__c = cas.id;
                    newNights.add(n);
                }
            }
        }
    }
    public static void createNightsNew(Case__c cas, Date start,Case__c casOld) {
        if(start <> null){
            
            Date old_close = casOld.Early_Close_Date__c != null ? casOld.Early_Close_Date__c: casOld.Case_Close_Date__c;
            Date new_close = cas.Early_Close_Date__c != null ? cas.Early_Close_Date__c: cas.Case_Close_Date__c;
            
            for (integer i=0; true; i++) {
                if (start.adddays(i) >= new_close) {
                    break;
                }
                else {
                    if (!(new_close != null && start.adddays(i) >= casOld.Case_Open_Date__c && start.adddays(i) < old_close) &&
                        !(old_close == null && start.adddays(i) >= casOld.Case_Open_Date__c && start.adddays(i) < old_close)){
                            Night__c n = new Night__c();
                            n.date_service_was_provided__c = start.adddays(i);
                            n.case__c = cas.id;
                            newNights.add(n);
                        }
                }
            }
        }
    }
    
    public static void updateContactsWithFirmOrSchoolQuantOnCase(List<Case__c> cases){        
        List<Id> caseIds = new List<Id>();
        List<Id> casePrimaryStaffIds = new List<Id>();
        for (Case__c c : cases) {
            caseIds.add(c.Id);
            casePrimaryStaffIds.add(c.Primary_Staff_on_Case__c);
        }
        List<Additional_Staff__c> casesAddStaff = [SELECT Contact__c, Contact__r.Law_Firm__c, Contact__r.Law_School__c, Case__c, Primary_Staff_on_Case__c FROM Additional_Staff__c WHERE Case__c IN :caseIds];
        List<Contact> casesPrimaryStaff = [SELECT Law_Firm__c, Law_School__c FROM Contact WHERE Id IN :casePrimaryStaffIds];
        
        for (Case__c myCase : cases){
            Integer qty = 0;
            
            for (Contact primary : casesPrimaryStaff){
                if (myCase.Primary_Staff_on_Case__c == primary.Id && (primary.Law_Firm__c != null || primary.Law_School__c != null)){
                    qty++;
                    break;
                }                
            }
            for (Additional_Staff__c staff : casesAddStaff){
                if (staff.Case__c == myCase.Id){
                    if (!staff.Primary_Staff_on_Case__c && (staff.Contact__r.Law_Firm__c != null || staff.Contact__r.Law_School__c != null)){
                        qty++;
                    }
                }
            }
            myCase.Quant_Contacts_Law_Firm_School__c = qty;  		
        }	
    }
}