public with sharing class serviceNeedsOutcomeAdd_Extension {

	public ServiceNeedsOutcome__c newsn {get;set;}
	public list<ServiceNeedsOutcome__c> serviceNeeds {get;set;}
	private string caseid;

	public serviceNeedsOutcomeAdd_Extension(Apexpages.standardcontroller con){
		caseid = con.getid();
		serviceNeeds = new list<ServiceNeedsOutcome__c>();
		newSn = new ServiceNeedsOutcome__c(Service_Need_Identify_Date__c = system.today());
		
	}
	
	public pagereference addSn(){
		newsn.Case__c = caseid;
		serviceNeeds.add(newsn);
		newSn = new ServiceNeedsOutcome__c(Service_Need_Identify_Date__c=newsn.Service_Need_Identify_Date__c);
		return null;
	}
	
	public pagereference saveDone(){
		newsn.Case__c = caseid;
		serviceNeeds.add(newsn);
		insert serviceNeeds;
		return new pagereference('/'+caseid);
	}
}