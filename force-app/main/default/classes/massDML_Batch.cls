/*
	To Run:
			massDML_Batch job = new massDML_Batch();
			Id processId = Database.executeBatch(job);
*/
global class massDML_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts{

	global final String queryString;

	global massDML_Batch(string qstring){
		
		if ( queryString == null ){
			queryString = qstring;
			system.debug(queryString);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext bc){
		system.debug(queryString);
		return Database.getQueryLocator(queryString);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		delete scope;
	}

	global static void startBatch(string qstring, string dml){
		massDML_Batch job = new massDML_Batch(qstring);
		Database.executeBatch(job);
	}

	global static void startBatch(string qstring,integer batchSize){
		massDML_Batch job = new massDML_Batch(qstring);
		Database.executeBatch(job,batchSize);
	}

	global void finish(Database.BatchableContext BC){
	}
}