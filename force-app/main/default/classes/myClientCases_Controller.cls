public with sharing class myClientCases_Controller {
	static Map<String, Schema.SObjectField> CASE_FIELDMAP = Schema.SObjectType.Case__c.fields.getMap();
	//static Map<String, Schema.SObjectField> CONTACT_FIELDMAP = Schema.SObjectType.Contact.fields.getMap();
	static Set<String> CONTACT_FIELDS = new set<String>{'Id','name','firstname','lastname','other_name__c','birthdate','Head_of_Household__c','RecordTypeId', 'Contact_ID__c'};
	static Set<String> excludeFields = new Set<String>{''};
	//public List<Contact> contactList {get;set;}
	//public transient List<ContactCaseWrapper> contactList {get;set;}
	//test Deployment
	//public List<Case__c> caseList {get;set;}
	public Id selectedContactId {get;set;}
	public String selectedContactName{get;set;}
	public ApexPages.StandardSetController caseSetController{get;set;}
	public ApexPages.StandardSetController contactSetController{get;set;}
	public String StaffType{get;set;}
	public myClientCases_Controller(){
		StaffType='Primary Staff';
		queryContactandCases();
	}

	public void queryCases(){
		
        String queryString=getQueryString();
		caseSetController=new Apexpages.StandardSetController(database.getQueryLocator(queryString));
		caseSetController.setPageSize(50);
		if(this.selectedContactId!=null){
			if(caseSetController.getRecords().size()>0)
				this.selectedContactName=getCurrentCases().get(0).contact__r.name;
		}

		
	}

	
	public void queryContactAndCases(){ 
		queryCases();
		Map<Id,Contact> contactMap = new Map<Id,Contact>();		
		for ( Case__c c : database.query(getQueryString()) ){			
           if ( c.contact__r == null ) system.debug('null contact on case '+c.id);
            else contactMap.put(c.Contact__c,c.Contact__r);

		}
		List<Contact> contactList = contactMap.values();
		contactList.sort();
		contactSetController=new ApexPages.StandardSetController(contactList);
		contactSetController.setPageSize(20);
	}

	public String getQueryString(){
		//If Additional Staff
		String addStaffString;
		if(staffType=='Additional Staff'){
			//list<Additional_Staff__c> astaffs=[select id, case__c from Additional_Staff__c where Primary_Staff_On_Case__c=false and contact__r.Salesforce_User__c=:userInfo.getUserId()];
			//if(astaffs.size()>0){
			//	set<id> addCaseIds=new set<id>();
			//	for(Additional_Staff__c astaff:astaffs){
			//		addCaseIds.add(astaff.case__c);
			//	}
			//	list<id> caseIdList=new list<id>();
			//	caseIdList.addAll(addCaseIds);
			//	addStaffString='(\''+String.join(caseIdList,'\',\'') +'\')';
			//}
			addStaffString=getAdditionalStaffSetString();
		}
		String queryString = 'Select ';
		
		for ( String f : CASE_FIELDMAP.keySet() ){
			if ( !excludeFields.contains(f) ){
				queryString += f+', ';
			}
		}
		/*for ( String f : CONTACT_FIELDMAP.keySet() ){
			if ( !excludeFields.contains(f) ){
				queryString += 'Contact__r.'+f+', ';
			}
		}*/
		for ( String f : CONTACT_FIELDS ){
			if ( !excludeFields.contains(f) ){
				queryString += 'Contact__r.'+f+', ';
			}
		}
		queryString=queryString.removeEnd(', ');
		queryString += ' from Case__c ';
		queryString += ' where Statusformula__c != \'Closed\'';
		if(staffType=='Additional Staff' && addStaffString!=null)		
			queryString+=' and id in '+addStaffString;
		else
			queryString+=' and Primary_Staff_On_Case__r.Salesforce_User__c = \''+userInfo.getUserId()+'\' ';
	
		if ( selectedContactId != null ){
			queryString += ' and Contact__c = :selectedContactId ';
		}
		queryString += ' order by Case_Open_Date__c asc';
		//caseList = new List<Case__c>();
		System.debug(queryString);
		return queryString;

	}
	public string getAdditionalStaffSetString(){
		String addStaffString;
		list<Additional_Staff__c> astaffs=[select id, case__c from Additional_Staff__c where Primary_Staff_On_Case__c=false and contact__r.Salesforce_User__c=:userInfo.getUserId()];
			if(astaffs.size()>0){
				set<id> addCaseIds=new set<id>();
				for(Additional_Staff__c astaff:astaffs){
					addCaseIds.add(astaff.case__c);
				}
				//list<id> caseIdList=new list<id>();
				//caseIdList.addAll(addCaseIds);
				addStaffString=getSOQLSetString(addCaseIds);//'(\''+String.join(caseIdList,'\',\'') +'\')';
			}
		return addStaffString;
	}
	public string getSOQLSetString(set<id> setVal){
		list<id> listVal=new list<id>();
		listVal.addAll(setVal);
		String SOQLSetString='(\''+String.join(listVal,'\',\'') +'\')';
		return SOQLSetString;
	}
	public list<case__c> getcurrentCases(){
		return (list<Case__c>)caseSetController.getRecords();  

	}
	public list<contactCaseWrapper> getcurrentContacts(){
		list<contact> cons=(list<Contact>)contactSetController.getRecords();  
		list<contactCaseWrapper> cwlist=new list<contactCaseWrapper>();
		//calculate number of cases per contact here.. can't set in setcontroller.
		map<id,integer> contactcasemap=new map<id,integer>();
		for(contact c:cons){
			contactcasemap.put(c.id,0);
		}
		if(staffType!='Additional Staff'){
			for(case__c cas:[select id,contact__c from case__c where Statusformula__c !='Closed' and Primary_Staff_On_Case__r.Salesforce_User__c = :userInfo.getUserId() and contact__c in:contactcaseMap.keyset()]){
				integer caseCount=contactCasemap.get(cas.contact__c);
				contactCaseMap.put(cas.contact__c,++caseCount);
			}
	    }
	    else{
	    	//string addStaffQuery='select id, contact__c from case__c where Statusformula__c !=\'Closed\' and Primary_Staff_On_Case__r.Salesforce_User__c = '\''+userInfo.getUserId()+'\' and id in '+getAdditionalStaffSetString+' and contact__c in:contactcaseMap.keyset()';
			string addStaffQuery='select id, contact__c from case__c where Statusformula__c !=\'Closed\' and id in '+getAdditionalStaffSetString()+' and contact__c in '+getSOQLSetString(contactcaseMap.keyset());
	    	system.debug(addStaffQuery);
	    	for(case__c cas:database.query(addStaffQuery)){
	    		integer caseCount=contactCasemap.get(cas.contact__c);
				contactCaseMap.put(cas.contact__c,++caseCount);
	    	}

	    }
		for(contact c:cons){
			cwlist.add(new contactCaseWrapper(c,contactCaseMap.get(c.id)));
		}
		return cwlist;
	    	
	}
	public pageReference nextcase(){
		caseSetController.next();
		return null;
	}
	public pageReference previouscase(){
		caseSetController.previous();
		return null;
	}
	public boolean gethasNextcase(){
		return caseSetController.getHasNext();
	}
	public boolean gethasPreviouscase(){
		return caseSetController.getHasPrevious();
	}

	public pageReference nextcontact(){
		contactSetController.next();
		return null;
	}
	public pageReference previouscontact(){
		contactSetController.previous();
		return null;
	}
	public boolean gethasNextcontact(){
		return contactSetController.getHasNext();
	}
	public boolean gethasPreviouscontact(){

		return contactSetController.getHasPrevious();
	}
	public String getCaseResultString(){		
		return getResultString(caseSetController);			

	}
	public String getContactResultString(){
		return getResultString(contactSetController);
	}

	public String getResultString(ApexPAges.StandardSetController sCon){
		// 0 - 50 of 100 results
		integer totalResults=sCon.getResultSize();
		integer ofResults=sCon.getPageNumber()*sCon.getRecords().size(); 
		integer currentResult=(sCon.getPageNumber()*sCon.getpagesize() )-sCon.getpageSize()+1;
		String currentResultString=currentResult+' - '+ofResults+ ' of '+ totalResults;
		return currentResultString;
	}

	public void clearSelection(){
		selectedContactId = null;
		queryContactandCases();
	}
	public class contactCaseWrapper implements comparable{
		public Contact con{get;set;}
		public integer numCases{get;set;}
		public contactCaseWrapper(contact con){
			this.con=con;
			numcases=0;
		}	
		public contactCaseWrapper(contact con,integer numcases){
			this.con=con;
			this.numcases=numcases;
		}	
		public integer compareTo(object comp){
			ContactCaseWrapper compare= (ContactCaseWrapper)comp;
			return this.con.name.compareTo(compare.con.name);
		}	
	}
	public list<selectOption> getStaffOptions(){
		list<selectOption> options=new list<SelectOption>();
		options.add(new selectOption('Primary Staff','Primary Staff'));
		options.add(new selectOption('Additional Staff','Additional Staff'));
		return options;
	}

}