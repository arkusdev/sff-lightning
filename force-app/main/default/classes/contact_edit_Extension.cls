public with sharing class contact_edit_Extension {
	
	public Contact con {get;set;}
	public string recordTypeName {get;set;}
	
	public list<Contact> perfectmatchlist {get;set;}
	public list<Contact> partialmatchlist {get;set;}
	
	public boolean displayresults {get;set;}
	public boolean perfectmatchfound {get;set;}
	public boolean partialmatchfound {get;set;}
	
	public boolean continueanyway {get;set;}
	public boolean showsaveerror {get;set;}
	public boolean newContact {get;set;}
	
	public contact_edit_Extension(apexpages.standardcontroller sc){
		newContact = false;
		continueanyway = false;
		showsaveerror = false;
		displayresults = false; // show dupe results
		partialmatchlist = new list<Contact>();
		perfectmatchlist = new list<Contact>();
		
		con = (Contact)sc.getRecord();	
		
		// if used for editing of existing records	
		if (con.id != null) {
			con = (Contact)global_Utilities.queryAllFields(con.id);
		}
		else {
			newContact = true;
			con.OwnerId = userinfo.getUserId();
		}
		
		if (test.isrunningTest() == false) recordtypename = [select id, Name from RecordType where id = :con.recordtypeid].name;
	}
	
	public pagereference checkForDupes () {
		partialmatchlist = new list<Contact>();
		perfectmatchlist = new list<Contact>();
		
		list<Contact> queryResults = [Select Birth_Country__c, Gender__c, c.id, c.LastName, c.FirstName, c.Birthdate From Contact c where (c.firstname = :con.firstname OR c.lastname = :con.lastname OR c.birthdate = :con.birthdate) AND id != :con.id order by c.lastname,c.firstname];
		
		integer matchcount;
		for (Contact c : queryResults) {
			matchcount=0;
			if (c.firstname == con.firstname) {
				matchcount++;
			}
			if (c.lastname == con.lastname) {
				matchcount++;
			}
			if (c.birthdate != null && c.birthdate == con.birthdate) {
				matchcount++;
			}
			
			if (matchcount == 2) {
				partialmatchlist.add(c);
			}
			
			if (matchcount == 3) {
				perfectmatchlist.add(c);
			}
		}
		
		partialmatchfound = false;
		perfectmatchfound = false;
		if (partialmatchlist.size() > 0) {
			partialmatchfound = true;
		}
		if (perfectmatchlist.size() > 0) {
			perfectmatchfound = true;
		}
		
		displayresults = true; // show dupe results
		
		return null;
	}
	
	public pagereference savecontact() {
		if (!newContact && (partialmatchlist.isEmpty() || perfectmatchlist.isEmpty())) {
			checkfordupes();
		}
		
		if (continueanyway==false) {
		 	if(!newContact || (perfectmatchlist.isEmpty() && partialmatchlist.isEmpty())) {
				showsaveerror=false;
				checkBlankFields();
				if (blankerror == false) {
					upsert con;
					Pagereference p = new pagereference('/'+con.id);
					return p;
				}
		 	}	
		 	else {
		 		showsaveerror = true;
				return null;
		 	}
		}
		else {
			showsaveerror=false;
			checkBlankFields();
			if (blankerror == false) {
				upsert con;
				Pagereference p = new pagereference('/'+con.id);
				return p;
			}
		}
		return null;
	}
	
	public string blankerrorstring {get;set;}
	public boolean blankerror {get;set;}
	
	public pagereference checkBlankFields () {
		blankerrorstring ='';
		if (con.FirstName == null || con.firstname.trim().length() == 0) {
			blankerrorstring = 'First name must be filled out\n';
		}
		if (con.lastname == null || con.lastname.trim().length() == 0) {
			blankerrorstring = 'Last name must be filled out\n';
		}
		//if (con.birthdate == null) {
		//	blankerrorstring = 'Birthdate name must be filled out';
		//}
		
		if (blankerrorstring.trim().length() > 0) {
			blankerror = true;
		}
		else {
			blankerror = false;
			blankerrorstring = '';
		}
		return null;
	}
}