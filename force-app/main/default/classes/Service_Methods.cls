global with sharing class Service_Methods {
	
	public static void afterInsert(list<Service__c> newlist){
		//createServiceAssignmentJunction(newlist);
	}
	
	public static void afterUpdate(list<Service__c> newlist, map<id,Service__c> oldmap){
		map <id, date> fixSessionDateMap = new map<id, date>();
		for (Service__c s : newlist){
			if (s.service_date__c != oldmap.get(s.id).service_date__c){
				fixSEssionDateMap.put(s.id,s.service_date__c);
			}
		}
		
		list<Session__c> sessionUpdates = new list<Session__c>();
		for (Session__c s : [select id, session_date__c, service__c from Session__c where Service__c IN : fixSessionDateMap.keyset()]){
			s.Session_Date__c = fixSessionDateMap.get(s.service__c);
			sessionUpdates.add(s);
		}
		
		if (sessionUpdates.size()>0) update sessionUpdates;
		
		list<Service__c> staffupdated = new list<service__c>();
		for (integer i=0; i<newlist.size(); i++){
			if (newlist[i].service_Date__c != oldmap.get(newlist[i].id).service_date__c) staffupdated.add(newlist[i]); 
			else if (newlist[i].Staff_Member__c != oldmap.get(newlist[i].id).staff_member__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member2__c != oldmap.get(newlist[i].id).staff_member2__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member3__c != oldmap.get(newlist[i].id).staff_member3__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member4__c != oldmap.get(newlist[i].id).staff_member4__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member5__c != oldmap.get(newlist[i].id).staff_member5__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member6__c != oldmap.get(newlist[i].id).staff_member6__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member7__c != oldmap.get(newlist[i].id).staff_member7__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member8__c != oldmap.get(newlist[i].id).staff_member8__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member9__c != oldmap.get(newlist[i].id).staff_member9__c) staffupdated.add(newlist[i]);
			else if (newlist[i].Staff_Member10__c != oldmap.get(newlist[i].id).staff_member10__c) staffupdated.add(newlist[i]);
		}
		system.debug('staff updated size '+staffupdated.size());
		//if (staffupdated.size()>0) createServiceAssignmentJunction(newlist);
	}
	
	public static void beforeDelete (Map<Id, Service__c> oldMap) {
		List<Session__c> sessionsToDelete = [SELECT Id FROM Session__c WHERE Service__c IN :oldMap.keySet()];
		delete sessionsToDelete;
	}
	
	webservice static string cloneWithAttendance(string oldid){
		string newid;
		
		Service__c oldservice = ((list<Service__c>)queryAllFields('service__c','id = \''+oldid+'\''))[0]; 
		list<Session__c> sessions = (list<Session__c>)queryAllFields('session__c', 'service__c = \''+oldid+'\'');
		Service__c newService;
		savepoint save = database.setSavepoint();
		try{
			newService = (Service__c)cloneObject(oldService);
			newService.Notes__c ='';
			newService.Service_Date__c = system.today();
			insert newService;
			list<Session__c> sessionlist = new list<session__c>();
			for (Session__c s : sessions){
				Session__c newSession = (Session__c)cloneObject(s);
				newSession.service__c = newservice.id;
				newSession.Attendance_Status__c = null;
				newSession.Session_Date__c = system.today();
				sessionlist.add(newsession);
			}
			if (sessionlist.size()>0) insert sessionlist;
		}
		catch(Exception e){
			database.rollback(save);
		}
		return (newservice!=null)?newservice.id:'error';		
	}
	
	public static sObject cloneObject(sObject input){
		sObject output = input.getsobjecttype().newSObject();
		map <String, Schema.SObjectField> fieldMap = schema.getGlobalDescribe().get(input.getsobjecttype().getDescribe().getname().tolowercase()).getDescribe().fields.getMap();
		for (string field : fieldMap.keySet()) {
			Schema.DescribeFieldResult fieldDescribe = fieldmap.get(field).getDescribe();
			if (fieldDescribe.isUpdateable() && fieldDescribe.isAutoNumber() == false && fieldDescribe.isCalculated() == false) {
				system.debug(field+input.get(field));
				output.put(field,input.get(field));
			}
		}
		return output;
	}
	
	public static list<sObject> queryAllFields (string objectName, string filtersLimits) {
		string queryString = 'Select ';
		for (string s : schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap().keySet()){
			queryString += s+', ';
		}
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		queryString+= ' FROM '+objectname;
		if (filtersLimits != null) queryString+= ' where '+filtersLimits;
		
		return database.query(queryString);
	}
	/*
	public static void createServiceAssignmentJunction(list<Service__c> inputlist){
		date mindate = system.today().addyears(100);
		date maxdate = system.today().addyears(-100);
		set<id> staffmemberids = new set<id>();
		set<id> junctionclearids = new set<id>();
		
		for (Service__c s : inputlist){
			if (s.Service_Date__c < mindate) mindate = s.service_Date__c;
			if (s.service_Date__c > maxdate) maxdate = s.service_date__c;
			system.debug('min date: '+mindate+' max date '+maxdate);
			if (!util.isnullorblank(s.staff_member__c)) staffmemberids.add(s.staff_member__c);
			if (!util.isnullorblank(s.staff_member2__c)) staffmemberids.add(s.staff_member2__c);
			if (!util.isnullorblank(s.staff_member3__c)) staffmemberids.add(s.staff_member3__c);
			if (!util.isnullorblank(s.staff_member4__c)) staffmemberids.add(s.staff_member4__c);
			if (!util.isnullorblank(s.staff_member5__c)) staffmemberids.add(s.staff_member5__c);
			if (!util.isnullorblank(s.staff_member6__c)) staffmemberids.add(s.staff_member6__c);
			if (!util.isnullorblank(s.staff_member7__c)) staffmemberids.add(s.staff_member7__c);
			if (!util.isnullorblank(s.staff_member8__c)) staffmemberids.add(s.staff_member8__c);
			if (!util.isnullorblank(s.staff_member9__c)) staffmemberids.add(s.staff_member9__c);
			if (!util.isnullorblank(s.staff_member10__c)) staffmemberids.add(s.staff_member10__c);
			junctionClearIds.add(s.id);
		}
		
		delete [select id from SCA_Service_Junction__c where service__c IN :junctionclearids];
		
		system.debug('staffmembers '+staffmemberids.size());
		
		map<id, list<Staff_Category_Assignment__c>> staffAssignments = new map<id, list<staff_category_Assignment__c>>();
		
		for (Staff_Category_Assignment__c sca : [select id, contact__c, Date_Assignment_Started__c, Date_Assignment_Ended__c from Staff_Category_Assignment__c where contact__c IN :staffmemberids AND (Date_Assignment_Started__c <= :mindate AND (Date_Assignment_Ended__c >= :maxdate OR date_assignment_ended__c = null))]){
			list<Staff_Category_Assignment__c> temp = new list<Staff_Category_Assignment__c>();
			if (staffAssignments.containskey(sca.contact__c)) temp = staffAssignments.get(sca.contact__C);
			temp.add(sca);
			staffAssignments.put(sca.contact__c, temp);
		}
		
		list<SCA_Service_Junction__c> junctions = new list<SCA_Service_Junction__c>();
		
		for (Service__c s : inputlist){
			list<Staff_Category_Assignment__c> thisServiceAssignments = new list<Staff_Category_Assignment__c>();
			if (!util.isnullorblank(s.staff_member__c) && staffassignments.containskey(s.staff_member__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member__c));
			if (!util.isnullorblank(s.staff_member2__c) && staffassignments.containskey(s.staff_member2__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member2__c));
			if (!util.isnullorblank(s.staff_member3__c) && staffassignments.containskey(s.staff_member3__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member3__c));
			if (!util.isnullorblank(s.staff_member4__c) && staffassignments.containskey(s.staff_member4__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member4__c));
			if (!util.isnullorblank(s.staff_member5__c) && staffassignments.containskey(s.staff_member5__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member5__c));
			if (!util.isnullorblank(s.staff_member6__c) && staffassignments.containskey(s.staff_member6__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member6__c));
			if (!util.isnullorblank(s.staff_member7__c) && staffassignments.containskey(s.staff_member7__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member7__c));
			if (!util.isnullorblank(s.staff_member8__c) && staffassignments.containskey(s.staff_member8__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member8__c));
			if (!util.isnullorblank(s.staff_member9__c) && staffassignments.containskey(s.staff_member9__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member9__c));
			if (!util.isnullorblank(s.staff_member10__c) && staffassignments.containskey(s.staff_member10__c)) thisServiceAssignments.addall(staffassignments.get(s.staff_member10__c));
			
			for (Staff_Category_Assignment__c sca : thisServiceAssignments){
				if (sca.Date_Assignment_Started__c <= s.service_date__c && (sca.Date_Assignment_Ended__c >= s.service_date__c || sca.date_assignment_ended__c == null)) junctions.add(new SCA_Service_Junction__c(service__C = s.id, staff_Category_Assignment__c = sca.id));
			}
		}
		
		system.debug('created '+junctions.size()+' junctions');
		
		if (junctions.size()>0) insert junctions;
		
	}
	*/
	@isTest
	static void Service_Methods_Test(){
		Service__c serv=new Service__c();
		insert serv;
		delete serv;
	}
}