global with sharing  class SFF_wrapperService implements Comparable{

    public SFF_detailCase serviceItem{get;set;}
    public boolean wrap{get;set;}
    public boolean HaveImmigrationEconomic{get;set;} 
    public string ImmigrationEconomic{get;set;} 
    public string ValueImmigrationEconomic{get;set;}
    public string TypeOfAssistance{get;set;}
    public string TypeOfMatter{get;set;}
    public string orderby{get;set;}
    public string StaffMemberName{get;set;}//
    public SFF_wrapperService(boolean boo,String AttendanceStatusC,String nameSC,String OwnerServiceC, id idCaseC, id idServiceC,String serviceC,String typeServiceC,String nameC,date serviceDateC,String subtypeC,String areaFocusC,decimal horsServiceC,decimal dollarAmountOfServiceProvidedC,String modeC,String staffMemberC){
         orderby = 'c1';
         wrap = boo;
         serviceItem = new SFF_detailCase(idCaseC,AttendanceStatusC,nameSC,OwnerServiceC,  idServiceC, serviceC, typeServiceC, nameC, serviceDateC, subtypeC, areaFocusC, horsServiceC, dollarAmountOfServiceProvidedC, modeC, staffMemberC);
    }
    global Integer compareTo(Object compareTo) {
        SFF_wrapperService compareToServ = (SFF_wrapperService)compareTo;
        Integer returnValue = 0;
        if(this.orderby.equals('c1')){
        	if (this.serviceItem.serviceDate > compareToServ.serviceItem.serviceDate) {
            	returnValue = 1;
	        } else if (this.serviceItem.serviceDate < compareToServ.serviceItem.serviceDate) {
	            returnValue = -1;
	        }
        }else{
            if (serviceItem.nameCinteger > compareToServ.serviceItem.nameCinteger) {
	            returnValue = 1;
	        } else if (serviceItem.nameCinteger < compareToServ.serviceItem.nameCinteger) {
	            returnValue = -1;
	        }
        }
        return returnValue; 
    }
}