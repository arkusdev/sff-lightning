/**
 * util
 * @author Michael Wicherski
 * @Version 2013
 * @Description A collection of commonly used methods
 * @Copyright Public use permitted as long as original author is credited and this copyright block remains intact and unmodified.
 */ 

public without sharing class util { 

	/**
	 * isNullOrBlank
	 * @description
	 * @param integer, decimal, string, date, datetime, time, blob, double, id, long
	 * @return boolean true if input null or blank
	 */
	public static boolean isNullOrBlank (integer input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (decimal input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (string input)	{return (input==null || input.trim().length()==0)?true:false;}
	public static boolean isNullOrBlank (date input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (datetime input){return (input==null)?true:false;}
	public static boolean isNullOrBlank (time input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (blob input)	{return (input==null || input.size()==0)?true:false;}
	public static boolean isNullOrBlank (double input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (id input)		{return (input==null)?true:false;}
	public static boolean isNullOrBlank (long input)	{return (input==null)?true:false;}
	public static boolean isNullOrBlank (boolean input)	{return (input==null)?true:false;}
	
	/**
	 * fixNull
	 * @description
	 * @param integer, decimal, string, date, datetime, time, blob, double, id, long
	 * @return integer, decimal, double, long inputs return 0
	 * @return string input returns '' (empty string) 
	 * @return date returns system.today()
	 * @return datetime returns system.now()
	 * @return id inputs returns null
	 * @return time input returns 0:00:00.0
	 * @return blob input returns blob.valueof('') (empty string)
	 */
	public static integer fixNull (integer input) 	{return (isNullOrBlank(input))?0:input;}
	public static decimal fixNull (decimal input) 	{return (isNullOrBlank(input))?0:input;}
	public static string fixNull (string input) 	{return (isNullOrBlank(input))?'':input;}
	public static date fixNull (date input) 		{return (isNullOrBlank(input))?system.today():input;}	
	public static datetime fixNull (datetime input) {return (isNullOrBlank(input))?system.now():input;}
	public static time fixNull (time input) 		{return (isNullOrBlank(input))?time.newinstance(0,0,0,0):input;}
	public static blob fixNull (blob input) 		{return (isNullOrBlank(input))?blob.valueof(''):input;}
	public static double fixNull (double input) 	{return (isNullOrBlank(input))?0:input;}
	public static id fixNull (id input) 			{return (isNullOrBlank(input))?null:input;} //can't fix a null id
	public static long fixNull (long input) 		{return (isNullOrBlank(input))?0:input;}
	public static boolean fixNull (boolean input) 	{return (isNullOrBlank(input))?false:input;}
	
	/**
	 * validateEmail
	 * @description
	 * @param string
	 * @return true if valid email, false if not
	 */
	public static Boolean validateEmail(string email) {
		if ( !isNullOrBlank(email) ) {
			String pat = '[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+';
			Boolean test = Pattern.matches(pat, email);
			return test;
		} else {
			return false;
		}
	}
	
	/**
	 * getUSStateListOptions
	 * @description
	 * @return list<SelectOption> with state abbreviations (USA)
	 */
	public static list<selectoption> getUSStateListOptions(){
		list<SelectOption> statelist = new list<Selectoption>();
		statelist.add(new selectoption('','--'));
		statelist.add(new selectoption('AL','AL'));
		statelist.add(new selectoption('AK','AK'));
		statelist.add(new selectoption('AZ','AZ'));
		statelist.add(new selectoption('AR','AR'));
		statelist.add(new selectoption('CA','CA'));
		statelist.add(new selectoption('CO','CO'));
		statelist.add(new selectoption('CT','CT'));
		statelist.add(new selectoption('DE','DE'));
		statelist.add(new selectoption('FL','FL'));
		statelist.add(new selectoption('GA','GA'));
		statelist.add(new selectoption('HI','HI'));
		statelist.add(new selectoption('ID','ID'));
		statelist.add(new selectoption('IL','IL'));
		statelist.add(new selectoption('IN','IN'));
		statelist.add(new selectoption('IA','IA'));
		statelist.add(new selectoption('KS','KS'));
		statelist.add(new selectoption('KY','KY'));
		statelist.add(new selectoption('LA','LA'));
		statelist.add(new selectoption('ME','ME'));
		statelist.add(new selectoption('MD','MD'));
		statelist.add(new selectoption('MA','MA'));
		statelist.add(new selectoption('MI','MI'));
		statelist.add(new selectoption('MN','MN'));
		statelist.add(new selectoption('MS','MS'));
		statelist.add(new selectoption('MO','MO'));
		statelist.add(new selectoption('MT','MT'));
		statelist.add(new selectoption('NE','NE'));
		statelist.add(new selectoption('NV','NV'));
		statelist.add(new selectoption('NH','NH'));
		statelist.add(new selectoption('NJ','NJ'));
		statelist.add(new selectoption('NM','NM'));
		statelist.add(new selectoption('NY','NY'));
		statelist.add(new selectoption('NC','NC'));
		statelist.add(new selectoption('ND','ND'));
		statelist.add(new selectoption('OH','OH'));
		statelist.add(new selectoption('OK','OK'));
		statelist.add(new selectoption('OR','OR'));
		statelist.add(new selectoption('PA','PA'));
		statelist.add(new selectoption('RI','RI'));
		statelist.add(new selectoption('SC','SC'));
		statelist.add(new selectoption('SD','SD'));
		statelist.add(new selectoption('TN','TN'));
		statelist.add(new selectoption('TX','TX'));
		statelist.add(new selectoption('UT','UT'));
		statelist.add(new selectoption('VT','VT'));
		statelist.add(new selectoption('VA','VA'));
		statelist.add(new selectoption('WA','WA'));
		statelist.add(new selectoption('WV','WV'));
		statelist.add(new selectoption('WI','WI'));
		statelist.add(new selectoption('WY','WY'));
		return statelist;
	}
	
	/**
	 * getSchemaPicklist
	 * @description
	 * @param string objectname, string fieldname
	 * @return list<SelectOption> List of selectoptions from the specified object's picklist field
	 */	
	public static list<SelectOption> getSchemaPicklist(string objectname, string fieldname){
		list<SelectOption> selectoptionslist = new list<SelectOption>();
		selectoptionslist.add(new selectoption('', '--Select One--'));
		for (Schema.Picklistentry ple : schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap().get(fieldname.toLowerCase()).getDescribe().getPickListValues()) {
			selectoptionslist.add(new selectoption(ple.getvalue(), ple.getlabel()));
		}
		return selectoptionslist;	
	}
	
	/**
	 * callout
	 * @description
	 * @param string calloutMethod, map<string,string> headers, string endPoint, map<string,string> parameters, string body, boolean compression, string testbody
	 * @return HTTPResponse
	 */
	public static HTTPResponse callout(string calloutMethod, map<string,string> headers, String endPoint, map<string,string> parameters, String body, boolean compression, string testBody){
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		req.setMethod(calloutMethod.toUpperCase());
		if (headers!=null) {
			for (string s : headers.keyset()) {
				req.setHeader(s,headers.get(s));
			}
		}
		string fullEndPoint = endpoint+'?';
		if (parameters!=null){
			for (string s : parameters.keyset()) {
				fullEndpoint += s+'='+utf8(parameters.get(s))+'&'; 
			}
		}
		fullEndpoint = fullEndpoint.substring(0,fullEndpoint.length()-1).trim();
		req.setEndpoint(fullEndpoint);
		if (calloutMethod.toUpperCase() == 'POST' && body != null) req.setBody(body);
		req.setCompressed(compression);
		HttpResponse res = new HttpResponse();
		res.setStatusCode(200);
		res.setBody((testbody!=null)?testBody:'');
		if ( test.isRunningTest() == false) res = h.send(req);
		system.debug('Request:\n\nEndpoint: '+req.getendpoint()+((req.getBody()!=null)?'':'\n\nBody:\n'+req.getBody()));
		system.debug('Response:\n\n'+res.getStatus()+'\n\n'+res.getBody());
		return res;
	}
	
	/**
	 * cloneObject
	 * @description
	 * @param string objectName
	 * @param sObject input
	 * @return sObject with cloned values
	 */
	public static sObject cloneObject(sObject input){
		sObject output = input.getsobjecttype().newSObject();
		map <String, Schema.SObjectField> fieldMap = schema.getGlobalDescribe().get(input.getsobjecttype().getDescribe().getname().tolowercase()).getDescribe().fields.getMap();
		for (string field : fieldMap.keySet()) {
			Schema.DescribeFieldResult fieldDescribe = fieldmap.get(field).getDescribe();
			if (fieldDescribe.isUpdateable() && fieldDescribe.isAutoNumber() == false && fieldDescribe.isCalculated() == false) {
				system.debug(field+input.get(field));
				output.put(field,input.get(field));
			}
		}
		return output;
	}

	/**
	 * saveCookie
	 * @description
	 * @param string name
	 * @param string cookieval
	 * @param integer ttl
	 * @param boolean h24
	 */
	public static void saveCookie (string name, string cookieval, integer ttl, boolean h24) {
		Cookie cook = new Cookie(name,cookieval,null,((h24)?86400:ttl),false);
		Apexpages.currentPage().setcookies(new list<cookie>{cook});
	}
	
	/**
	 * loadCookie
	 * @description
	 * @param string name
	 * @return string cookieValue or 'Not Found'
	 */
	public static string loadCookie (string name){
		return (apexpages.currentPage().getCookies().get(name) != null)?apexpages.currentPage().getCookies().get(name).getValue():'Not Found';
	}
	
	/**
	 * queryAllFields
	 * @description Returns list of query results for the specified object with all of its fields, filtered
	 * @param string objectname
	 * @param string filtersLimits
	 * @return list<sObject>
	 */
	public static list<sObject> queryAllFields (string objectName, string filtersLimits) {
		string queryString = 'Select ';
		for (string s : schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap().keySet()){
			queryString += s+', ';
		}
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		queryString+= ' FROM '+objectname;
		if (filtersLimits != null) queryString+= ' where '+filtersLimits;
		
		return database.query(queryString);
	}
	
	/**
	 * queryAllFieldsAndParents
	 * @description Returns list of query results for the specified object with all of its fields, and specified parent fields filtered
	 * @param string objectname
	 * @param map<string,string> parentObjectToNamedFields
	 * @param string filtersLimits
	 * @return list<sObject>
	 */
	public static list<sObject> queryAllFieldsAndParents (string objectName, map<string,string> parentObjectToNamedFields, string filtersLimits) {
		string queryString = 'Select ';
		for (string s : schema.getGlobalDescribe().get(objectname.toLowerCase()).getDescribe().fields.getMap().keySet()){
			queryString += s+', ';
		}
		for (string s : parentObjectToNamedFields.keyset()) {
			string parentfield = parentObjectToNamedFields.get(s);
			parentfield += (parentObjectToNamedFields.get(s).contains('__c'))?'__r':'';
			parentField = parentfield.replace('__c__r','__r');
			for (string f : schema.getGlobalDescribe().get(s.toLowerCase()).getDescribe().fields.getMap().keySet()){
				querystring += parentfield+'.'+f+', ';
			}
		}
		queryString = queryString.trim();
		queryString = queryString.substring(0,queryString.length()-1);
		queryString+= ' FROM '+objectname;
		if (filtersLimits != null) queryString+= ' where '+filtersLimits;
		
		return database.query(queryString);
	}
	
	/**
	 * reverseList
	 * @description Takes an input of a list of string, and reverses their order.
	 * @param list<string> input
	 * @return list<string> reversed list
	 */
	public static list<string> reverseList (list<string> input) {
		list<string> output = new list<string>();
		for (integer i=input.size()-1; i>=0; i--) {
			output.add(input[i]);
		}
		return output;
	}
	
	/**
	 * reverseList
	 * @description Takes an input of a list of sObject, and reverses their order.
	 * @param list<sObject> input
	 * @return list<sObject> reversed list
	 */
	public static list<sObject> reverseList (list<sObject> input) {
		list<sObject> output = new list<sObject>();
		for (integer i=input.size()-1; i>=0; i--) {
			output.add(input[i]);
		}
		return output;
	}
	
	/**
	 * sObjectSort
	 * @description Takes an input of a list of sObject, and sorts them on input field criteria in specified order.
	 * @param list<sObject> input
	 * @param string criteriaField api name of field
	 * @param string sortOrder (ASC, DESC)
	 * @return list<sObject> sorted list
	 */
	public static list<sObject> sObjectSort(list<sObject> input, string criteriaField, string sortOrder) {
		map<object, list<sObject>> sortingMap = new map<object, list<sObject>>();
		for (sObject s : input){
			if (sortingMap.get(s.get(criteriaField.toLowerCase()))!= null) sortingMap.get(s.get(criteriaField.toLowerCase())).add(s);
			else sortingMap.put(s.get(criteriaField.toLowerCase()),new list<sObject>{s});
		}
		list<object> sortedKeys = (new list<object>(sortingMap.keyset()));
		sortedKeys.sort();
		list<sObject> output = new list<sObject>();
		for (object k : sortedKeys) output.addAll(sortingMap.get(k));
		return (sortOrder.toLowerCase() == 'desc')?reverseList(output):output;	
	}
	
	/**
	 * setUserPassword
	 * @description 
	 * @param string userid
	 * @param string pass
	 * @param idUnknown
	 **/
	public static void setUserPassword(string userNameOrId, string pass, boolean idUnknown){
		system.setPassword((idUnknown)?[select id from user where username=:userNameOrId limit 1].id:userNameOrId, pass);	
	}
	
	/**
	 * utf8
	 * @param string input
	 * @return string UTF-8 encoded input
	 **/
	public static string utf8 (string input){
		return encodingUtil.urlEncode(input,'UTF-8');
	}
	
	/**
	 * utf8decode
	 * @param string input
	 * @return string UTF-8 decoded input
	 **/
	public static string utf8decode (string input){
		return encodingUtil.urlDecode(input,'UTF-8');
	}
	
	/**
	 * reparentAttachments
	 * @description
	 * @param list<Attachment> inputlist
	 * @param string newParent
	 **/
	public static void reparentAttachments (list<Attachment> inputList, string newParentId){
		list<Attachment> newlist = new list<Attachment>();
		for (Attachment a : inputlist){
			Attachment att = new Attachment();
			att.Body = fixnull(a.body);
			att.ContentType = fixnull(a.contenttype);
			att.description = fixnull(a.description);
			att.isprivate = fixnull(a.isprivate);
			att.name = fixnull(a.name);
			att.ownerid = fixnull(a.ownerid);
			att.parentid = fixnull(newParentId);
			newlist.add(att);
		}
		if (newlist.size() > 0) insert newlist; delete inputlist;
	}
	
	public static void scheduleDebugLog(string username){
		pagereference p = new pagereference('/setup/ui/listApexTraces.apexp?user_id='+username+'&user_logging=true');
		blob b = p.getContent();
	}
	
	public static string twoDigitIntegerString(integer input){
		if (input < 10){
			return '0'+input;
		}
		else return string.valueof(input);
	}
	
	public static string SQLDateFormat(date input){
		string temp ='';
		temp += input.year();
		temp += '-';
		temp += (input.month()<10)?'0'+input.month():string.valueof(input.month());
		temp += '-';
		temp += (input.day()<10)?'0'+input.day():string.valueof(input.day());
		return temp;		
	}
	
	public static string SQLDateTimeFormat(datetime input){
		string temp ='';
		temp += input.year();
		temp += '-';
		temp += (input.month()<10)?'0'+input.month():string.valueof(input.month());
		temp += '-';
		temp += (input.day()<10)?'0'+input.day():string.valueof(input.day());
		temp += ' ';
		
		temp += (input.hour()<10)?'0'+input.hour():string.valueof(input.hour());
		temp += ':';
		temp += (input.minute()<10)?'0'+input.minute():string.valueof(input.minute());
		temp += ':';
		temp += (input.second()<10)?'0'+input.second():string.valueof(input.second());
		return temp;		
	}
	
	public static string SOQLDateTimeFormat(datetime input){
		string temp ='';
		temp += input.year();
		temp += '-';
		temp += (input.month()<10)?'0'+input.month():string.valueof(input.month());
		temp += '-';
		temp += (input.day()<10)?'0'+input.day():string.valueof(input.day());
		temp += 'T';
		
		temp += (input.hour()<10)?'0'+input.hour():string.valueof(input.hour());
		temp += ':';
		temp += (input.minute()<10)?'0'+input.minute():string.valueof(input.minute());
		temp += ':';
		temp += (input.second()<10)?'0'+input.second():string.valueof(input.second());
		temp += '.0Z';
		return temp;
	}
	
	public static void pagemsg(string severity, string msg){
		if (severity == 'info') apexpages.addmessage(new apexpages.message(apexpages.severity.info, msg));
		else if (severity == 'error') apexpages.addmessage(new apexpages.message(apexpages.severity.error, msg));
		else if (severity == 'confirm') apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, msg));
	}
	
	public static boolean isSandbox(){
		string temp = URL.getSalesforceBaseUrl().toExternalForm();
		temp = temp.substringbefore('.force.com');
		boolean sandbox = (temp.contains('.cs'))?true:false;
		return sandbox;
	}
	
	@isTest
	static void utility_Tests(){
		//Test Variables
		blob testblob;
		Boolean testBoolean;
		Contact testContact = new Contact();
		date testdate;
		datetime testdt;
		decimal testdecimal;
		double testDouble;
		HTTPResponse testHTTPResponse;
		id testid;
		integer testint;
		list<Contact> cList;
		list<SelectOption> testSelectOptionList; 
		long testlong;
		Pagereference p;
		string teststring;
		time testtime;
		
		//isNullOrBlank Tests
		testBoolean = isNullOrBlank(testBoolean);
		testBoolean = isNullOrBlank(0);
		testBoolean = isNullOrBlank(0);
		testBoolean = isNullOrBlank('');
		testBoolean = isNullOrBlank(system.today());
		testBoolean = isNullOrBlank(system.now());
		testBoolean = isNullOrBlank(system.now().time());
		testBoolean = isNullOrBlank(blob.valueof(''));
		testBoolean = isNullOrBlank(0);
		testBoolean = isNullOrBlank('test');
		testBoolean = isNullOrBlank(0);
		
		//fixNull Tests
		testint 	= fixNull(testint);
		testdecimal = fixNull(testdecimal);
		teststring 	= fixNull(teststring);
		testdate	= fixNull(testdate);
		testdt	 	= fixNull(testdt);
		testtime	= fixNull(testtime);
		testblob	= fixNull(testblob);
		testDouble	= fixNull(testDouble);
		testid		= fixNull(testid);
		testlong	= fixNull(testlong);
		boolean newboolean;
		testBoolean = fixNull(newboolean);

		//validateEmail Tests
		testBoolean = validateEmail('test@test.com');
		testBoolean = validateEmail(null);
		
		//getStateListOptions Tests
		testSelectOptionList = getUSStateListOptions();
		testSelectOptionList = getSchemaPicklist('opportunity','stagename');
		
		//callout Tests
		testHTTPResponse = callout('POST', new map<string,string>{'Content-length'=>string.valueof(('test').length())}, 'http://test.com', new map<string,string>{'name'=>'test'}, 'test', false, 'test');
		
		//cloneObject Tests
		testContact = (Contact)cloneObject(testContact);
		
		//saveCookie Test
		p = new PageReference('/');
		test.setCurrentPage(p);
		saveCookie('testCookie', 'testbody', null, true);
		
		//loadCookie Test
		loadCookie('testCookie');
		
		//queryAllFields Test
		clist = (list<Contact>)queryAllFields('contact', 'name != null limit 1');
		
		//queryAllFieldsAndParents Test
		list<Contact> cListWithA = (list<Contact>)queryAllFieldsAndParents('contact', new map<string,string>{'account'=> 'account'} , 'name != null limit 1');
		
		//reverseList Test
		list<string> reversedList = reverseList(new list<string>{'5','4','3','2','1'});

		//sObjectSort Test
		clist = (list<Contact>)sObjectSort(new list<Contact>{new Contact(lastname='test1'), new Contact(lastname='test2')}, 'lastname', 'desc');
		
		//UTF-8 Encode/Decode
		teststring = utf8('test 20');
		teststring = utf8decode(teststring);
		
		teststring = twoDigitIntegerString(9);
		teststring = twoDigitIntegerString(10);
		
		//SQL date and datetime formats
		teststring = sqldateformat(system.today());
		teststring = sqldatetimeformat(system.now());
		teststring = soqldatetimeformat(system.now());
	}
	
	@isTest (seealldata=true)
	static void setUserPassword_Test(){
		User u = new User();
		u.username= 'utiltest@user.com5.util.test';
		u.lastname = 'lastname';
		u.email = 'email@test.com';
		u.alias = 'alias';
		u.communitynickname = 'nick';
		u.timezonesidkey = 'America/Tijuana';
		u.localesidkey ='en_US';
		u.profileid = [select id from Profile where name = 'System Administrator' limit 1].id;
		u.EmailEncodingKey = 'UTF-8';
		u.languagelocalekey='en_US';
		
		try{ // there may be required custom fields that would cause insert to fail		
			insert u;
			setUserPassword(u.id, 'tesing1pass',false);
		}
		catch(exception e){}//fail quietly
	} 

	@isTest (seealldata=true)
	static void reparentAttachment_Test(){
		boolean unableToTest = false;
		
		Account a;
		list<Account> alist = [select id from account limit 1];
		if (alist.size()==0) {
			try{ //Accounts may have validation rules on them that cause insert to fail
				a = new Account(name='testacct');
				insert a;
			}
			catch(exception e){unableToTest=true;} // fail quietly
		}
		else {
			a = alist[0];
		}
		if (!unableToTest){
			Attachment att = new Attachment(name='testatt', body=blob.valueof('test'),parentid=a.id, ownerid=userinfo.getuserid());
			insert att;
			reparentAttachments(new list<Attachment>{att}, a.id);
		}
	}
	
	// call out tester
	// xml generator, json generator 
	// log a case
	// query builder
	// sandbox mover
	// schema extract - xls, xml, pdf
	// profile 
}