@isTest
private class testContact_Methods {

    static testMethod void updateProbonoFirmOnCasesWithLawFirm() {
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		List<Account> accounts = new List<Account>();
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account anotherLawFirm = new Account(Name = 'anotherLawFirm');
		anotherLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(anotherLawFirm);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact primaryOnCase = new Contact(FirstName = 'primaryOnCase',LastName = 'primaryOnCase', Law_Firm__c = aLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(primaryOnCase);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
		insert myCase;
		
		primaryOnCase.Law_Firm__c = anotherLawFirm.Id;
		
		Test.startTest();
			update primaryOnCase;
		Test.stopTest();
        
        myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
        system.assertEquals(primaryOnCase.Law_Firm__c, myCase.Probono_Firm__c);
        
    }
    
    static testMethod void updateProbonoFirmOnCasesWithLawSchool() {
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];
		List<Account> accounts = new List<Account>();
		Account aLawFirm = new Account(Name = 'aLawFirm');
		aLawFirm.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawFirm);
		Account aLawSchool = new Account(Name = 'aLawSchool');
		aLawSchool.RecordTypeId = lawFirmRecordType.Id;
		accounts.add(aLawSchool);
		insert accounts;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact primaryOnCase = new Contact(FirstName = 'primaryOnCase',LastName = 'primaryOnCase', Law_Firm__c = aLawFirm.Id, Law_School__c = aLawSchool.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(primaryOnCase);
		insert contactList;
		
		RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
		insert myCase;
		
		primaryOnCase.Law_Firm__c = null;
		
		Test.startTest();
			update primaryOnCase;
		Test.stopTest();
        
        myCase = [SELECT Probono_Firm__c FROM Case__c WHERE Id = :myCase.Id];
        system.assertEquals(primaryOnCase.Law_School__c, myCase.Probono_Firm__c);
        
    }
    
    static testMethod void updateContactsWithFirmOrSchoolQuantOnCase() {
        
        RecordType lawFirmRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Law_Firm'];	
		Account primaryLawFirm = new Account(Name = 'primaryLawFirm');
		primaryLawFirm.RecordTypeId = lawFirmRecordType.Id;
		insert primaryLawFirm;
		
		RecordType contactRecordType = [SELECT DeveloperName FROM RecordType WHERE DeveloperName = 'Staff'];
		List<Contact> contactList = new List<Contact>();
		Contact contactForCase = new Contact(FirstName = 'contactForCase',LastName = 'contactForCase', RecordTypeId = contactRecordType.Id);
		contactList.add(contactForCase);
		Contact primaryOnCase = new Contact(FirstName = 'pimaryOnCase',LastName = 'pimaryOnCase', Law_Firm__c = primaryLawFirm.Id, RecordTypeId = contactRecordType.Id);
		contactList.add(primaryOnCase);
		insert contactList;
		
        RecordType caseRecordType = [SELECT DeveloperName FROM RecordType WHERE SobjectType = 'Case__c' LIMIT 1];
		Case__c myCase = new Case__c();
        myCase.Matter_Type_Detail__c = 'Child Support';
        myCase.Type_of_Matter__c = 'Follow-up Case Type';
		myCase.Contact__c = contactForCase.Id;
		myCase.RecordTypeId = caseRecordType.Id;
		myCase.Primary_Staff_on_Case__c = primaryOnCase.Id;
		insert myCase;
		
		Decimal quantBeforeUpdate = myCase.Quant_Contacts_Law_Firm_School__c;
		
		primaryOnCase.Law_Firm__c = null;
		
		Test.startTest();
			update primaryOnCase;
		Test.stopTest();
		
		myCase = [SELECT Quant_Contacts_Law_Firm_School__c FROM Case__c WHERE Id = :myCase.Id];
		system.assertEquals(0, myCase.Quant_Contacts_Law_Firm_School__c);
		system.assertNotEquals(quantBeforeUpdate, myCase.Quant_Contacts_Law_Firm_School__c);
    }
    
}