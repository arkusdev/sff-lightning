global class UpdateCasesBatchJob implements Database.Batchable<sObject>{

     global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator('SELECT Quant_Contacts_Law_Firm_School__c, Name FROM Case__c LIMIT 50000000');
     }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        update scope;
    }
    
    global void finish(Database.BatchableContext BC){

   }
}