public with sharing class Household_Methods {
	public static void afterInsert (list<npo02__Household__c> newlist) {
		Set<Id> householdIds = new Set<Id>();
		for (integer i =0; i<newlist.size();i++) {
			householdIds.add(newList[i].Id);
		}
		futureUpdate(householdIds);
	}
	
	public static void beforeUpdate (list<npo02__Household__c> oldlist, map<id, npo02__Household__c> newmap) {
		for(Contact c : [select Id, FirstName, LastName, npo02__Household__c, npo02__Household__r.Household_Auto_Number__c from Contact where npo02__Household__c in :newmap.keyset()]) {
			newmap.get(c.npo02__Household__c).name = c.LastName + ' ' + c.FirstName + ' Household #' + c.npo02__Household__r.Household_Auto_Number__c;
		}
	}
	
	@future
	public static void futureUpdate(set<id> hSet){
		update [select id from npo02__Household__c where id in :hSet];
	}
	
	@isTest
	static void Household_Methods_Test(){
		npo02__Household__c hh=new npo02__Household__c();
		insert hh;
		hh.Name='test';
		update hh;
		delete hh;		
	}
	
}