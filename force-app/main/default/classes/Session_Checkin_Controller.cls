public with sharing class Session_Checkin_Controller {
	public Service__c thisService {get; set;}
	public Contact dateholder {get; set;}
	
	public Date selecteddate {get; set;}
	public String selectedServiceId {get; set;}
	public Service__c selectedService {get; set;}
	public List<Service_Facilitator__c> selectedServiceFacilitators {get; set;}
	
	public Map<String, List<Session__c>> sessionMap {get; set;}
	public List<Session__c> sessionRows {get; set;}
	public Session__c newSession {get; set;}
	public List<SelectOption> options {get; set;}
	public String errorStr {get; set;}
	
	public Boolean createServiceBool {get; set;}
	public Service__c newService {get; set;}
	public Service_Facilitator__c tempServiceFacilitator {get; set;}
	public List<Service_Facilitator__c> newServiceFacilitators {get; set;}
	
	public String deleteSessionId {get; set;}
	
	public Boolean prevSessionsBool {get; set;}
	public Contact prevdateholder {get; set;}
	public List<SelectOption> prevOptions {get; set;}
	public Map<String, List<Session__c>> prevSessionMap {get; set;}
	public List<Session__c> prevSessions {get; set;}
	public String prevServiceId {get; set;}
	
	public Session_Checkin_Controller(Apexpages.standardController con) {
		thisService = (Service__c) con.getRecord();
		
		if(thisService.Id == null) {
			selecteddate = System.today();
		}
		else {
			List<Service__c> tempServiceList = [SELECT Id, Name FROM Service__c WHERE Id = :thisService.Id];
			thisService = tempServiceList.get(0);
			selecteddate = System.today();
			List<Session__c> temp = [SELECT Session_Date__c FROM Session__c WHERE Session_Date__c >= :selecteddate AND Service__c = :thisService.Id ORDER BY Session_Date__c ASC LIMIT 1];
			if(temp.size() == 0) {
				selecteddate = System.today();
			}
			else {
				selecteddate = temp.get(0).Session_Date__c;
			}
			selectedServiceId = thisService.Id;
			tempServiceList = [SELECT Notes__C, Name, Id, Mode__c FROM Service__c WHERE Id = :selectedServiceId];
			selectedService = tempServiceList.get(0);
			selectedServiceFacilitators = [SELECT Contact__r.Name FROM Service_Facilitator__c WHERE Service__c = :selectedService.Id];
			sessionRows = [SELECT Id, Service__c, Service__r.Service__c, Service__r.Name, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Attendance_Status__c, Session_Date__c, Service__r.Subtype__c, Service__r.Area_of_Focus__c, Service__r.Notes__c, Name from Session__c where Session_Date__c = :selecteddate AND Service__c = :thisService.Id ORDER BY Case__r.Contact__r.Name];
		}
		dateholder = new Contact();
		prevdateholder = new Contact();
		dateholder.birthdate = selecteddate;
		prevdateholder.birthdate = System.today();
		newSession = new Session__c();
		newService = new Service__c();
		tempServiceFacilitator = new Service_Facilitator__c();
		newServiceFacilitators = new List<Service_Facilitator__c>();
		createServiceBool = false;
		prevSessionsBool = false;
		compileOptions();
		prevCompileOptions();
		errorStr = '';
	}
	
	public void compileOptions() {
		errorStr = '';
		selecteddate = dateholder.birthdate;
		sessionMap = new Map<String, List<Session__c>>();
		options = new List<SelectOption>();
		Set<Id> uniqueServices = new Set<Id>();
		if(selectedService != null) {
			Service__c temp_serv = [SELECT Name, Id, Service__c FROM Service__c WHERE Id = :selectedService.Id];
			options.add(new selectoption(temp_serv.Id, temp_serv.Name + '-' + temp_serv.Service__c));
			uniqueServices.add(temp_serv.Id);
		}
		else {
			options.add(new selectoption('', '-- None Selected --'));
		}
		
		for(Session__c s : [SELECT Id, Service__c, Service__r.Service__c, Service__r.Name, Service__r.Subtype__c, Service__r.Area_of_Focus__c, Service__r.Notes__c, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Attendance_Status__c, Session_Date__c, Name from Session__c where Session_Date__c = :selecteddate ORDER BY Case__r.Contact__r.Name]) {
			if(!uniqueServices.contains(s.Service__c)) {
				options.add(new selectoption(s.Service__c, s.Service__r.Name + '-' + s.Service__r.Service__c));
				uniqueServices.add(s.Service__c);
			}
			List<Session__c> templist = new List<Session__c>();
			if(sessionMap.containsKey(s.Service__c)) {
				templist = sessionMap.get(s.Service__c);
				templist.add(s);
				sessionMap.put(s.Service__c, templist);
			}
			else {
				templist.add(s);
				sessionMap.put(s.Service__c, templist);
			}
		}
		if(selectedService != null) {
			sessionRows = sessionMap.get(selectedServiceId);
		}
	}
	
	public PageReference resetOptions() {
/*		
		Map<Id,Service__c> serviceMap = new Map<Id,Service__c>([Select Id, (Select Id from Sessions__r order by ) from Service__c]);
*/		
		compileOptions();
		return null;
	}
	
	public void setService(String serviceId) {
		errorStr = '';
		selectedServiceId = serviceId;
	}
	
	public PageReference retrieveSessionRows() {
		errorStr = '';
		sessionRows = sessionMap.get(selectedServiceId);
		List<Service__c> tempServiceList = [SELECT Notes__C, Name, Id, Mode__c FROM Service__c WHERE Id = :selectedServiceId];
		selectedService = tempServiceList.get(0);
		selectedServiceFacilitators = [SELECT Contact__r.Name FROM Service_Facilitator__c WHERE Service__c = :selectedService.Id];
		return null;
	}
	
	public pagereference allpresent(){
		for (Session__c s : sessionRows){
			s.Attendance_Status__c = 'Present';
		}
		return null;
	}
	
	public PageReference saveSessions() {
		if(selectedService != null) {
			if(newSession.Case__c != null) {
				errorStr = '';
				
				// If the session list isn't empty, check if this contact
				// already has a session
				Boolean alreadyInSession = false;
				if(sessionRows != null) {
					for(Session__c check : sessionRows) {
						if(check.Case__r.Contact__r.Name == newSession.Case__r.Contact__r.Name) {
							alreadyInSession = true;
							break;
						}
					}
				}
				
				if(alreadyInSession == false) {
					newSession.Service__c = selectedService.Id;
					List<Case__c> tempCaseList = [SELECT name, Contact__c, Contact__r.Name FROM Case__c WHERE Id = :newSession.Case__c];
					Id newContact = tempCaseList.get(0).Contact__c;
					String tempName = tempCaseList.get(0).Name;
					System.debug(tempName);
					newSession.Contact__c = newContact;
					newSession.Session_Date__c = dateholder.birthdate;
					insert newSession;
					Session__c temp_sess = [SELECT Id, Service__c, Service__r.Service__c, Service__r.Name, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Attendance_Status__c, Session_Date__c, Service__r.Subtype__c, Service__r.Area_of_Focus__c, Service__r.Notes__c, Name from Session__c where Id = :newSession.Id ORDER BY Case__r.Contact__r.Name];
					if(sessionRows == null) {
						sessionRows = new List<Session__c>();
					}
					sessionRows.add(temp_sess);
				}
				newSession = new Session__c();
			}
			if(sessionRows != null) {
				upsert sessionRows;
			}
			update selectedService;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved Successfully');
			ApexPages.addMessage(myMsg);
		}
		else {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select service before saving changes');
			ApexPages.addMessage(myMsg);
		}
		return null;
	}
	
	public PageReference addSession() {
		if(selectedService != null) {
			errorStr = '';
			if(newSession.Case__c != null) {
				
				// If the session list isn't empty, check if this contact
				// already has a session
				Boolean alreadyInSession = false;
				if(sessionRows != null) {
					for(Session__c check : sessionRows) {
						if(check.Case__r.Contact__r.Name == newSession.Case__r.Contact__r.Name) {
							alreadyInSession = true;
							break;
						}
					}
				}
				
				if(alreadyInSession == false) {
					newSession.Service__c = selectedService.Id;
					//newSession.Contact__c = newSession.Case__r.Contact__c;
					List<Case__c> tempCaseList = [SELECT Contact__c, Contact__r.Name FROM Case__c WHERE Id = :newSession.Case__c];
					Id newContact = tempCaseList.get(0).Contact__c;
					String tempName = tempCaseList.get(0).Contact__r.Name;
					System.debug(tempName);
					newSession.Contact__c = newContact;
					newSession.Session_Date__c = dateholder.birthdate;
					insert newSession;
					Session__c temp_sess = [SELECT Id, Service__c, Service__r.Service__c, Service__r.Name, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Attendance_Status__c, Session_Date__c, Service__r.Subtype__c, Service__r.Area_of_Focus__c, Service__r.Notes__c, Name from Session__c where Id = :newSession.Id ORDER BY Case__r.Contact__r.Name];
					if(sessionRows == null) {
						sessionRows = new List<Session__c>();
					}
					sessionRows.add(temp_sess);
				}
				else {
					errorStr = 'Contact already has session with this service';
				}	
				newSession = new Session__c();
			}
			else {
				errorStr = 'New Member field cannot be empty';
			}
		}
		else {
			errorStr = 'Select a service before adding sessions';
		}
		return null;
	}
	
	public PageReference deleteSession() {
       if (apexpages.currentpage().getparameters().get('deleteSessionId') != null) {
           deleteSessionId = apexpages.currentpage().getparameters().get('deleteSessionId');
           system.debug(apexpages.currentpage().getparameters().get('deleteSessionId'));
       }
       System.debug('DELETESESSIONID: ' + deleteSessionId);
       for (Integer i = 0; i < sessionRows.size(); i++) {
           if (sessionRows[i].Id == deleteSessionId){
           		System.debug('INSIDE FOR DELETESESSION()');
               List<Session__c> templist = [SELECT Id FROM Session__c WHERE Id = :sessionRows[i].Id];
               Session__c temp = templist.get(0);
               sessionRows.remove(i);
               delete temp;
               break;
           }
       }
       deleteSessionId = '';
       return null;
	}
	
	public PageReference addServiceFacilitator() {
		if(tempServiceFacilitator.Contact__c != null) {
			errorStr = '';
			Boolean alreadyInService = false;
			for(Service_Facilitator__c sf : newServiceFacilitators) {
				if(sf.Contact__c == tempServiceFacilitator.Contact__c) {
					alreadyInService = true;
					break;
				}
			}
			
			if(alreadyInService == false) {
				newServiceFacilitators.add(tempServiceFacilitator);
			}
			else {
				errorStr = 'Already facilitating service';
			}
			tempServiceFacilitator = new Service_Facilitator__c();
		}
		else {
			errorStr = 'Service Facilitator field cannot be empty';
		}
		return null;
	}
	
	public PageReference createService() {
		errorStr = '';
		createServiceBool = true;
		return null;
	}
	
	public PageReference returnToService() {
		errorStr = '';
		PageReference p = new PageReference('/' + thisService.Id);
		return p;
	}
	
	public PageReference cancelCreateService() {
		errorStr = '';
		newService = new Service__c();
		tempServiceFacilitator = new Service_Facilitator__c();
		newServiceFacilitators = new List<Service_Facilitator__c>();
		PageReference p = page.session_checkin;
		p.getparameters().put('id', thisService.Id);
		p.setredirect(true);
		return p;
	}
	
	public PageReference saveService() {
		errorStr = '';
		insert newService;
		if(newServiceFacilitators.size() > 0) {
			for(Integer i = 0; i < newServiceFacilitators.size(); i++) {
				newServiceFacilitators[i].Service__c = newService.Id;
			}
			insert newServiceFacilitators;
		}
		createServiceBool = false;
		selecteddate = System.today();
		dateholder.birthdate = selecteddate;
		selectedService = [SELECT Notes__C, Name, Id, Mode__c FROM Service__c WHERE Id = :newService.Id];
		selectedServiceId = selectedService.Id;
		selectedServiceFacilitators = [SELECT Contact__r.Name FROM Service_Facilitator__c WHERE Service__c = :selectedService.Id];
		sessionMap.put(selectedService.Id, new List<Session__c>());
		//newService = new Service__c();
		tempServiceFacilitator = new Service_Facilitator__c();
		newServiceFacilitators = new List<Service_Facilitator__c>();
		
		PageReference p = page.session_checkin;
		p.getparameters().put('id', selectedService.Id);
		p.setredirect(true);
		return p;
	}
	
	public PageReference copyPreviousSessions() {
		errorStr = '';
		prevSessionsBool = true;
		return null;
	}
	
	public void prevCompileOptions() {
		errorStr = '';
		prevSessionMap = new Map<String, List<Session__c>>();
		prevOptions = new List<SelectOption>();
		Set<Id> uniqueServices = new Set<Id>();
		prevOptions.add(new selectoption('', '-- None Selected --'));
		
		for(Session__c s : [SELECT Id, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Service__c, Service__r.Name, Service__r.Service__c from Session__c where Session_Date__c = :prevdateholder.birthdate ORDER BY Case__r.Contact__r.Name]) {						
			if(s.Service__c == selectedService.Id) {
				continue;
			}
			if(!uniqueServices.contains(s.Service__c)) {
				prevOptions.add(new selectoption(s.Service__c, s.Service__r.Name + '-' + s.Service__r.Service__c));
				uniqueServices.add(s.Service__c);
			}
			List<Session__c> templist = new List<Session__c>();
			if(prevSessionMap.containsKey(s.Service__c)) {
				templist = prevSessionMap.get(s.Service__c);
				templist.add(s);
				prevSessionMap.put(s.Service__c, templist);
			}
			else {
				templist.add(s);
				prevSessionMap.put(s.Service__c, templist);
			}
		}
	}
	
	public PageReference resetPrevOptions() {
		prevCompileOptions();
		return null;
	}
	
	public PageReference retrievePrevSessions() {
		errorStr = '';
		prevSessions = prevSessionMap.get(prevServiceId);
		
		// Get the Contact Ids for contacts that have sessions with the current service
		Set<Id> currentContacts = new Set<Id>();
		if(sessionRows != null) {
			for(Session__c s : sessionRows) {
				currentContacts.add(s.Case__r.Contact__c);
			}
		}
		
		for(Integer i = 0; i < prevSessions.size(); i++) {
			if(currentContacts.contains(prevSessions[i].Case__r.Contact__c)) {
				prevSessions.remove(i);
				i--;
			}
		}
		
		if(prevSessions.size() == 0) {
			errorStr = 'No contacts available for this service';
		}
		return null;
	}
	
	public PageReference savePrevSessions() {
		errorStr = '';
		if(prevServiceId != null) {
			List<Session__c> tempSessionList = new List<Session__c>();
			for(Session__c s : prevSessions) {
				Session__c tempSess = new Session__c();
				tempSess.Service__c = selectedService.Id;
				tempSess.Session_Date__c = dateholder.birthdate;
				tempSess.Case__c = s.Case__c;
				tempSess.Contact__c = s.Contact__c;
				tempSessionList.add(tempSess);
			}
			insert tempSessionList;
			sessionRows = [SELECT Id, Service__c, Service__r.Service__c, Service__r.Name, Contact__c, Case__c, Case__r.Contact__c, Case__r.Contact__r.Name, Case__r.Contact__r.Birthdate, Attendance_Status__c, Session_Date__c, Service__r.Subtype__c, Service__r.Area_of_Focus__c, Service__r.Notes__c, Name from Session__c where Session_Date__c = :dateholder.birthdate AND Service__c = :selectedService.Id ORDER BY Case__r.Contact__r.Name];
			prevSessionsBool = false;
		}
		else {
			errorStr = 'Select service before saving';
		}
		return null;
	}
	
	public PageReference cancelPrevSessions() {
		errorStr = '';
		prevSessionsBool = false;

		prevdateholder.birthdate = System.today();
		prevSessions = new List<Session__c>();
		prevServiceId = '';
		
		PageReference p = page.session_checkin;
		p.getparameters().put('id', thisService.Id);
		p.setredirect(true);
		return p;
		return null;
	}
	
	@isTest
	static void testingMyController() {
		Service__c test_serv = new Service__c();
		ApexPages.StandardController sc = new ApexPages.StandardController(test_serv);
		Session_Checkin_Controller scc = new Session_Checkin_Controller(sc);
		
		Contact c1 = new Contact(Firstname = 'c1last', Lastname = 'c1last');
		Contact c2 = new Contact(Firstname = 'c1last',Lastname = 'c2last');
		
		insert c1;
		insert c2;
		Case__c c3 = new Case__c();
        c3.Matter_Type_Detail__c = 'Child Support';
		insert c3;
		
		// Attempt to add session with a null service
		scc.addSession();
		System.assert(scc.errorStr == 'Select a service before adding sessions');
		
		// Attempt to save changes with a null service
		scc.saveSessions();
		
		// Set the create service bool to create a new service
		scc.createService();
		System.assert(scc.createServiceBool);
		
		// Adding a new service
		scc.newService = new Service__c(Service__c = 'Advice (Legal)');
		scc.saveService();
		System.assert(scc.newService.Id != null);
		//System.assert(scc.newService.Id == null);
		
		// Adding a new session for the new service
		scc.newSession = new Session__c(Contact__c = c1.Id, Case__c=c3.id);
		scc.addSession();
		List<Session__c> temp_sessions_c1 = [SELECT Id, Name FROM Session__c WHERE Service__c = :scc.selectedService.Id];
		//if(scc.errorStr=='New Member field cannot be empty')
		System.assert(temp_sessions_c1.size() == 1);
		
		// Change session attendance
		scc.sessionRows.get(0).Attendance_Status__c = 'Absent';
		scc.saveSessions();
		temp_sessions_c1 = [SELECT Attendance_Status__c FROM Session__c WHERE Service__c = :scc.selectedService.Id];
		System.assert(temp_sessions_c1.size() == 1);
		System.assert(temp_sessions_c1.get(0).Attendance_Status__c == 'Absent');
		
		// Insert new session and change attendance status
		scc.newSession = new Session__c(Contact__c = c2.Id,Case__c=c3.id);
		scc.sessionRows.get(0).Attendance_Status__c = 'Present';
		scc.saveSessions();
		//List<Session__c> temp_sessions_c2 = [SELECT Id, Name FROM Session__c WHERE Contact__r.Lastname = 'c2last'];
		//temp_sessions_c1 = [SELECT Attendance_Status__c FROM Session__c WHERE Contact__r.LastName = 'c1last'];
		List<Session__c> tem = [SELECT Attendance_Status__c,Contact__r.id FROM Session__c WHERE Service__c=:scc.selectedService.Id];
		System.assert(tem.get(0).Attendance_Status__c=='Present');
		//System.assert(temp_sessions_c2.size() == 1);
		//System.assert(temp_sessions_c1.get(0).Attendance_Status__c == 'Present');
		
		
		
		// Use the controller with an existing service
		test_serv = scc.newService;			
		sc = new ApexPages.StandardController(test_serv);
		scc = new Session_Checkin_Controller(sc);		
		System.assert(test_serv.Id == scc.selectedService.Id);
		scc.addServiceFacilitator();
		
		scc.copyPreviousSessions();
		scc.savePrevSessions();
		scc.resetOptions();
		scc.resetPrevOptions();
		scc.cancelPrevSessions();
		
		
		
		scc.deleteSession();
		
		
		// Set the selectedServiceId
		scc.setService(test_serv.Id);
		
	//	System.assert(test_serv.Id == scc.selectedService.Id);
		
		// Reset the options on the sessionMap
		scc.resetOptions();
		System.assert(scc.sessionMap.size() == 1);
		
		// Retrieve session rows for the selected service id
		scc.retrieveSessionRows();
		//System.assert(scc.sessionRows.size() == 2);
		
		// Insert a new service without any sessions
		test_serv = new Service__c(Service__c = 'Advice (non-Legal)');
		insert test_serv;
		test_serv = [SELECT Id FROM Service__c WHERE Service__c = 'Advice (non-Legal)' LIMIT 1];
		sc = new ApexPages.StandardController(test_serv);
		scc = new Session_Checkin_Controller(sc);
		System.assert(test_serv.Id == scc.selectedService.Id);
		
		// Attempt to add session without the Contact field filled
		scc.addSession();
		System.assert(scc.errorStr == 'New Member field cannot be empty');
	}
}