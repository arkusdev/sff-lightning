@isTest
public class EEPStatusControllerTest {
    
    @isTest
    public static void testCreateEEPStatusOnStatusUpdate(){
        RecordType eepType = [SELECT Id FROM RecordType WHERE sobjecttype = 'Case__c' AND Name = 'Economic Empowerment'];	
        Case__c case1 = new Case__c();
        case1.RecordTypeId = eepType.Id;
        case1.Matter_Type_Detail__c = 'Child Support';
        case1.Type_of_Matter__c = 'Follow-up Case Type';
        insert case1;
        
        test.startTest();
        EEPStatusController.updateCaseEEPStatus(case1.Id, 'Placed', 'Placed',Date.today().addDays(-1));
        List<EEP_Status__c> status1 = [SELECT Id FROM EEP_Status__c];
        
        EEPStatusController.updateCaseEEPStatus(case1.Id, 'Literacy', 'Literacy',Date.today());
        List<EEP_Status__c> status2 = [SELECT Id FROM EEP_Status__c];
        EEP_Status__c lastStatus = EEPStatusController.getLastStatusUpdate(case1.Id);
        test.stopTest();
        
        System.assertEquals(1, status1.size());
        System.assertEquals(2, status2.size());
        
        System.assertEquals('Literacy', lastStatus.Status__c);
		System.assertEquals(Date.today(), lastStatus.Date__c);

    }
}