public with sharing class SFF_detailCase {
    
    	public id      idService{get;set;}
        public String  idCase{get;set;}
        public String  service{get;set;}  
        public String  OwnerService{get;set;}  
        public String  typeService{get;set;} 
        public String  nameC{get;set;}
    	public integer nameCinteger;
        public String  nameS{get;set;}
        public String  AttendanceStatus{get;set;}
        public date    serviceDate{get;set;}
        public String  subtype{get;set;}
        public String  areaFocus{get;set;}
        public decimal horsService{get;set;}
        public decimal unit{get;set;}
        public String  mode{get;set;}
        public String  staffMember{get;set;}
        public SFF_detailCase(id idCaseC,String AttendanceStatusC,String nameSC,String OwnerServiceC, id idServiceC,String serviceC,String typeServiceC,String nameCc,date serviceDateC,String subtypeC,String areaFocusC,decimal horsServiceC,decimal dollarAmountOfServiceProvidedC,String modeC,String staffMemberC){
            AttendanceStatus = AttendanceStatusC;
            nameS        = nameSC;
            OwnerService = OwnerServiceC;
            idCase       = idCaseC;
            idService    = idServiceC;
            service      = serviceC;   
            typeService  = typeServiceC; 
            nameC        = nameCc;
            if(nameC == null){
                nameCinteger = -21;
            }else{
                nameCinteger = integer.valueOf(nameC.substring(2));
            }
            serviceDate = serviceDateC;
            subtype     = subtypeC;
            areaFocus   = areaFocusC;
            horsService = horsServiceC;
            unit        = dollarAmountOfServiceProvidedC;
            mode        = modeC;
            staffMember = staffMemberC;            
        }   
}