global with sharing class MaximumRetentionTimeScheduler implements Schedulable {

   global void execute(SchedulableContext sc) {
      MaximumRetentionTimeHandler mrth = new MaximumRetentionTimeHandler();
      mrth.updateMaximumRetentionTimeOnCases();
   }

}