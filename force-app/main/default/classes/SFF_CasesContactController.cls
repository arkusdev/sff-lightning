global with sharing class SFF_CasesContactController {
    
    
    private final Contact contactRecord;
    transient private Map<ID, Session__c> listSessions;
    //transient private Map<ID, Night__c>   listNight;
    transient private Map<ID, Service__c> listServices;
    private String CaseID;
    private Map<date,id> listdates;
    public List<Event__c>         listEvents      {get;set;}
    public List<SFF_wrapperService>   listWS      {get;set;}
    public List<Session__c> sessionNotes {get;set;}
    
    public List<EventAndSessionNote> evAndNotes {get;set;}

    transient public List<SFF_wrapperService>   Aux_listWS ;
    public Map<String,boolean>    listArrowCol    {get;set;}
    transient public Map<String,String>     listFieldSort  ;
    transient public Map<id,SFF_wrapperService> listWrapp  ;
    public boolean up                   {get;set;} 
    public boolean down                 {get;set;}
    public boolean goUp                 {get;set;}
    public boolean seeTable2            {get;set;}
    public String  orderbyAscDesc       {get;set;}
    public String  colum                {get;set;}
    public String  queryString          {get;set;}
    public String  queryC               {get;set;} 
    public string  NotesRadio           {get;set;}
    public string  CaseRadio            {get;set;}
    public string  ServicesRadio        {get;set;}
    public boolean ShowTotals           {get;set;} 
    public integer totalsSrvices    {get;set;}
    public integer totalsSrvicesShow    {get;set;} 
    transient public integer value       ;
    public integer valueCase            {get;set;}
    transient public string  arrowOrder   ;
    public string  columOrder           {get;set;}
    private Date twoYearsBack = Date.today().addYears(-2);


    
    public SFF_CasesContactController(ApexPages.StandardController controller) {
        this.contactRecord= (Contact)controller.getRecord();
        CaseRadio       = '2';
        listArrowCol    = new Map<String,boolean>();
        listdates       = new Map<date,id>(); 
        listFieldSort   = new Map<String,String>();
        sessionNotes    = new List<Session__c>();

        listArrowCol.put('col-1',true); 
        for(integer i = 2 ; i<10;i++){
             listArrowCol.put('col-'+i,false); 
        }
        listFieldSort.put('col-1','Service_Date__c ');
        listFieldSort.put('col-2','name');
        listFieldSort.put('col-3','Subtype__c ');
        listFieldSort.put('col-4','Area_of_Focus__c ');
        listFieldSort.put('col-5',' ');
        listFieldSort.put('col-6','Hours_of_Service_Provided__c');
        listFieldSort.put('col-7','Dollar_Amount_of_Service_Provided__c');
        listFieldSort.put('col-8','Mode__c.case__c ');
        listFieldSort.put('col-9','Event__r.case__c ');
        NotesRadio='1';
        CaseID     = contactRecord.id;
        listEvents = [  select id,Date__c, Owner.Name, Event_Note__c,Case__r.name
                      from Event__c
                      where Case__r.Contact__c= :contactRecord.id order by Date__c desc];
        colum   ='col-1';
        columOrder = 'c1';
        goUp    = true;
        listWS  = new list<SFF_wrapperService>();
        initLists();
        //retrieveSessionNotes();
        fillNoteLists();
    }
    public void initLists(){
        chargeDataThisCaseServices();
        listWS = OrganizeDATA().values();
        ServicesRadio='1';
        listWS.sort();
        list<SFF_wrapperService> aux = new list<SFF_wrapperService>();
        for(integer i=listWS.size()-1 ; i > -1;i--){
             aux.add(listWS.get(i)); 
        }
        listWS=new list<SFF_wrapperService>(aux);
        ServicesRecordsCut();
    }
    
    public void NoteRadioList(){
       if(NotesRadio=='1'){
            listEvents.clear();
            listEvents   = [select id,Date__c, Owner.Name, Event_Note__c,Case__c,Case__r.name
                            from Event__c
                           where Case__r.Contact__c= :contactRecord.id order by Date__c desc];
        }else{
            listEvents.clear();
                listEvents  = [  select id,Date__c, Owner.Name, Event_Note__c,Case__c,Case__r.name
                             	from Event__c
                            	where Case__r.Contact__c= :contactRecord.id  and Ownerid = :UserInfo.getUserId() order by Date__c desc];
        }
        value = listEvents.size();
    }
    
    global class EventAndSessionNote implements Comparable {
        public Date datec {get;set;}
        public String owner {get;set;}
        public String note {get;set;}
        public Id casec {get;set;}
        public String caseName {get;set;}

        public EventAndSessionNote(Date pdate, String pOwner, String pNote, Id pCase, String pCaseName){
            this.datec = pdate;
            this.owner = pOwner;
            this.note = pNote;
            this.casec = pCase;
            this.caseName = pCaseName;
        }

        // Compare Events and Session Notes based on the date field.
        global Integer compareTo(Object compareTo) {
            EventAndSessionNote evNote = (EventAndSessionNote)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;

            if(this.datec < evNote.datec){
                // Set return value to a positive value.
                returnValue = 1;
            } else if (this.datec > evNote.datec) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            return returnValue;
        }
    }

    public void fillNoteLists(){
        NoteRadioList();
        retrieveSessionNotes();

        evAndNotes = new List<EventAndSessionNote>();
        for(Event__c ev : listEvents){
            EventAndSessionNote evAndNote = new EventAndSessionNote(ev.Date__c, ev.Owner.Name, ev.Event_Note__c, ev.Case__c, ev.Case__r.Name);
            evAndNotes.add(evAndNote);
        }

        for(Session__c s : sessionNotes){
            EventAndSessionNote evAndNote = new EventAndSessionNote(s.Session_Date__c, s.Owner.Name, s.Notes__c, s.Case__c, s.Case__r.Name);
            evAndNotes.add(evAndNote);
        }

        evAndNotes.sort();
    }

    public void retrieveSessionNotes(){
        if(NotesRadio=='1'){
            sessionNotes.clear();
            sessionNotes  = [   Select Id, Session_Date__c, Owner.Name, Case__c, Case__r.Name, Notes__c
                                From Session__c
                                Where Case__r.Contact__c =: contactRecord.id  
                                ORDER BY Session_Date__c desc];
        }else{
            sessionNotes.clear();
            sessionNotes = [Select Id, Session_Date__c, Owner.Name, Case__c, Case__r.Name, Notes__c
                            From Session__c
                            Where Case__r.Contact__c =: contactRecord.id  
                            AND Ownerid =: UserInfo.getUserId() 
                            ORDER BY Session_Date__c desc];
        }
        value = sessionNotes.size();
    }

    public void CaseRadioList(){
        if(ServicesRadio=='2'){

            Set<Id> contactsIds = new Set<Id>();
            for(integer j=0; j < listWS.size(); j++){ 
                contactsIds.add(listWS.get(j).serviceItem.OwnerService);
            }
            //get the contacts that are staff member of the list services
            List<Contact> contacts = [SELECT Id, Salesforce_User__c FROM Contact WHERE Id IN :contactsIds];
            
            //create a Map from UsersId to ContactId to get the contact related to the user
            Map<Id,Id> userIdToContactId = new Map<Id,Id>();
            for(Contact con: contacts){
              if(con.Salesforce_User__c != null){
                userIdToContactId.put(con.Salesforce_User__c,con.Id);
              }
            }

            list<SFF_wrapperService> auxList = new  list<SFF_wrapperService>();
            
            if(userIdToContactId.containsKey(UserInfo.getUserId())){
              for(integer i=0; i < listWS.size(); i++){
               //if ((listWS.get(i).serviceItem.OwnerService == UserInfo.getUserId()) &&(!listWS.get(i).serviceItem.typeService.equals('Night')) ){
                if(listWS.get(i).serviceItem.OwnerService == userIdToContactId.get(UserInfo.getUserId())){
                  auxList.add(listWS.get(i));
                }
              }
            }
            listWS.clear();
            listWS = new  list<SFF_wrapperService>(auxList);
            ShowTotals = false;
       }else{
            chargeDataThisCaseServices();
	        listWS = OrganizeDATA().values();
	        for(SFF_wrapperService var :listWS){
			    var.orderby =columOrder;
			}
	        if(listArrowCol.get(colum)){
		                listWS.sort();
		                list<SFF_wrapperService> aux = new list<SFF_wrapperService>();
		                for(integer i = listWS.size()-1 ; i > -1;i--  ){
		                      aux.add(listWS.get(i)); 
		                }
		                listWS=new list<SFF_wrapperService>(aux);
		            }else{
		                 arrowOrder='0';
		                 listWS.sort();
		            } 
		  ServicesRecordsCut();
       }
       
    }
    public void ServicesRecordsCut(){
	   
	   totalsSrvices = listWS.size();
	   Integer displayLimit = Integer.valueOf(Label.Display_Records_Limit);
       if(listWS.size()>displayLimit){
       		Aux_listWS = new List<SFF_wrapperService>();  
       		for(integer i= 0 ; i<displayLimit ; i++){
       	    	Aux_listWS.add(listWS.get(i));
       		}
       		listWS = new List<SFF_wrapperService>(Aux_listWS);
       		
       }
       totalsSrvicesShow = listWS.size();
       ShowTotals = true;
    }
    public void OrderBy(){
        columOrder = 'c1';
        for(SFF_wrapperService var :listWS){
		    var.orderby ='c1';
		}
        listArrowCol.put(colum,!listArrowCol.get(colum));
        if(listWS.size()>0 ){
            if(listArrowCol.get(colum)){
                arrowOrder='1';
                listWS.sort();
            	list<SFF_wrapperService> aux = new list<SFF_wrapperService>();
                for(integer i=listWS.size()-1 ; i > -1;i--  ){
                      aux.add(listWS.get(i)); 
                }
                listWS=new list<SFF_wrapperService>(aux);
            }else{
            	list<SFF_wrapperService> aux = new list<SFF_wrapperService>();
                for(integer i=listWS.size()-1 ; i > -1;i--  ){
                      aux.add(listWS.get(i)); 
                }
                listWS=new list<SFF_wrapperService>(aux);
            } 
        }
    }
    public void OrderByCase(){
        columOrder = 'c2';
        for(SFF_wrapperService var :listWS){
		    var.orderby ='c2';
		}
        listArrowCol.put(colum,!listArrowCol.get(colum));
        if(listWS.size()>0 ){
            if(listArrowCol.get(colum)){
                arrowOrder='1';
                listWS.sort();
                list<SFF_wrapperService> aux = new list<SFF_wrapperService>();
                for(integer i = listWS.size()-1 ; i > -1;i--  ){
                      aux.add(listWS.get(i)); 
                }
                listWS=new list<SFF_wrapperService>(aux);
            }else{
                 arrowOrder='0';
                 listWS.sort();
            } 
         }
    }
    public Map<id,SFF_wrapperService> OrganizeDATA(){
        listWrapp    = new Map<id,SFF_wrapperService>();   
        for(id var :listServices.keySet()){

            if(String.isNotBlank(listServices.get(var).Event__r.Case__c)){

                listWrapp.put(var, new SFF_wrapperService(false,
                                                     '',
                                                     listServices.get(var).Event__r.name,
                                                     listServices.get(var).Staff_Member__c,
                                                     listServices.get(var).Event__r.case__c,
                                                     listServices.get(var).Id,
                                                     listServices.get(var).Service__c,
                                                     'Service',
                                                     listServices.get(var).Event__r.case__r.name,
                                                     listServices.get(var).Service_Date__c,
                                                     listServices.get(var).Subtype__c,
                                                     listServices.get(var).Area_of_Focus__c,
                                                     listServices.get(var).Hours_of_Service_Provided__c,
                                                     listServices.get(var).Dollar_Amount_of_Service_Provided__c,
                                                     listServices.get(var).Event__r.Mode__c,
                                                     listServices.get(var).Staff_Member__r.Name ));
            } else {
                listWrapp.put(var, new SFF_wrapperService(false,
                                                     '',
                                                     listServices.get(var).Event__r.name,
                                                     listServices.get(var).Staff_Member__c,
                                                     listServices.get(var).Case__c,
                                                     listServices.get(var).Id,
                                                     listServices.get(var).Service__c,
                                                     'Service',
                                                     listServices.get(var).Case__r.name,
                                                     listServices.get(var).Service_Date__c,
                                                     listServices.get(var).Subtype__c,
                                                     listServices.get(var).Area_of_Focus__c,
                                                     listServices.get(var).Hours_of_Service_Provided__c,
                                                     listServices.get(var).Dollar_Amount_of_Service_Provided__c,
                                                     listServices.get(var).Event__r.Mode__c,
                                                     listServices.get(var).Staff_Member__r.Name ));
            }
            // idCaseC,  idServiceC, serviceC, typeServiceC, nameC, serviceDateC, subtypeC, areaFocusC, horsServiceC, unitC, modeC, staffMemberC
        }
        /*for(id var :listNight.keySet()){
           listWrapp.put(var,new SFF_wrapperService(false,
                                                listNight.get(var).Attendance_status__c,
                                                listNight.get(var).name,
                                                listNight.get(var).Case__r.Primary_Staff_on_Case__c,
                                                listNight.get(var).Case__c,
                                                listNight.get(var).id,
                                                'Bed Night',
                                                'Night',
                                                listNight.get(var).Case__r.name,
                                                listNight.get(var).Date_Service_was_Provided__c,
                                               '','',0,0,'',
                                               listNight.get(var).Case__r.Primary_Staff_on_Case__r.name ));
        }*/
        list<id> ownwerIdList = new list<id>();
        for(id var :listSessions.keySet()){
        	ownwerIdList.add(listSessions.get(var).Ownerid);
        }
        Map<id,user> userMap = new Map<id,user>();
        for( user u : [SELECT Name   FROM user WHERE id in :ownwerIdList]){
        	userMap.put(u.id,u);
        }
        for(id var :listSessions.keySet()){
        
           listWrapp.put(var, new SFF_wrapperService(false,
                                                 listSessions.get(var).Attendance_Status__c,
                                                 listSessions.get(var).name,
                                                 listSessions.get(var).Service__r.Staff_Member__c,
                                                 listSessions.get(var).Case__c,
                                                 listSessions.get(var).id,
                                                 listSessions.get(var).Service_Provided__c,
                                                 'Session',
                                                 listSessions.get(var).Case__r.name,
                                                 listSessions.get(var).Session_Date__c,
                                                 listSessions.get(var).Subtype__c,
                                                 listSessions.get(var).Area_of_Focus__c,
                                                 0,0, '',
												                         listSessions.get(var).Service__r.Staff_Member__r.Name));
        }

        //change Decimal to two decimal points
        for(SFF_wrapperService value: listWrapp.values()){
            System.debug('value--->'+value);
            if(value.serviceItem.unit != null){
                value.serviceItem.unit = value.serviceItem.unit.setScale(2);
            }
        }
        return listWrapp;
    }
    public void chargeDataThisCaseServices(){
        listServices = new Map<ID, Service__c>([select id, name, Service__c,
                                                    Service_Date__c, 
                                                    Subtype__c, 
                                                    Event__r.name,
                                                    Event__r.case__r.name,
                                                    Area_of_Focus__c, 
                                                    Hours_of_Service_Provided__c, 
                                                    Dollar_Amount_of_Service_Provided__c, 
                                                    Event__r.Mode__c, 
                                                    Event__r.case__c,
                                                    Staff_Member__c,
                                                    Staff_Member__r.Name,
                                                    Ownerid, Case__c, Case__r.Name, Case__r.Contact__c
                                            from  Service__c 
                                            where (Event__r.case__r.Contact__c=:contactRecord.id OR (Case__r.Contact__c = :contactRecord.id AND Service__c =: 'MetroCards (non-monetary)')) AND Service_Date__c >= :twoYearsBack]);

         listSessions = new Map<ID, Session__c>([SELECT Id,Service_Provided__c,Attendance_Status__c,Area_of_Focus__c,Session_Date__c,Subtype__c,Case__r.name,Case__c,Ownerid,name, Service__r.Staff_Member__c, Service__r.Staff_Member__r.Name  FROM Session__c WHERE Case__r.Contact__c = :contactRecord.id AND Session_Date__c >= :twoYearsBack]);
         //listNight    = new Map<ID, Night__c>  ([SELECT Id,Attendance_status__c,Date_Service_was_Provided__c,Case__c,Case__r.name,Case__r.Primary_Staff_on_Case__c,Case__r.Primary_Staff_on_Case__r.name,name FROM Night__c WHERE Case__r.Contact__c = :contactRecord.id]);
    }
  


}