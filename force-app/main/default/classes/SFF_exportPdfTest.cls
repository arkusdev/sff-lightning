@isTest
public class SFF_exportPdfTest {
    
    static testMethod void testMethodPDF(){
        contact testContact = new contact();
        testContact.FirstName = 'firstname';
        testContact.LastName = 'lastname';
        testContact.Birthdate = date.newInstance(1987, 3, 3);
        insert testContact;
        //
        Case__c testCase = new Case__c();
        testCase.Contact__c = testContact.id;
        testCase.Project__c = 'Children & Youth Program';
        testCase.Location__c ='BxFJC';
        testCase.Type_of_Matter__c = 'Economic';
        testCase.Case_Open_Date__c = date.today();
        testCase.Matter_Type_Detail__c = 'Consular Processing';
        insert testCase;
        //
        Case__c testCase2 = new Case__c();
        testCase2.Contact__c = testContact.id;
        testCase2.Project__c = 'Children & Youth Program';
        testCase2.Location__c ='BxFJC';
        testCase2.Type_of_Matter__c = 'Immigration';
        testCase2.Case_Open_Date__c = date.today();
        testCase2.Matter_Type_Detail__c = 'Consular Processing';
        insert testCase2;
        //
        Event__c testEvent = new Event__c();
        testEvent.Case__c = testCase.id;
        testEvent.Mode__c = 'In Office';
        testEvent.Date__c =date.newInstance(2015, 4, 4);
        testEvent.Event_Note__c = 'text sample';
        insert testEvent;
        //
        Night__c testNight = new Night__c();
        testNight.Case__c = testCase.id; 
        date aux =  date.newInstance(2015, 5, 5 );
        testNight.Date_Service_was_Provided__c = aux;
        insert testNight;
        //
        Session__c testSession = new Session__c();
        testSession.Contact__c = testContact.id;
        testSession.Case__c = testCase.id;
        testSession.Session_Date__c = date.newInstance(2015, 6, 6);
        insert testSession;
        //
        Service__c testService = new Service__c();
        testService.Event__c = testEvent.id;
        testService.Service__c = 'Advice (Legal)';
        testService.Subtype__c='Abuse & Neglect';
        testService.Area_of_Focus__c ='Child/Parent Services - Children\'s/Family Activities';
        testService.Mode__c='In Office';
        testService.Service_Date__c= date.newInstance(2015, 7, 7);
        insert testService;
        
        PageReference pageRef = Page.SFF_NotesPDFview;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','1');
        ApexPages.currentPage().getParameters().put('test','0');
        
        SFF_renderPdf control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','0');
        ApexPages.currentPage().getParameters().put('CaseR','1');
        ApexPages.currentPage().getParameters().put('ServiceR','2');
        
        control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','0');
        ApexPages.currentPage().getParameters().put('CaseR','2');
        ApexPages.currentPage().getParameters().put('ServiceR','2');
        
         control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','1');
        ApexPages.currentPage().getParameters().put('test','0');
        ApexPages.currentPage().getParameters().put('CaseR','2');
        ApexPages.currentPage().getParameters().put('ServiceR','1');
        
        control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','0');
        ApexPages.currentPage().getParameters().put('CaseR','1');
        ApexPages.currentPage().getParameters().put('ServiceR','2');
        
        control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        
         control = new SFF_renderPdf();
        //////////////////////////// 
        ApexPages.currentPage().getParameters().put('idd',''+testCase.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','1');
        
        control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testCase.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','1');
        ApexPages.currentPage().getParameters().put('CaseR','1');
        ApexPages.currentPage().getParameters().put('ServiceR','1');
        control = new SFF_renderPdf();
        
        ApexPages.currentPage().getParameters().put('idd',''+testCase.id);
        ApexPages.currentPage().getParameters().put('ops','2');
        ApexPages.currentPage().getParameters().put('test','1');
        ApexPages.currentPage().getParameters().put('CaseR','1');
        ApexPages.currentPage().getParameters().put('ServiceR','2');
        ApexPages.currentPage().getParameters().put('arr','1');
        ApexPages.currentPage().getParameters().put('col','c1');
        control = new SFF_renderPdf();
            
        Test.startTest();
            ApexPages.currentPage().getParameters().put('idd',''+testCase.id);
            ApexPages.currentPage().getParameters().put('ops','2');
            ApexPages.currentPage().getParameters().put('test','1');
            ApexPages.currentPage().getParameters().put('CaseR','2');
            ApexPages.currentPage().getParameters().put('ServiceR','1');
            control = new SFF_renderPdf();
            
            ApexPages.currentPage().getParameters().put('idd',''+testCase.id);
            ApexPages.currentPage().getParameters().put('ops','2');
            ApexPages.currentPage().getParameters().put('test','1');
            ApexPages.currentPage().getParameters().put('arr','0');
            ApexPages.currentPage().getParameters().put('col','c2');
            control = new SFF_renderPdf();
            
            ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
            ApexPages.currentPage().getParameters().put('ops','2');
            ApexPages.currentPage().getParameters().put('test','0');
            ApexPages.currentPage().getParameters().put('CaseR','2');
            ApexPages.currentPage().getParameters().put('ServiceR','2');
            control = new SFF_renderPdf();
            
            ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
            ApexPages.currentPage().getParameters().put('ops','2');
            ApexPages.currentPage().getParameters().put('test','0');
            ApexPages.currentPage().getParameters().put('CaseR','2');
            ApexPages.currentPage().getParameters().put('ServiceR','1');
            control = new SFF_renderPdf();
            
            ApexPages.currentPage().getParameters().put('idd',''+testContact.id);
            ApexPages.currentPage().getParameters().put('ops','2');
            ApexPages.currentPage().getParameters().put('test','0');
            ApexPages.currentPage().getParameters().put('CaseR','1');
            ApexPages.currentPage().getParameters().put('ServiceR','1');
            control = new SFF_renderPdf();
            control.getDateT();
        Test.stopTest();
    }
}