public with sharing class SFF_renderPdf {
    
    
    private String Option;
    public  String testentra{get;set;}
    private String idObject ;
    public  String test {get;set;}
    public  String dataID{get;set;}
    public  String title{get;set;}
    private list<Contact> dataContact;
    private list<Case__c> dataCase;
    public  boolean renderTable{get;set;}
    public  list<Event__c>   listEvents{get;set;}
    public Map<id,SFF_UnitData > listWrapp{get;set;}
    public List<SFF_UnitData >   listWS{get;set;}
    public string  CaseRadio        {get;set;}
    public string  ServicesRadio    {get;set;}
    public string message {get;set;}
    private Map<ID, Night__c>   listNight;
    private Map<ID, Service__c> listServices;
    private Map<ID, Session__c> listSessions;
    public string dateT;
    public string getin {get;set;}
    public string whereQueryEvents;
    public id idtest{get;set;}
    public string noteChosen{get;set;}
    public string servicesChosen{get;set;}
    public string ThoiseOrAllCase{get;set;}
    public string conta{get;set;}
    public  SFF_renderPdf () {
        System.debug('******1');
        if(ApexPages.currentPage().getParameters().containsKey('ops') && ApexPages.currentPage().getParameters().containsKey('idd')&&ApexPages.currentPage().getParameters().containsKey('test')){
            
            System.debug('******2');
            // idd ( id of contact || id of case )
            idObject = ApexPages.currentPage().getParameters().get('idd');
            //  option ( 1 = "All notes" | 2 = "My notes" )
            Option   = ApexPages.currentPage().getParameters().get('ops');
            //  test inform from where comes the id ( 0 = Contacts | 1 = Cases )
            test     = ApexPages.currentPage().getParameters().get('test'); 
            servicesChosen = ' All Services';
            listWS     = new list<SFF_UnitData >();
            listEvents = new list<Event__c>();
            title = 'Contact ID: ';
            if(test.equals('0')){
                System.debug('******3');
                dataContact = new list<Contact>([SELECT Contact_ID__c FROM Contact WHERE id = :idObject LIMIT 1 ]);
                dataID= dataContact.get(0).Contact_ID__c ;
                if(Option.equals('2')){
                    noteChosen = 'My Notes';
                    whereQueryEvents = ' Case__r.Contact__c = \''+idObject+'\' and Ownerid = \''+UserInfo.getUserId()+'\'';
                    listEvents  = Database.query( 'select id,Date__c, Owner.Name, Event_Note__c, Event_Note_ignore__c, Case__c,Case__r.name from Event__c where '+whereQueryEvents+' order by Date__c desc');
                    System.debug('******4');
                }else{
                    noteChosen = 'All Notes';
                    whereQueryEvents = ' Case__r.Contact__c = \''+idObject+'\'';
                    listEvents  = Database.query( 'select id,Date__c, Owner.Name, Event_Note__c, Event_Note_ignore__c, Case__c,Case__r.name from Event__c where '+whereQueryEvents+' order by Date__c desc');     
                    System.debug('******5');
                }
            }else{
                if(test.equals('1')){
                    dataCase = new list<Case__c>([SELECT Contact__r.Contact_ID__c  FROM Case__c WHERE id = :idObject LIMIT 1 ]); 
                    dataID= dataCase.get(0).Contact__r.Contact_ID__c;
                    if(Option.equals('2')){
                        noteChosen = 'My Notes';
                        whereQueryEvents = ' Case__c = \''+idObject+'\' and Ownerid = \''+UserInfo.getUserId()+'\'';
                        listEvents = Database.query( 'select id,Date__c, Owner.Name, Event_Note__c, Event_Note_ignore__c, Case__c,Case__r.name from Event__c where '+whereQueryEvents+' order by Date__c desc');
                        System.debug('******6');
                    }else{
                        noteChosen = 'All Notes';
                        getin ='llega';
                        whereQueryEvents = ' Case__c = \''+idObject+'\'';
                        listEvents    = Database.query( 'select id,Date__c, Owner.Name, Event_Note__c, Event_Note_ignore__c, Case__c,Case__r.name from Event__c where '+whereQueryEvents+' order by Date__c desc');     
                        System.debug('******7');
                    }
                }
            }  
            if(listEvents.size()>0 ){
                for(integer i = 0 ; i < listEvents.size(); i++){
                    if(listEvents[i] != null && listEvents[i].Event_Note__c != null){
                        listEvents[i].Event_Note__c = listEvents[i].Event_Note__c.replaceAll('<img\\s*(.*?)\\s*>', '');
                    }
                    SFF_UnitData eve = new SFF_UnitData();
                    eve.initEvent(true,listEvents.get(i).Date__c,listEvents.get(i));
                    listWS.add(eve);
                    System.debug('******8'); 
                    
                    
                }
            }
        }
        initLists();
        listWS.sort();
        investList();
        if(listEvents.size()>0){
            renderTable = true;
        }else{
            renderTable = false;
        }
    }
    public void initLists(){
        System.debug('******9');
        if(ApexPages.currentPage().getParameters().containsKey('CaseR') && ApexPages.currentPage().getParameters().containsKey('ServiceR')){
            CaseRadio     = ApexPages.currentPage().getParameters().get('CaseR');
            ServicesRadio = ApexPages.currentPage().getParameters().get('ServiceR');
            if((CaseRadio <> null)&&(ServicesRadio <>null)){
                if(test.equals('0')){
                    ThoiseOrAllCase = ' Services on this Contact';
                    chargeServicesFromThisCONTACT();
                    listWS.addAll(OrganizeDATA().values());
                    // conta = ' ' +  listWS.get(4).serviceItem.AttendanceStatus;
                    CaseRadioList();
                }else{
                    if(test.equals('1')){
                        ServicesFromCasesRadio();
                        CaseRadioList();
                    }
                }
            }else{
                message = 'One parameter is missing. '; 
            }
            System.debug('******10');
        }else{
            message = 'One parameter is missing. '; 
        }   
    }
    public void ServicesFromCasesRadio(){
        System.debug('******11');
        if(CaseRadio.equals('2')){
            ThoiseOrAllCase = ' Services on this Case';
            chargeDataThisCaseServices();  
            listWS.addAll(OrganizeDATA().values());
        }else{ 
            ThoiseOrAllCase = ' Services on all Cases';
            chargeDataAllCasesServices();
            listWS.addAll(OrganizeDATA().values());             
        }
    }
    public void CaseRadioList(){
        System.debug('******12');
        if(ServicesRadio.equals('2')){
            servicesChosen = ' My Services';
            list<SFF_UnitData > auxList = new  list<SFF_UnitData >();
            for(integer i=0; i < listWS.size(); i++){
                if (listWS.get(i).isEvent || ((listWS.get(i).serviceItem.OwnerService == UserInfo.getUserId()) &&(!listWS.get(i).serviceItem.typeService.equals('Night'))) ){
                    auxList.add(listWS.get(i));
                }
            }
            listWS = new  list<SFF_UnitData >(auxList);
        }
    }
    public Map<id,SFF_UnitData > OrganizeDATA(){
        System.debug('******13');
        listWrapp    = new Map<id,SFF_UnitData >();   
        for(id var :listServices.keySet()){
            SFF_UnitData auxWrapp = new SFF_UnitData();
            auxWrapp.initService(false,
                                 '',
                                 listServices.get(var).Event__r.name,
                                 listServices.get(var).Staff_Member__c,
                                 listServices.get(var).Event__r.case__c,
                                 listServices.get(var).Id,
                                 listServices.get(var).Service__c,
                                 'Service',
                                 listServices.get(var).Event__r.case__r.name,
                                 listServices.get(var).Service_Date__c,
                                 listServices.get(var).Subtype__c,
                                 listServices.get(var).Area_of_Focus__c,
                                 listServices.get(var).Hours_of_Service_Provided__c,
                                 listServices.get(var).Unit__c,
                                 listServices.get(var).Event__r.Mode__c,
                                 listServices.get(var).Staff_Member__r.name );
            // conta = 'en wrapp '+auxWrapp.serviceItem.service;                     
            if(listServices.get(var).Event__r.Case__r.Type_of_Matter__c <> null){
                if(listServices.get(var).Event__r.Case__r.Type_of_Matter__c.equals('Economic')){
                    auxWrapp.HaveImmigrationEconomic =  true;
                    auxWrapp.ImmigrationEconomic = 'Economic Matter Details: ';
                    auxWrapp.ValueImmigrationEconomic = listServices.get(var).Event__r.Case__r.Economic_Matter_Detail__c;
                }else{
                    if(listServices.get(var).Event__r.Case__r.Type_of_Matter__c.equals('Immigration')){
                        auxWrapp.HaveImmigrationEconomic =  true;
                        auxWrapp.ImmigrationEconomic = 'Immigration Matter Details: ';
                        auxWrapp.ValueImmigrationEconomic = listServices.get(var).Event__r.Case__r.Immigration_Law_Matter_Detail__c;
                    }else{
                        auxWrapp.HaveImmigrationEconomic =  false;
                    }
                }
            }
            auxWrapp.TypeOfAssistance=listServices.get(var).Event__r.Case__r.Type_of_Assistance__c ;
            auxWrapp.TypeOfMatter    =listServices.get(var).Event__r.Case__r.Type_of_Matter__c ;
            
            listWrapp.put(var,auxWrapp);
            idtest = var;
            //conta= listWrapp.get(var).serviceItem.service+' ' +listWrapp.get(var).serviceItem.serviceDate;
            // idCaseC,  idServiceC, serviceC, typeServiceC, nameC, serviceDateC, subtypeC, areaFocusC, horsServiceC, unitC, modeC, staffMemberC
        }
        for(id var :listNight.keySet()){
            System.debug('******14');
            SFF_UnitData auxWrappN= new SFF_UnitData();
            auxWrappN.initService(false,
                                  listNight.get(var).Attendance_status__c,
                                  listNight.get(var).name,
                                  listNight.get(var).Case__r.Primary_Staff_on_Case__c,
                                  listNight.get(var).Case__c,
                                  listNight.get(var).id,
                                  'Bed Night',
                                  'Night',
                                  listNight.get(var).Case__r.name,
                                  listNight.get(var).Date_Service_was_Provided__c,
                                  '','',0,0,'',
                                  listNight.get(var).Case__r.Primary_Staff_on_Case__r.name );
            if(listNight.get(var).Case__r.Type_of_Matter__c <> null){
                if(listNight.get(var).Case__r.Type_of_Matter__c.equals('Economic')){
                    auxWrappN.HaveImmigrationEconomic =  true;
                    auxWrappN.ImmigrationEconomic = 'Economic Matter Details: ';
                    auxWrappN.ValueImmigrationEconomic = listNight.get(var).Case__r.Economic_Matter_Detail__c;
                }else{
                    if(listNight.get(var).Case__r.Type_of_Matter__c.equals('Immigration')){
                        auxWrappN.HaveImmigrationEconomic =  true;
                        auxWrappN.ImmigrationEconomic = 'Immigration Matter Details: ';
                        auxWrappN.ValueImmigrationEconomic = listNight.get(var).Case__r.Immigration_Law_Matter_Detail__c;
                    }else{
                        auxWrappN.HaveImmigrationEconomic =  false;
                    }
                }
            }      
            auxWrappN.TypeOfAssistance=listNight.get(var).Case__r.Type_of_Assistance__c ;
            auxWrappN.TypeOfMatter    =listNight.get(var).Case__r.Type_of_Matter__c ;                              
            listWrapp.put(var,auxWrappN );
        }
        
        list<id> ownwerIdList = new list<id>();
        for(id var :listSessions.keySet()){
            ownwerIdList.add(listSessions.get(var).Ownerid);
        }
        Map<id,user> userMap = new Map<id,user>();
        for( user u : [SELECT Name   FROM user WHERE id in :ownwerIdList]){
            userMap.put(u.id,u);
        }
        for(id var :listSessions.keySet()){
            SFF_UnitData auxWrappS = new SFF_UnitData();
            auxWrappS.initService(false,
                                  listSessions.get(var).Attendance_Status__c,
                                  listSessions.get(var).name,
                                  listSessions.get(var).Ownerid,
                                  listSessions.get(var).Case__c,
                                  listSessions.get(var).id,
                                  listSessions.get(var).Service_Provided__c,
                                  'Session',
                                  listSessions.get(var).Case__r.name,
                                  listSessions.get(var).Session_Date__c,
                                  listSessions.get(var).Subtype__c,
                                  listSessions.get(var).Area_of_Focus__c,
                                  0,0, '',
                                  userMap.get(listSessions.get(var).Ownerid).name);
            if(listSessions.get(var).Case__r.Type_of_Matter__c <> null){
                if(listSessions.get(var).Case__r.Type_of_Matter__c.equals('Economic')){
                    auxWrappS.HaveImmigrationEconomic =  true;
                    auxWrappS.ImmigrationEconomic = 'Economic Matter Details: ';
                    auxWrappS.ValueImmigrationEconomic = listSessions.get(var).Case__r.Economic_Matter_Detail__c;
                }else{
                    if(listSessions.get(var).Case__r.Type_of_Matter__c.equals('Immigration')){
                        auxWrappS.HaveImmigrationEconomic =  true;
                        auxWrappS.ImmigrationEconomic = 'Immigration Matter Details: ';
                        auxWrappS.ValueImmigrationEconomic = listSessions.get(var).Case__r.Immigration_Law_Matter_Detail__c;
                    }else{
                        auxWrappS.HaveImmigrationEconomic =  false;
                    }
                }
            } 
            auxWrappS.TypeOfAssistance=listSessions.get(var).Case__r.Type_of_Assistance__c ;
            auxWrappS.TypeOfMatter    =listSessions.get(var).Case__r.Type_of_Matter__c ; 
            listWrapp.put(var,auxWrappS );
        }
        System.debug('******15');
        //  conta= listWrapp.get(idtest).serviceItem.service+' ' +listWrapp.get(idtest).serviceItem.serviceDate;
        return listWrapp;
    }
    public void chargeDataAllCasesServices(){
        System.debug('******16');
        list<Case__c> CaseContact = new list<Case__c>( [SELECT id,Contact__c FROM Case__c  WHERE id = :idObject  limit 1]);
        string testContactID = CaseContact.get(0).Contact__c ;
        listServices = new Map<ID, Service__c>([select id, name, Service__c,
                                                Service_Date__c, 
                                                Subtype__c, 
                                                Event__r.name,
                                                Event__r.case__r.name,
                                                Area_of_Focus__c, 
                                                Hours_of_Service_Provided__c, 
                                                Unit__c, 
                                                Event__r.Mode__c, 
                                                Event__r.case__c,
                                                Staff_Member__c,Staff_Member__r.name,
                                                Event__r.Case__r.Economic_Matter_Detail__c,
                                                Event__r.Case__r.Immigration_Law_Matter_Detail__c,
                                                Event__r.Case__r.Type_of_Matter__c,
                                                Event__r.Case__r.Type_of_Assistance__c
                                                from  Service__c where Event__r.Case__r.Contact__c = :testContactID  ]);
        listSessions = new Map<ID, Session__c>([SELECT Id,Service_Provided__c,Attendance_Status__c,Area_of_Focus__c,Session_Date__c,Subtype__c,Case__r.name,Case__c,Ownerid,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c,
                                                Case__r.Type_of_Assistance__c
                                                FROM Session__c where Case__r.Contact__c = :testContactID]);
        listNight    = new Map<ID, Night__c>  ([SELECT Id,Attendance_status__c,Date_Service_was_Provided__c,Case__c,Case__r.name, Case__r.Primary_Staff_on_Case__c,Case__r.Primary_Staff_on_Case__r.name,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c,
                                                Case__r.Type_of_Assistance__c
                                                FROM Night__c where  Case__r.Contact__c = :testContactID]);
        System.debug('******17');
    }
    public void chargeDataThisCaseServices(){
        System.debug('******18');
        listServices = new Map<ID, Service__c>([select id, name, Service__c,
                                                Service_Date__c, 
                                                Subtype__c, 
                                                Event__r.name,
                                                Event__r.case__r.name,
                                                Area_of_Focus__c, 
                                                Hours_of_Service_Provided__c, 
                                                Unit__c, 
                                                Event__r.Mode__c, 
                                                Event__r.case__c,
                                                Staff_Member__c,Staff_Member__r.name,
                                                Ownerid,
                                                Event__r.Case__r.Economic_Matter_Detail__c,
                                                Event__r.Case__r.Immigration_Law_Matter_Detail__c,
                                                Event__r.Case__r.Type_of_Matter__c,
                                                Event__r.Case__r.Type_of_Assistance__c
                                                from  Service__c 
                                                where Event__r.case__c=:idObject ]);
        listSessions = new Map<ID, Session__c>([SELECT Id,Service_Provided__c,Attendance_Status__c,Area_of_Focus__c,Session_Date__c,Subtype__c,Case__r.name,Case__c,Ownerid,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c  ,
                                                Case__r.Type_of_Assistance__c
                                                FROM Session__c WHERE Case__c = :idObject ]);
        listNight    = new Map<ID, Night__c>  ([SELECT Id,Attendance_status__c,Date_Service_was_Provided__c,Case__c,Case__r.name,Case__r.Primary_Staff_on_Case__c,Case__r.Primary_Staff_on_Case__r.name,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c ,
                                                Case__r.Type_of_Assistance__c
                                                FROM Night__c WHERE Case__c = :idObject ]);
        System.debug('******19 ' + listServices.values().size());
        System.debug('******19 ' + listSessions.values().size());
        System.debug('******19 ' + listNight.values().size());
    }
    public void chargeServicesFromThisCONTACT(){
        System.debug('******20');
        
        listServices = new Map<ID, Service__c>([select id, name, Service__c,
                                                Service_Date__c, 
                                                Subtype__c, 
                                                Event__r.name,
                                                Event__r.case__r.name,
                                                Area_of_Focus__c, 
                                                Hours_of_Service_Provided__c, 
                                                Unit__c, 
                                                Event__r.Mode__c, 
                                                Event__r.case__c,
                                                Staff_Member__c,
                                                Staff_Member__r.name,
                                                Ownerid,
                                                Event__r.Case__r.Economic_Matter_Detail__c,
                                                Event__r.Case__r.Immigration_Law_Matter_Detail__c,
                                                Event__r.Case__r.Type_of_Matter__c,
                                                Event__r.Case__r.Type_of_Assistance__c
                                                from  Service__c 
                                                where Event__r.case__r.Contact__c=:idObject ]);
        listSessions = new Map<ID, Session__c>([SELECT Id,Service_Provided__c,Attendance_Status__c,Area_of_Focus__c,Session_Date__c,Subtype__c,Case__r.name,Case__c,Ownerid,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c,
                                                Case__r.Type_of_Assistance__c
                                                FROM Session__c WHERE Case__r.Contact__c = :idObject ]);
        listNight    = new Map<ID, Night__c>  ([SELECT Id,Attendance_status__c,Date_Service_was_Provided__c,Case__c,Case__r.name,Case__r.Primary_Staff_on_Case__c,Case__r.Primary_Staff_on_Case__r.name,name,
                                                Case__r.Economic_Matter_Detail__c,
                                                Case__r.Immigration_Law_Matter_Detail__c,
                                                Case__r.Type_of_Matter__c,
                                                Case__r.Type_of_Assistance__c
                                                FROM Night__c WHERE Case__r.Contact__c = :idObject ]);
        System.debug('******21');
    }
    public string getDateT(){
        
        String myDate = date.today().month()+'/'+date.today().day()+'/'+date.today().year();
        return myDate;
    }
    public void investList(){
        list<SFF_UnitData > aux = new list<SFF_UnitData >();
        for(integer i=listWS.size()-1 ; i > -1;i--  ){
            aux.add(listWS.get(i)); 
        }
        listWS=new list<SFF_UnitData >(aux);
    }
}