@isTest
private class Checkin_Extension_Test {
	
	public static testmethod void Checkin_Extension_Test () {
		Shelter__c shelter1 = new Shelter__c(Name = 'Test Shelter', Capacity__c = 100);
		insert shelter1;
		
		RecordType caseRt = [select Id from RecordType where developerName = 'Hotline' and SobjectType = 'Case__c' limit 1];
		RecordType contRt = [select Id from RecordType where developerName = 'Potential_Client' and SobjectType = 'Contact' limit 1];
       // c.RecordTypeId = rt.Id;
 
		
		Contact con1 = new Contact(FirstName='John', LastName='Bonham',RecordTypeId=contRt.Id);
		Contact con2 = new Contact(FirstName='Jimi', LastName='Hendrix',RecordTypeId=contRt.Id);
		Contact con3 = new Contact(FirstName='Jim', LastName='Morrison',RecordTypeId=contRt.Id);
		List<Contact> cons = new List<Contact>{con1, con2, con3};
		insert cons;
		
		cons = [SELECT Id FROM Contact];
		List<Case__c> cases = new List<Case__c>();
		for(Contact c : cons) {
			Case__c temp = new Case__c(Contact__c = c.Id,RecordTypeId=caseRt.Id, 
                                       Matter_Type_Detail__c = 'Child Support',
                                      Type_of_Assistance__c = 'Hotline',
                                      Type_of_Matter__c = 'Follow-up Case Type');
			cases.add(temp);
		}
		insert cases;
		
		List<Night__c> nights = new List<Night__c>();
		cases = [SELECT Id FROM Case__c];
		for(Case__c temp_case : cases) {
			Night__c temp_night = new Night__c(Case__c = temp_case.Id);
			nights.add(temp_night);
		}
		insert nights;
		
		//Night__c test_night1 = new 
		ApexPages.StandardController sc = new ApexPages.StandardController(shelter1);
		checkIn_Extension ce = new checkIn_Extension(sc);
		
		ce.getStays();
		//ce.closeNight();
		//ce.closequick();
		ce.doQuickAdd();
		ce.closeNight();
		ce.confirmNight();
		
		//ce.doQuickAdd();
		//ce.lastnameholder='test';
		//ce.quickinsert();
		ce.saveStays();
		ce.closeNight();
		//ce.uncloseNight();
		
	}
}